<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cluster extends Model
{
    protected $table = 'm_cluster';

    protected $fillable = {
    	code,
    	description
    }
}
