<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FaRegistry extends Model
{
    	

    public function scopeItemSerial(){

    	$q_item = $q_location = '';        

        if ($_POST['q_item_code'] != ""){
            $q_item = " AND m_item.`code` = '".$_POST['q_item_code']."'";
        }

        if ($_POST['q_location_code'] != ""){
            $q_location = " AND t_serial.`current_location` = '".$_POST['q_location_code']."' ";
        }

        return DB::select( DB::raw("SELECT 
m_item.code, 
m_item.description, 
m_department.description AS dept_code, 
m_item.model,
t_serial.serial_no,
t_serial.trans_code,
t_trans_code.description AS trans_type,
t_serial.trans_no,
t_serial.trans_date,
t_serial.pur_cost,
m_location.description AS location_desc 

FROM m_item

JOIN t_serial ON m_item.code =t_serial.item_code
JOIN t_trans_code ON t_serial.trans_code=t_trans_code.code
JOIN m_department ON m_department.code=m_item.department_code
JOIN m_location ON m_location.code=t_serial.current_location

WHERE t_serial.`trans_date` BETWEEN '".$_POST['fd']."' AND '".$_POST['td']."'  $q_item $q_location

ORDER BY m_item.code") );
    


    }


    public function scopeItemSerialAll(){

        $q_item = $q_location = '';        

        if ($_POST['q_item_code'] != ""){
            $q_item = " m_item.`code` = '".$_POST['q_item_code']."'";
        }

        if ($_POST['q_location_code'] != ""){
            $q_location = " t_serial.`current_location` = '".$_POST['q_location_code']."' ";
        }

        if($_POST['q_item_code'] !=""||$_POST['q_location_code']!=""){
        $where="WHERE  ";
        }else{
        $where="";
        }

        if($_POST['q_item_code'] != ""&&$_POST['q_location_code'] != ""){
        $and=" AND ";
        }else{
        $and="";
        }
        

        return DB::select( DB::raw("SELECT 
m_item.code, 
m_item.description, 
m_department.description AS dept_code, 
m_item.model,
t_serial.serial_no,
t_serial.trans_code,
t_trans_code.description AS trans_type,
t_serial.trans_no,
t_serial.trans_date,
t_serial.pur_cost,
m_location.description AS location_desc 

FROM m_item

JOIN t_serial ON m_item.code =t_serial.item_code
JOIN t_trans_code ON t_serial.trans_code=t_trans_code.code
JOIN m_department ON m_department.code=m_item.department_code
JOIN m_location ON m_location.code=t_serial.current_location

 $where  $q_item $and $q_location ORDER BY m_item.code") );
    


    }




}
