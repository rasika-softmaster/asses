<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{    
	protected $table = 'm_department';

	protected $primaryKey = 'code'; // or null
    public $incrementing = false; 


    public function Items(){
    	return $this->hasMany(Item::class,"department_code","code");
    }

}
