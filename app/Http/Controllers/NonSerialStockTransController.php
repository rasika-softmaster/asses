<?php

namespace App\Http\Controllers;

use App\Http\Controllers\LocationController;
use Auth;
use DB;
use Illuminate\Http\Request;
use Response;
use Validator;

class NonSerialStockTransController extends Controller
{
    private $tbl_itm_movement = 't_item_movement';
    private $mstrurl = 'stock_transfer_non_serial';
    private $tbl_sum = 't_transfer_other_sum';
    private $tbl_det = 't_transfer_other_det';

    public function index()
    {
        $CLST = new CustomfunctionsController;
        $LCLT = new LocationController;
        $DPST = new DepartmentController;
        $CTST = new CategoryController;
        $BRST = new BrandController;
        $SIST = new SizeController;
        $UIST = new UnitController;
        $CMAC = new createModifyAccountController;
        $SULR = new SupplierController;
        return view($this->mstrurl, [
            'pagename' => $this->mstrurl,
            'next_id' => DB::table($this->tbl_sum)->max('nno') + 1,
            'lc' => $LCLT->location_list('from_location'),
            'lcn' => $LCLT->location_list('to_location'),
            's_department' => $DPST->department_list('s_department_code'),
            's_category' => $CTST->category_list('s_category_code'),
            's_brand' => $BRST->brand_list('s_brand_code'),
            's_size' => $SIST->size_list('s_size_code'),
            's_unit' => $UIST->unit_list('s_unit_code'),
            's_supplier_code' => $SULR->supplier_list('pur_supplier'),
        ]);
    }

    public function itemsearch()
    {

        $Q = DB::table("m_item")

            ->select(

                'm_item.code',
                'm_item.description',
                'm_department.description as department_code',
                'm_category.description as category_code',
                'm_sub_category.description as subcategory_code',
                'm_brand.description as brand_code',
                'm_item.model',
                'm_size.description as size_code',
                'm_unit.description as unit_code',
                'm_item.purchase_price'
            )

            ->where('m_item.code', 'LIKE', '%' . $_POST['s_code'] . '%')
            ->where('m_item.description', 'LIKE', '%' . $_POST['s_description'] . '%')
            ->where('m_item.department_code', 'LIKE', '%' . $_POST['s_department_code'] . '%')
            ->where('m_item.category_code', 'LIKE', '%' . $_POST['s_category_code'] . '%')
            ->where('m_item.subcategory_code', 'LIKE', '%' . $_POST['subcategory_code'] . '%')
            ->where('m_item.brand_code', 'LIKE', '%' . $_POST['s_brand_code'] . '%')
            ->where('m_item.model', 'LIKE', '%' . $_POST['model'] . '%')
            ->where('m_item.size_code', 'LIKE', '%' . $_POST['s_size_code'] . '%')
            ->where('m_item.unit_code', 'LIKE', '%' . $_POST['s_unit_code'] . '%')
            ->where('m_item.purchase_price', 'LIKE', '%' . $_POST['purchase_price'] . '%')
            ->leftJoin('m_department', 'm_item.department_code', '=', 'm_department.code')
            ->leftJoin('m_category', 'm_item.category_code', '=', 'm_category.code')
            ->leftJoin('m_sub_category', 'm_item.subcategory_code', '=', 'm_sub_category.code')
            ->leftJoin('m_brand', 'm_item.brand_code', '=', 'm_brand.code')
            ->leftJoin('m_size', 'm_item.size_code', '=', 'm_size.code')
            ->leftJoin('m_unit', 'm_item.unit_code', '=', 'm_unit.code');

        //$Q = DB::select("Select * from m_item");
        //$Q = DB::select( DB::raw("Select * from m_item") );

        $a['count'] = $Q->count();
        $a['data'] = $Q->get();

        if ($Q) {
            $a['success'] = 1;
        } else {
            $a['errors'] = '0';
        }

        return $a;

    }

    public function store(Request $request)
    {
        try {

            $messages = ['code.required' => 'required', 'from_location.required' => 'required', 'to_location.required' => 'required', 'qty.required' => 'required'];

            $validator = Validator::make($request->all(), ['code' => 'required', 'from_location' => 'required', 'to_location' => 'required', 'qty' => 'required'], $messages);

            // unset($_POST['s_item_code'],$_POST['XXXX']);

            $input = $request->all();

            if ($validator->passes()) {

                $_POST['oc'] = Auth::id();
                $_POST['current_datetime'] = date('Y-m-d h:i:s');
                $is_edit = $_POST['is_edit'];

               
                if ($is_edit == 0) {

                    $sum['nno'] = $_POST['code'];
                    // $sum['item_code'] = $_POST['item_code'];
                    $sum['ddate'] = $_POST['date'];
                    $sum['from_location'] = $_POST['from_location'];
                    $sum['to_location'] = $_POST['to_location'];
                    $sum['memo'] = $_POST['note'];
                    $sum['oc'] = $_POST['oc'];
                    $sum['action_date'] = $_POST['current_datetime'];

                    DB::table($this->tbl_sum)->insert($sum);

                    //details

                    $det['nno'] = $_POST['code'];
                    $det['code'] = $_POST['item_code'];
                    $det['qty'] = $_POST['qty'];

                    DB::table($this->tbl_det)->insert($det);

                    // item move out

                    $_POST_OUT['item'] = $_POST['item_code'];
                    $_POST_OUT['location'] = $_POST['from_location'];
                    $_POST_OUT['qty_in'] = 0;
                    $_POST_OUT['qty_out'] = $_POST['qty'];
                    $_POST_OUT['trans_code'] = 125;
                    $_POST_OUT['trans_no'] = $_POST['code'];
                    $_POST_OUT['action_date'] = $_POST['current_datetime'];
                    $_POST_OUT['memo'] = $_POST['note'];
                    DB::table($this->tbl_itm_movement)->insert($_POST_OUT);

                    // item move in

                    $_POST_IN['item'] = $_POST['item_code'];
                    $_POST_IN['location'] = $_POST['to_location'];
                    $_POST_IN['qty_in'] = $_POST['qty'];
                    $_POST_IN['qty_out'] = 0;
                    $_POST_IN['trans_code'] = 125;
                    $_POST_IN['trans_no'] = $_POST['code'];
                    $_POST_IN['action_date'] = $_POST['current_datetime'];
                    $_POST_IN['memo'] = $_POST['note'];
                    $_POST_IN['trans_date'] = $_POST['date'];
                    DB::table($this->tbl_itm_movement)->insert($_POST_IN);

                    // return Response::json(['success' => '1', 'm' => "Saved Successfully"]);
                    return Response::json(['success' => '1','next_id' => DB::table($this->tbl_sum)->max('nno')+1,'m'=>'Successfuly Added']);
                  
                } else {


                    //update sum table
                 
                   
                    $sum['ddate'] = $_POST['date'];
                    $sum['from_location'] = $_POST['from_location'];
                    $sum['to_location'] = $_POST['to_location'];
                    $sum['memo'] = $_POST['note'];
                    $sum['oc'] = $_POST['oc'];
                    $sum['action_date'] = $_POST['current_datetime'];

                    DB::table($this->tbl_sum)->where('nno', $_POST['code'])->update($sum);

                    $this->delete_details($_POST['code'], 125);

                  

                    $det['nno'] = $_POST['code'];
                    $det['code'] = $_POST['item_code'];
                    $det['qty'] = $_POST['qty'];

                    DB::table($this->tbl_det)->insert($det);

                    // item move out

                    $_POST_OUT['item'] = $_POST['item_code'];
                    $_POST_OUT['location'] = $_POST['from_location'];
                    $_POST_OUT['qty_in'] = 0;
                    $_POST_OUT['qty_out'] = $_POST['qty'];
                    $_POST_OUT['trans_code'] = 125;
                    $_POST_OUT['trans_no'] = $_POST['code'];
                    $_POST_OUT['action_date'] = $_POST['current_datetime'];
                    $_POST_OUT['memo'] = $_POST['note'];
                    DB::table($this->tbl_itm_movement)->insert($_POST_OUT);

                    // item move in

                    $_POST_IN['item'] = $_POST['item_code'];
                    $_POST_IN['location'] = $_POST['to_location'];
                    $_POST_IN['qty_in'] = $_POST['qty'];
                    $_POST_IN['qty_out'] = 0;
                    $_POST_IN['trans_code'] = 125;
                    $_POST_IN['trans_no'] = $_POST['code'];
                    $_POST_IN['action_date'] = $_POST['current_datetime'];
                    $_POST_IN['memo'] = $_POST['note'];
                    $_POST_IN['trans_date'] = $_POST['date'];
                    DB::table($this->tbl_itm_movement)->insert($_POST_IN);

                  

                    // return Response::json(['success' => 2, 'm' => "Update Successfully"]);
                    return Response::json(['success' => '1','next_id' => DB::table($this->tbl_sum)->max('nno')+1,'m'=>'Update Successfully']);
                  
                }

            }else{
                return Response::json(['errors' => $validator->errors()]);
            }

          

        } catch (\Illuminate\Database\QueryException $ex) {

            $a['errors'] = $ex->errorInfo;
            $a['errors_a'] = 1;
            return $a;

        }

    }
  public function delete_details($no,$trans_code){

        //grn det table
        DB::table($this->tbl_det)->where('nno', '=', $no)->delete();

        //delete item movement

        DB::table($this->tbl_itm_movement)->where('trans_code', '=', $trans_code)->where('trans_no', '=', $no)->delete();

}
    public function loadData(Request $request)
    {

        try {

            $validator = Validator::make($request->all(), [
                'edit_id' => 'required',

            ]);

            if ($validator->passes()) {

                $edit_id = $request->edit_id;
                $records = [];
                $records["sum"] = [];
                $records["det"] = [];
                $records["sum"] = DB::table($this->tbl_sum)->where("nno", $edit_id)->get();
                $records["det"] = DB::table($this->tbl_det)->join("m_item", "m_item.code", "=", $this->tbl_det . ".code")->where("nno", $edit_id)->get();
                return Response::json(['success' => '1', 'records' => $records]);

            } else {
                return Response::json(['success' => '2', 'Message' => "Something Went Wrong Please Contact Admin", "validation" => $validator->errors()]);

            }

        } catch (\Illuminate\Database\QueryException $ex) {

            $a['Message'] = $ex->errorInfo;
            $a['errors_a'] = 1;
            return $a;

        }

    }

}
