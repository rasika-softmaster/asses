<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Http\Request;

use App\Cluster;
use Auth;

class accountCategoryController extends Controller
{

    private $mstrtbl = 'm_account_category';
    private $mstrurl = 'account_category';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        
        return view($this->mstrurl, [   'code' => DB::table($this->mstrtbl)->max('code')+1,
                                        'pagename'=>$this->mstrurl,
                                        'parent' => $this->set_acc_parent_dropdown(),
                                    ]);

        								//'category'=>$CAST->category_list('category_code'),
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        try{
        
	        $validator = Validator::make($request->all(), [
	            'description' => 'required|max:255|regex:/^[a-zA-Z0-9 ]+$/u',            
	        ]);        

	        if ($validator->passes()) {

	            $is_edit = $_POST['is_edit']; unset($_POST['is_edit']);
	            empty($_POST['parent_code']) ? $_POST['parent_code'] = 0 : $_POST['parent_code'] = $_POST['parent_code'];

	            $cdt = date('Y-m-d H:i:s');

	            if ($is_edit == 0){
	            	

	            	$_POST['created_datetime'] = $_POST['modified_datetime'] = $cdt;
	            	$_POST['created_by'] = $_POST['modified_by'] = Auth::user()->id;	            	 
	                
	                DB::table($this->mstrtbl)->insert($_POST);

	            }else{               

	            	$_POST['modified_datetime'] = $cdt;
	            	$_POST['modified_by'] = Auth::user()->id;

	                if (!$validator->passes()) { return Response::json(['errors' => $validator->errors()]); }
	                DB::table($this->mstrtbl)->where('code', $_POST['code']) ->update($_POST); 

	            }

	            return Response::json(['success' => '1','next_id' => DB::table($this->mstrtbl)->max('code')+1]);

	        }
	        
	        return Response::json(['errors' => $validator->errors()]);

	    } catch(\Illuminate\Database\QueryException $ex){ 

	    	$a['errors'] = $ex->errorInfo; 
	    	$a['errors_a'] = 1; 
            return $a;

	    }

    }

    /**
     * Display the specified resource.
     *
     
     */
    public function show(){        
        
        $Q = DB::table($this->mstrtbl)
        				->leftjoin('m_account_category as B', $this->mstrtbl.'.parent_code', '=','B.code' )
        				
        				->select(	$this->mstrtbl.'.*',        							
        							DB::Raw('IFNULL(`'.$this->mstrtbl.'`.`type`,"") as `type`') ,
        							DB::Raw('IFNULL(`B`.`description`,"") as `parent`') 
        						);


        $a['count'] = $Q->count();    
        $a['data'] = $Q->get();

        if ($Q){
            $a['success'] = 1;
        }else{
            $a['errors'] = '0';
        }

        /*$data = Cluster::all();
        $success = 1;*/

        return $a;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(){

        try{
        
            $Q = DB::table($this->mstrtbl)->where(array("code"=>$_POST['code']));
            $a['count'] = $Q->count();    
            $a['data'] = $Q->get();

            if ($Q){
                $a['success'] = 1;
            }else{
                $a['errors'] = '0';
            }

        } catch(\Illuminate\Database\QueryException $ex){ 

            $a['errors'] = $ex->errorInfo; 
            $a['errors_a'] = 1; 

        }

        return $a;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(){


        try { 
            
            if (DB::table($this->mstrtbl)->where('code', '=', $_POST['code'])->limit(1)->delete()){            
                return Response::json(['success' => '1','next_id' => DB::table($this->mstrtbl)->max('code')+1]);
            }else{
                $a['errors'] = '0';
            }
        
        } catch(\Illuminate\Database\QueryException $ex){ 
            
            $a['errors'] = $ex->errorInfo;
            $a['errors_a'] = 1;  
            // Note any method of class PDOException can be called on $ex.
        
        }



        return $a;
    }

    public function account_category_list($n='category_code'){
        $Q = DB::table('m_account_category');       

        $d  = '<select class="selectpicker" data-live-search="true" name="'.$n.'" id="'.$n.'" required>';
        $d .= '<option value=""></option>';
        if ($Q->count() > 0){
            foreach($Q->get() as $r){
                $d .= '<option value="'.$r->code.'">'.$r->description.'</option>';
            }
        }
        $d .= '</select>';

        return $d;
    }



    public function set_acc_parent_dropdown($n='parent_code'){

    	$Q = DB::table('m_account_category');       

        $d  = '<select class="selectpicker" data-live-search="true" name="'.$n.'" id="'.$n.'">';
        $d .= '<option value=""></option>';
        if ($Q->count() > 0){
            foreach($Q->get() as $r){
                $d .= '<option value="'.$r->code.'">'.$r->description.'</option>';
            }
        }
        $d .= '</select>';

        return $d;	

    }


    public function set_parent_dd(){
    	return $this->set_acc_parent_dropdown('parent_code');
    }






}