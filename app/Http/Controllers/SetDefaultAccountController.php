<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Http\Request;

use App\Cluster;
use Auth;

class SetDefaultAccountController extends Controller
{

    private $mstrtbl = 'm_account_default';
    private $mstrurl = 'set_default_account';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

    	$CMAC = new createModifyAccountController;
        
        return view($this->mstrurl, [   'pagename'=>$this->mstrurl,
                                        'code' => $this->max_code(),
                                        'account' => $CMAC->account_list('account_code'),
                                    ]);        							
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        try{
        
	        $validator = Validator::make($request->all(), [
	            'code' => 'required|regex:/^[0-9]+$/u',
	            'def_acc_code' => 'required|regex:/^[a-zA-Z0-9_]+$/u|max:25',
	            'description' => 'required|max:255|regex:/^[a-zA-Z0-9 ]+$/u',            
	            'account_code' => 'required|regex:/^[0-9]+$/u',
	        ]);        

	        if ($validator->passes()) {

	            $is_edit 	= $_POST['is_edit']; unset($_POST['is_edit']);
	            $cdt 		= date('Y-m-d H:i:s');

	            $_POST['def_acc_code'] = strtoupper($_POST['def_acc_code']);

	            if ($is_edit == 0){	            	
	            	$_POST['created_datetime'] = $_POST['modified_datetime'] = $cdt;
	            	$_POST['created_by'] = $_POST['modified_by'] = Auth::user()->id;
	                DB::table($this->mstrtbl)->insert($_POST);
	            }else{
	            	$_POST['modified_datetime'] = $cdt;
	            	$_POST['modified_by'] = Auth::user()->id;

	                if (!$validator->passes()) { return Response::json(['errors' => $validator->errors()]); }
	                DB::table($this->mstrtbl)->where('code', $_POST['code']) ->update($_POST);
	            }

	            return Response::json(['success' => '1','next_id' => $this->max_code()]);

	        }
	        
	        return Response::json(['errors' => $validator->errors()]);

	    } catch(\Illuminate\Database\QueryException $ex){ 

	    	$a['errors'] = $ex->errorInfo; 
	    	$a['errors_a'] = 1; 
            return $a;

	    }

    }

    /**
     * Display the specified resource.
     *
     
     */
    public function show(){        
        
        $Q = DB::table($this->mstrtbl)

        				->join('m_account',$this->mstrtbl.'.account_code','m_account.code')
        				->select($this->mstrtbl.'.*','m_account.description as account_desc')
        ;
        				
        				
        $a['count'] = $Q->count();    
        $a['data'] = $Q->get();

        if ($Q){
            $a['success'] = 1;
        }else{
            $a['errors'] = '0';
        }        

        return $a;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(){

        try{
        
            $Q = DB::table($this->mstrtbl)->where(array("code"=>$_POST['code']));
            $a['count'] = $Q->count();    
            $a['data'] = $Q->get();

            if ($Q){
                $a['success'] = 1;
            }else{
                $a['errors'] = '0';
            }

        } catch(\Illuminate\Database\QueryException $ex){ 

            $a['errors'] = $ex->errorInfo; 
            $a['errors_a'] = 1; 

        }

        return $a;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(){


        try { 
            
            if (DB::table($this->mstrtbl)->where('code', '=', $_POST['code'])->limit(1)->delete()){            
                return Response::json(['success' => '1','next_id' => DB::table($this->mstrtbl)->max('code')+1]);
            }else{
                $a['errors'] = '0';
            }
        
        } catch(\Illuminate\Database\QueryException $ex){ 
            
            $a['errors'] = $ex->errorInfo;
            $a['errors_a'] = 1;  
            // Note any method of class PDOException can be called on $ex.
        
        }



        return $a;
    }

    
    public function max_code(){
    	return DB::table($this->mstrtbl)->max('code')+1;
    }
    


}
