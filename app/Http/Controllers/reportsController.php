<?php

namespace App\Http\Controllers;

use App\FaRegistry;
use App\FaTransfer;
use App\Item;
use PDF;

class reportsController extends Controller
{

    private $mstrurl = 'rpt_reports';

    public function index()
    {
        $ITMC = new ItemController;
        $LOCT = new LocationController;
        return view($this->mstrurl, ['pagename' => $this->mstrurl,
            's_item' => $ITMC->item_list('q_item_code'),
            's_location' => $LOCT->location_list('q_location_code'),

        ]);
    }

    public function headerPage()
    {
        PDF::SetFont('helvetica', 'B', 18);
        PDF::MultiCell(200, 1, 'Fixed Assets -Kandyan Reach Hotel', $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

        PDF::SetFont('helvetica', '', 8);
		PDF::MultiCell(50,1, 'Address:', '0', 'L', false, 1, '', '', true, 0, false, true, 0, 'M', false);
		PDF::MultiCell(50,1, 'Contact:', '0', 'L', false, 1, '', '', true, 0, false, true, 0, 'M', false);
		PDF::MultiCell(50,1, 'Email:', '0', 'L', false, 1, '', '', true, 0, false, true, 0, 'M', false);
		PDF::MultiCell(100,1, '', 'B', 'L', false, 1, '', '', true, 0, false, true, 0, 'M', false);
    }
    public function generate()
    {

        if (($_POST['rpt_selected'] == "rpt_item_serial") && isset($_POST['rpt_selected_all'])) {

            $data = FaRegistry::ItemSerialAll();
            PDF::SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0.1, 'color' => array(0, 0, 0)));

            PDF::SetTitle(str_replace('_', ' ', $_POST['rpt_selected']));
            PDF::AddPage("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
            PDF::SetFont('helvetica', '', 8);
            $this->headerPage();

            PDF::SetFont('helvetica', 'B', 20);
            PDF::MultiCell(100, 20, 'Asset Items Registry', $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
            PDF::ln();
            PDF::SetFont('helvetica', 'B', 10);
            $heigh = 6 * (max(1, PDF::getNumLines('Trans Date', 20), PDF::getNumLines('Current Location', 30)));

            PDF::MultiCell(20, $heigh, 'Item Code', $border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
            PDF::MultiCell(40, $heigh, 'Description', $border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
            PDF::MultiCell(25, $heigh, 'Department', $border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
            PDF::MultiCell(35, $heigh, 'Model', $border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

            PDF::MultiCell(40, $heigh, 'Serial No', $border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
            PDF::MultiCell(25, $heigh, 'Trans Type', $border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
            PDF::MultiCell(20, $heigh, 'Trans No', $border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
            PDF::MultiCell(20, $heigh, 'Trans Date', $border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
            PDF::MultiCell(20, $heigh, 'Cost', $border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
            PDF::MultiCell(30, $heigh, 'Current Location', $border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

            PDF::SetFont('helvetica', '', 9);

            $s_item = '';

            foreach ($data as $key => $value) {

                $heigh = 6 * (max(1, PDF::getNumLines($value->description, 40), PDF::getNumLines($value->serial_no, 40)));
                PDF::HaveMorePages($heigh);
                PDF::MultiCell(20, $heigh, $value->code, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                PDF::MultiCell(40, $heigh, $value->description, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                PDF::MultiCell(25, $heigh, $value->dept_code, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                PDF::MultiCell(35, $heigh, $value->model, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

                PDF::MultiCell(40, $heigh, $value->serial_no, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

                PDF::MultiCell(25, $heigh, $value->trans_type, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                PDF::MultiCell(20, $heigh, $value->trans_no, $border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                PDF::MultiCell(20, $heigh, $value->trans_date, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                PDF::MultiCell(20, $heigh, $value->pur_cost, $border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                PDF::MultiCell(30, $heigh, $value->location_desc, $border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

            }

            PDF::Output($_POST['rpt_selected'] . '.pdf');

        } else if (isset($_POST['rpt_selected'])) {

            switch ($_POST['rpt_selected']) {

                case 'rpt_item_list':

                    $item = Item::Reportx();

                    PDF::SetTitle(str_replace('_', ' ', $_POST['rpt_selected']));
                    PDF::AddPage("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
                    PDF::SetFont('helvetica', '', 8);

                    $s_dept = '';
					$this->headerPage();
                    PDF::SetFont('helvetica', 'B', 20);
                    PDF::MultiCell(100, 0, 'Item List', $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

                    foreach ($item as $key => $value) {
                        $qty = Item::ItemSerialCount($value->code);
                        if ($s_dept != $value->dept_desc) {

                            PDF::ln();
                            PDF::SetFont('helvetica', 'B', 14);
                            PDF::MultiCell(200, 0, $value->dept_desc, $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                            PDF::SetFont('helvetica', 'B', 8);

                            PDF::MultiCell(10, 0, 'Code', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                            PDF::MultiCell(30, 0, 'Description', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

                            PDF::MultiCell(30, 0, 'Category', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                            PDF::MultiCell(24, 0, 'Sub Category', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                            PDF::MultiCell(20, 0, 'Brand', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                            PDF::MultiCell(35, 0, 'Model', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                            PDF::MultiCell(10, 0, 'Size', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                            PDF::MultiCell(10, 0, 'Unit', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                            PDF::MultiCell(15, 0, 'Qty', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                            PDF::MultiCell(25, 0, 'Purchase Price', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                            PDF::MultiCell(15, 0, 'Inactive', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                            PDF::MultiCell(20, 0, 'FA Acc', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                            PDF::MultiCell(20, 0, 'Dep Acc', $border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                            PDF::SetFont('helvetica', '', 8);
                            $s_dept = $value->dept_desc;
                        }

                        $heigh = 5 * (max(1, PDF::getNumLines($value->description, 30), PDF::getNumLines($value->cate_desc, 30)));
                        PDF::HaveMorePages($heigh);

                        PDF::MultiCell(10, $heigh, $value->code, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(30, $heigh, $value->description, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

                        PDF::MultiCell(30, $heigh, $value->cate_desc, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(24, $heigh, $value->sub_cate_desc, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(20, $heigh, $value->brand_desc, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(35, $heigh, $value->model, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(10, $heigh, $value->size_desc, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(10, $heigh, $value->unit_code, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(15, $heigh, $qty, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(25, $heigh, $value->purchase_price, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(15, $heigh, $value->inactive, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(20, $heigh, $value->fa_acc, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(20, $heigh, $value->dip_acc, $border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

                    }

                    PDF::Output($_POST['rpt_selected'] . '.pdf');
                    break;

                case 'rpt_item_serial':

                    $data = FaRegistry::ItemSerial();

                    PDF::SetTitle(str_replace('_', ' ', $_POST['rpt_selected']));
                    PDF::AddPage("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
                    PDF::SetFont('helvetica', '', 8);
					$this->headerPage();
                    PDF::SetFont('helvetica', 'B', 20);
                    PDF::MultiCell(100, 0, 'Asset Items Registry', $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                    PDF::ln();
                    PDF::SetFont('helvetica', 'B', 8);
                    $heigh = 6 * (max(1, PDF::getNumLines('Trans Date', 20), PDF::getNumLines('Current Location', 30)));

                    PDF::MultiCell(20, $heigh, 'Item Code', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                    PDF::MultiCell(40, $heigh, 'Description', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                    PDF::MultiCell(25, $heigh, 'Department', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                    PDF::MultiCell(35, $heigh, 'Model', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                    PDF::MultiCell(40, $heigh, 'Serial No', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                    PDF::MultiCell(25, $heigh, 'Trans Type', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                    PDF::MultiCell(20, $heigh, 'Trans No', $border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                    PDF::MultiCell(20, $heigh, 'Trans Date', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                    PDF::MultiCell(20, $heigh, 'Cost', $border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                    PDF::MultiCell(30, $heigh, 'Current Location', $border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

                    PDF::SetFont('helvetica', '', 9);

                    $s_item = '';

                    foreach ($data as $key => $value) {

                        //if ($s_item != $value->code){
                        $heigh = 6 * (max(1, PDF::getNumLines($value->description, 40), PDF::getNumLines($value->serial_no, 40)));
                        PDF::HaveMorePages($heigh);
                        PDF::MultiCell(20, $heigh, $value->code, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(40, $heigh, $value->description, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(25, $heigh, $value->dept_code, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(35, $heigh, $value->model, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(40, $heigh, $value->serial_no, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(25, $heigh, $value->trans_type, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(20, $heigh, $value->trans_no, $border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(20, $heigh, $value->trans_date, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(20, $heigh, $value->pur_cost, $border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(30, $heigh, $value->location_desc, $border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

                    }

                    PDF::Output($_POST['rpt_selected'] . '.pdf');

                    break;
                case 'rpt_item_transfer':

                    $data = faTransfer::TransferRpt();

                    PDF::SetTitle(str_replace('_', ' ', $_POST['rpt_selected']));
                    PDF::AddPage("L", PDF_UNIT, 'A4', true, 'UTF-8', false);
                    PDF::SetFont('helvetica', '', 8);
					$this->headerPage();
                    PDF::SetFont('helvetica', 'B', 20);
                    PDF::MultiCell(100, 0, 'Item Transfer', $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                    PDF::SetFont('helvetica', '', 8);
                    PDF::MultiCell(100, 0, 'From :' . $_POST['fd'] . ' To :' . $_POST['td'], $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

                    $s_item = '';

                    foreach ($data as $key => $value) {

                        if ($s_item != $value->code) {

                            PDF::ln();
                            PDF::SetFont('helvetica', 'B', 12);
                            PDF::MultiCell(0, 0, "SNo. " . $value->serial_no . " - " . $value->description . " - " . $value->model . " (" . $value->code . ")", $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

                            PDF::SetFont('helvetica', 'B', 8);

                            PDF::MultiCell(25, 0, 'Trans Type', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                            PDF::MultiCell(20, 0, 'Trans No', $border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                            PDF::MultiCell(20, 0, 'Trans Date', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                            PDF::MultiCell(50, 0, 'Location', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                            PDF::MultiCell(20, 0, 'Status', $border = '1', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                            PDF::SetFont('helvetica', '', 8);
                            $s_item = $value->code;
                        }

                        PDF::MultiCell(25, 0, $value->trans_type, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(20, 0, $value->trans_no, $border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(20, 0, $value->trans_date, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
                        PDF::MultiCell(50, 0, $value->location_desc, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

                        if ($value->qty_in == 1) {
                            $transaction = 'IN';
                        } else {
                            $transaction = 'OUT';
                        }

                        PDF::MultiCell(20, 0, $transaction, $border = '1', $align = 'C', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

                    }

                    PDF::Output($_POST['rpt_selected'] . '.pdf');

                    break;

                case 'r_pur_summary':

                    $RPC = new PurchaseReportsController;
                    $RPC->report_summary($_POST['rpt_selected'], $_POST['fd'], $_POST['td']);

                    break;
                case 'r_pur_details':

                    $RPC = new PurchaseReportsController;
                    $RPC->report_detail($_POST['rpt_selected'], $_POST['fd'], $_POST['td']);

                    break;
                case 'rpt_stock_in_hand':

                    $RPC = new StockReportController;
                    $RPC->report_stock_in_hand($_POST['rpt_selected'], $_POST['q_location_code']);

                    break;

                default:
                    echo 'not selected';

            }

        } else {
            echo "<script> alert('Please select a report'); close() </script>";
        }

    }

}
