<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;

class ItemDamageController extends Controller
{

    private $tbl_sum = 't_item_damage_sum';
    private $tbl_det = 't_item_damage_det';
    private $mstrurl = 'item_damage';
    private $tbl_itm_movement = 't_item_movement';
    public function index()
    {

        $STCL = new StoresController;

        return view($this->mstrurl, [
            'next_id' => DB::table($this->tbl_sum)->max('nno') + 1,
            'store' => $STCL->store_list(),
            'pagename' => $this->mstrurl,
        ]);
    }
    public function itemsearch()
    {

        $terms = $_POST['s_code'];
        $limit = 10;

        $Q =
        DB::table("m_item")->select('m_item.code', 'm_item.description', 'm_item.model')
            ->where(function ($query) use ($terms) {
                $query->orWhere('m_item.code', 'LIKE', '%' . $terms . '%')
                    ->orWhere('m_item.description', 'LIKE', '%' . $terms . '%')
                    ->orWhere('m_item.model', 'LIKE', '%' . $terms . '%');
            })->limit($limit);
        $a['count'] = $limit;

        $a['data'] = $Q->get();

        if ($Q) {
            $a['success'] = 1;
        } else {
            $a['errors'] = '0';
        }

        return $a;

    }
    public function store(Request $request)
    {

        try {

            $validator = Validator::make($request->all(), [

                'description' => 'required',
                'store_code' => 'required',
                'no' => 'required',
                'date' => 'required',

            ]);

            if ($validator->passes()) {

                // dd($request->all());
                $is_edit =  $request->is_edit; 
                //first saving record
                if ($is_edit == 0) {
                    //save sum table

                    $sum['nno'] = $_POST['no'];
                    $sum['ddate'] = $_POST['date'];
                    $sum['description'] = $_POST['description'];
                    $sum['to_location'] = $_POST['store_code'];
                    $sum['oc'] = Auth::user()->id;

                    DB::table($this->tbl_sum)->insert($sum);
                    for ($i = 0; $i < 25; $i++) {
                        //save to details table
                        if (isset($_POST['code_' . $i]) && $_POST['code_' . $i] != "") {

                            //grn_details
                            $det['nno'] = $_POST['no'];
                            $det['item'] = $_POST['code_' . $i];
                            $det['qty'] = $_POST['qty_' . $i];

                            DB::table($this->tbl_det)->insert($det);

                            //item movement
                            $_movement['item'] = $_POST['code_' . $i];
                            $_movement['location'] = $_POST['store_code'];
                            $_movement['trans_code'] = 14;
                            $_movement['trans_no'] = $_POST['no'];
                            $_movement['qty_in'] = $_POST['qty_' . $i];
                            $_movement['qty_out'] = 0;
                            $_movement['memo'] = $_POST['description'];
                            $_movement['trans_date'] = $_POST['date'];

                            DB::table($this->tbl_itm_movement)->insert($_movement);
                        }

                        //save to sum table

                    }
                    return Response::json(['success' => '1', 'next_id' => DB::table($this->tbl_sum)->max('nno') + 1,'message'=>'Successfuly Added']);
                    //edit records
                } else {

                    //update sum table
                  
                    $sum['ddate'] = $_POST['date'];
                    $sum['description'] = $_POST['description'];
                    $sum['to_location'] = $_POST['store_code'];
                    $sum['oc'] = Auth::user()->id;

                    DB::table($this->tbl_sum)->where('nno',$_POST['no'])->update($sum);
                    $this->delete_details($_POST['no'],14);
                    for ($i = 0; $i < 25; $i++) {
                        //save to details table
                        if (isset($_POST['code_' . $i]) && $_POST['code_' . $i] != "") {

                            //grn_details
                            $det['nno'] = $_POST['no'];
                            $det['item'] = $_POST['code_' . $i];
                            $det['qty'] = $_POST['qty_' . $i];

                            DB::table($this->tbl_det)->insert($det);

                            //item movement
                            $_movement['item'] = $_POST['code_' . $i];
                            $_movement['location'] = $_POST['store_code'];
                            $_movement['trans_code'] = 14;
                            $_movement['trans_no'] = $_POST['no'];
                            $_movement['qty_in'] = $_POST['qty_' . $i];
                            $_movement['qty_out'] = 0;
                            $_movement['memo'] = $_POST['description'];
                            $_movement['trans_date'] = $_POST['date'];

                            DB::table($this->tbl_itm_movement)->insert($_movement);
                        }
                    }
                    return Response::json(['success' => '1','next_id' => DB::table($this->tbl_sum)->max('nno')+1,'message'=>'Successfuly Updated']);
                  

                }

              

            }
            return Response::json(['success' => '2', 'Message' => "Something Went Wrong Please Contact Admin", "validation" => $validator->errors()]);

        } catch (\Illuminate\Database\QueryException $ex) {

            $a['Message'] = $ex->errorInfo;
            $a['errors_a'] = 1;
            return $a;

        }

    }
    public function delete_details($no,$trans_code){

        //grn det table
        DB::table($this->tbl_det)->where('nno', '=', $no)->delete();

        //delete item movement

        DB::table($this->tbl_itm_movement)->where('trans_code', '=', $trans_code)->where('trans_no', '=', $no)->delete();

}
public function loadData(Request $request){

    try{
        


    

        $validator = Validator::make($request->all(), [
            'edit_id' => 'required'
           
        
        ]);        

        if ($validator->passes()) {

            $edit_id = $request->edit_id;
            $records=[];
            $records["sum"]=[];
            $records["det"]=[];
            $records["sum"]= DB::table($this->tbl_sum)->where("nno",$edit_id)->get();
            $records["det"]= DB::table($this->tbl_det)->join("m_item","m_item.code","=",$this->tbl_det.".item")->where("nno",$edit_id)->get();
            return Response::json(['success' => '1','records' => $records]);

        }else{
            return Response::json(['success' => '2','Message' =>"Something Went Wrong Please Contact Admin","validation"=> $validator->errors()]);
       
        }
        

    } catch(\Illuminate\Database\QueryException $ex){ 

        $a['Message'] = $ex->errorInfo; 
        $a['errors_a'] = 1; 
        return $a;

    }

}

}
