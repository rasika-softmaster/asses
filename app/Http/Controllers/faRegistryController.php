<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CustomfunctionsController;
use App\Http\Controllers\LocationController;
use Auth;


class faRegistryController extends Controller
{

    private $tbl_sum = 't_serial';
    private $tbl_det = 't_serial_movement';
    private $mstrurl = 'fa_registry';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        
        $CLST = new CustomfunctionsController;
        $LCLT = new LocationController;
        $DPST = new DepartmentController;
        $CTST = new CategoryController;
        $BRST = new BrandController;
        $SIST = new SizeController;
        $UIST = new UnitController;
        $CMAC = new createModifyAccountController;
        $SULR = new SupplierController;
        
        return view($this->mstrurl, [

            'code' => DB::table($this->tbl_sum)->max('item_code')+1 ,
            'lc' => $LCLT->location_list(),
            'pagename' => $this->mstrurl,            
            
            's_department'=>$DPST->department_list('s_department_code'),
            's_category'=>$CTST->category_list('s_category_code'),
            's_brand'=>$BRST->brand_list('s_brand_code'),
            's_size'=>$SIST->size_list('s_size_code'),
            's_unit'=>$UIST->unit_list('s_unit_code'),
            's_trans_code'=>$this->trans_code_dd(),
            's_pur_type'=>$this->trans_code_dd('pur_type'),
            's_supplier_code'=>$SULR->supplier_list('pur_supplier'),
            'tr_no'=>$this->get_max_tr_no(2),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    	try {

            
            $messages = [
                
                'item_code.required' => 'required',
                'trans_code.required' => 'required',
                'trans_no.required' => 'required',
                'serial_no.required' => 'required',
                'location_code.required' => 'required',
                'pur_type.required' => 'required',
                'pur_no.required' => 'required',
                'pur_date.required' => 'required',
                'pur_supplier.required' => 'required',
                'pur_cost.required' => 'required',
                
                'dep_rate.required' => 'required',
                'dep_period.required' => 'required',
                'dep_value.required' => 'required',
                'dep_opb_date.required' => 'required',
                'dep_month.required' => 'required',
                'dep_acc_dep.required' => 'required',
                'dep_opb_value.required' => 'required',
                'dep_nbv.required' => 'required',                
                
                'cap_rate.required' => 'required',
                'cap_period.required' => 'required',
                'cap_n_years.required' => 'required',
                'cap_total.required' => 'required',
                'cap_for_year.required' => 'required',
                'cap_balance.required' => 'required',
                'cap_op_amount.required' => 'required',
                'cap_temp_diff.required' => 'required',
                
            ];

                
            $validator = Validator::make($request->all(), [                
                'item_code' => 'required',
                'trans_code' => 'required',
                'trans_no' => 'required',
                'serial_no' => 'required',
                'location_code' => 'required',              
                

                'pur_type' => 'required',
                'pur_no' => 'required',
                'pur_date' => 'required',
                'pur_supplier' => 'required',
                'pur_cost' => 'required',


                'dep_rate' => 'required',
                'dep_period' => 'required',
                'dep_value' => 'required',
                'dep_opb_date' => 'required',
                'dep_month' => 'required',
                'dep_acc_dep' => 'required',
                'dep_opb_value' => 'required',
                'dep_nbv' => 'required',
                
                'cap_rate' => 'required',
                'cap_period' => 'required',
                'cap_n_years' => 'required',
                'cap_total' => 'required',
                'cap_for_year' => 'required',
                'cap_balance' => 'required',
                'cap_op_amount' => 'required',
                'cap_temp_diff' => 'required',

                
                
                
            ],$messages);

            unset($_POST['s_item_code'],$_POST['XXXX']);

            $input = $request->all();

            if ($validator->passes()) {

                $_POST['oc'] = Auth::id();
                
                $is_edit = $_POST['is_edit']; unset($_POST['is_edit']);

                if ($is_edit == 0){
                    
                    $_POST['trans_no'] = $this->get_max_tr_no($_POST['trans_code']);
                    $_POST['current_location'] = $_POST['location_code'];
                    
                    DB::table($this->tbl_sum)->insert($_POST);

                    unset($_POST['oc'],$_POST['trans_cost'],$_POST['supplier_code'], $_POST['pur_type'], $_POST['pur_no'], $_POST['pur_date'], $_POST['pur_supplier'], $_POST['pur_cost'], $_POST['dep_rate'], $_POST['dep_period'], $_POST['dep_value'], $_POST['dep_opb_date'], $_POST['dep_month'], $_POST['dep_acc_dep'], $_POST['dep_opb_value'], $_POST['dep_nbv'], $_POST['cap_rate'], $_POST['cap_period'], $_POST['cap_n_years'], $_POST['cap_total'], $_POST['cap_for_year'], $_POST['cap_balance'], $_POST['cap_op_amount'], $_POST['cap_temp_diff'],$_POST['current_location'] );

                    $_POST['qty_in'] = 1;
                    $_POST['qty_out'] = 0;

                    DB::table($this->tbl_det)->insert($_POST);

                }else{
                    
                    if (!$validator->passes()) { return Response::json(['errors' => $validator->errors()]); }                    

                    if (isset($_POST['inactive'])){
                        $_POST['inactive'] = 1;
                    }else{
                        $_POST['inactive'] = 0;
                    }

                    unset($_POST['item_code'],$_POST['location_code']);
                    
                    DB::table($this->tbl_sum)   ->where('trans_code', $_POST['trans_code'])
                                                ->where('trans_no', $_POST['trans_no'])
                                                ->update($_POST);

                }

                return Response::json(['success' => '1','next_id' => '']);

            }
            
            return Response::json(['errors' => $validator->errors()]);

        } catch(\Illuminate\Database\QueryException $ex){ 

            $a['errors'] = $ex->errorInfo; 
            $a['errors_a'] = 1; 
            return $a;

        }

    }

    /**
     * Display the specified resource.
     *
     
     */
    public function show(){        
        $Q = DB::table($this->tbl_sum)->orderby('code');
        $a['count'] = $Q->count();    
        $a['data'] = $Q->get();

        if ($Q){
            $a['success'] = 1;
        }else{
            $a['errors'] = '0';
        }

        return $a;        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(){
        
        $Q = DB::table($this->tbl_sum)->where(array("code"=>$_POST['code']));
        $a['count'] = $Q->count();    
        $a['data'] = $Q->get();

        if ($Q){
            $a['success'] = 1;
        }else{
            $a['errors'] = '0';
        }

        return $a;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(){
        if (DB::table($this->tbl_sum)->where('code', '=', $_POST['code'])->limit(1)->delete()){            
            return Response::json(['success' => '1','next_id' => DB::table($this->tbl_sum)->max('code')+1]);
        }else{
            $a['errors'] = '0';
        }

        return $a;
    }





    public function itemsearch(){


$Q = DB::table("m_item")

->select(
'm_item.code',
'm_item.description',
'm_department.description as department_code',
'm_category.description as category_code',
'm_sub_category.description as subcategory_code',
'm_brand.description as brand_code',
'm_item.model',
'm_size.description as size_code',
'm_unit.description as unit_code',
'm_item.purchase_price')

->where('m_item.code','LIKE','%'.$_POST['s_code'].'%')
->where('m_item.description','LIKE','%'.$_POST['s_description'].'%')
->where('m_item.department_code','LIKE','%'.$_POST['s_department_code'].'%')
->where('m_item.category_code','LIKE','%'.$_POST['s_category_code'].'%')
->where('m_item.subcategory_code','LIKE','%'.$_POST['subcategory_code'].'%')
->where('m_item.brand_code','LIKE','%'.$_POST['s_brand_code'].'%')
->where('m_item.model','LIKE','%'.$_POST['model'].'%')
->where('m_item.size_code','LIKE','%'.$_POST['s_size_code'].'%')
->where('m_item.unit_code','LIKE','%'.$_POST['s_unit_code'].'%')
->where('m_item.purchase_price','LIKE','%'.$_POST['purchase_price'].'%')

->leftJoin('m_department','m_item.department_code','=','m_department.code')
->leftJoin('m_category','m_item.category_code','=','m_category.code')
->leftJoin('m_sub_category','m_item.subcategory_code','=','m_sub_category.code')
->leftJoin('m_brand','m_item.brand_code','=','m_brand.code')
->leftJoin('m_size','m_item.size_code','=','m_size.code')
->leftJoin('m_unit','m_item.unit_code','=','m_unit.code');





//->where('m_item.inactive','LIKE','%'.$_POST['inactive'].'%')
//->where('m_item.is_fa','LIKE','%'.$_POST['is_fa'].'%')
//->where('m_item.dip_period','LIKE','%'.$_POST['dip_period'].'%')
//->where('m_item.dip_per','LIKE','%'.$_POST['dip_per'].'%')
//->where('m_item.is_batch','LIKE','%'.$_POST['is_batch'].'%')
//->where('m_item.fa_acc','LIKE','%'.$_POST['fa_acc'].'%')
//->where('m_item.dip_acc','LIKE','%'.$_POST['dip_acc'].'%')
//->where('m_item.ca_period','LIKE','%'.$_POST['ca_period'].'%')
//->where('m_item.ca_per','LIKE','%'.$_POST['ca_per'].'%');

    	$a['count'] = $Q->count();    
        $a['data'] = $Q->get();

        if ($Q){
            $a['success'] = 1;
        }else{
            $a['errors'] = '0';
        }

        return $a;


    //where('email', Input::get('email'))->andWhere('name', 'like', '%' . Input::get('name') . '%');

    //$query = User::where('name', 'LIKE', '%' . $term . '%');



    	/*$s = "SELECT 
  * 
FROM
  `m_item` I 
WHERE (
    I.code LIKE '%$code%' AND I.description LIKE '%$description%' AND I.department_code LIKE '%$department_code%' AND I.category_code LIKE '%$category_code%' AND I.subcategory_code LIKE '%$subcategory_code%' AND I.brand_code LIKE '%$brand_code%' AND I.model LIKE '%$model%' AND I.size_code LIKE '%$size_code%' AND I.unit_code LIKE '%$unit_code%' AND I.purchase_price LIKE '%$purchase_price%' AND I.inactive LIKE '%$inactive%' AND I.is_fa LIKE '%$is_fa%' AND I.dip_period LIKE '%$dip_period%' AND I.dip_per LIKE '%$dip_per%' AND I.is_batch LIKE '%$is_batch%' AND I.fa_acc LIKE '%$fa_acc%' AND I.dip_acc LIKE '%$dip_acc%' AND I.ca_period LIKE '%$ca_period%' AND I.ca_per LIKE '%$ca_per%'
  ) ";
*/
    }








public function trans_code_dd($n='trans_code'){
        
    $Q = DB::table("t_trans_code");

    $d  = '<select class="selectpicker" data-live-search="true" name="'.$n.'" id="'.$n.'" required>';
    $d .= '<option value=""></option>';
    if ($Q->count() > 0){
        foreach($Q->get() as $r){
            $d .= '<option value="'.$r->code.'">'.$r->description.'</option>';
        }
    }
    $d .= '</select>';

    return $d;    

}

public function get_max_tr_no($trans_code){

    return DB::table($this->tbl_sum)->where('trans_code',$trans_code)->max('trans_no')+1;

}

public function checkSerialExit(){

    $Q = DB::table("t_serial")->where("serial_no",$_POST['serial_no']);

    if ( $Q->count() > 0){
        $a['s'] = 1;
    }else{
        $a['s'] = 0;
    }

    echo json_encode($a);

}
    



}
