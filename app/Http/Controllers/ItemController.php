<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Http\Request;

use App\Cluster;
use Illuminate\Support\Facades\Input;

class ItemController extends Controller
{

    private $mstrtbl = 'm_item';
    private $mstrurl = 'item';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        
        $DPST = new DepartmentController;
        $CTST = new CategoryController;
        $BRST = new BrandController;
        $SIST = new SizeController;
        $UIST = new UnitController;
        $CMAC = new createModifyAccountController;
        
        return view($this->mstrurl, [   'code' => $this->max_code(),
                                        'pagename'=>$this->mstrurl,
        								'department'=>$DPST->department_list('department_code'),
        								'category'=>$CTST->category_list('category_code'),
                                        'brand'=>$BRST->brand_list('brand_code'),
                                        'size'=>$SIST->size_list('size_code'),
                                        'unit'=>$UIST->unit_list('unit_code'),
                                        'fa_acc' => $CMAC->account_list('fa_acc'),
                                        'dip_acc' => $CMAC->account_list('dip_acc'),
                                        'q_department'=>$DPST->department_list('q_department_code'),
                                        'q_category'=>$CTST->category_list('q_category_code'),
                                        'q_brand'=>$BRST->brand_list('q_brand_code'),
                                        'q_size'=>$SIST->size_list('q_size_code'),
                                        'q_unit'=>$UIST->unit_list('q_unit_code'),
                                    ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function upload_image($i,$code,$action){

        if ($i == 'undefined'){
            unset($_POST['image']);                
        }else{

            $img_name = $code.".". Input::file('image')->getClientOriginalExtension();

            if ($action == 'update' ){          

                if (file_exists(public_path()."\img". $img_name)){
                    unlink(public_path()."\img". $img_name);
                }
            }            
            
            Input::file('image')->move(public_path()."\img", $img_name);

        }        
        
    }


    public function store(Request $request)
    {
        
        try{



            
        
	        $validator = Validator::make($request->all(), [
	            'description' => 'required|max:255|regex:/^[a-zA-Z0-9 ]+$/u',            
                'department_code' => 'required',
                'category_code' => 'required',
                'subcategory_code' => 'required',
                'brand_code' => 'required',
                'model' => 'required',
                'purchase_price' => 'required|regex:/^\d*(\.\d{1,2})?$/',

                'dip_period' => 'required',
                'dip_per' => 'required',
                'ca_period' => 'required',
                'ca_per' => 'required',
	        ]);        

	        if ($validator->passes()) {

	            $_POST['oc'] = 1;
	            $is_edit = $_POST['is_edit']; unset($_POST['is_edit']);
                $_POST['inactive'] = isset($_POST['inactive']) ? 1 : 0;
                $_POST['is_batch'] = isset($_POST['is_batch']) ? 1 : 0;
                $_POST['is_fa'] = isset($_POST['is_fa']) ? 1 : 0;

	            if ($is_edit == 0){
                    $_POST['code'] = $this->max_code();
                    $this->upload_image($request->image,$_POST['code'],'insert');
                    DB::table($this->mstrtbl)->insert($_POST);
	            }else{               

	                if (!$validator->passes()) { return Response::json(['errors' => $validator->errors()]); }
                    $this->upload_image($request->image,$_POST['code'],'update');
                    DB::table($this->mstrtbl)->where('code', $_POST['code']) ->update($_POST); 

	            }

	            return Response::json(['success' => '1','next_id' => DB::table($this->mstrtbl)->max('code')+1]);

	        }
	        
	        return Response::json(['errors' => $validator->errors()]);

	    } catch(\Illuminate\Database\QueryException $ex){ 

	    	$a['errors'] = $ex->errorInfo; 
	    	$a['errors_a'] = 1; 
            return $a;

	    }

    }

    /**
     * Display the specified resource.
     *
     
     */
    public function show(){        
        

        $Q = DB::table($this->mstrtbl)
                ->join('m_department', $this->mstrtbl.'.department_code', '=', 'm_department.code')                
                ->select($this->mstrtbl.'.*', 'm_department.description as dept_desc');
                


        //$Q = DB::table($this->mstrtbl);
        

        $a['count'] = $Q->count();    
        $a['data'] = $Q->get();

        if ($Q){
            $a['success'] = 1;
        }else{
            $a['errors'] = '0';
        }

        /*$data = Cluster::all();
        $success = 1;*/
        return $a;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(){

        try{
        
            $Q = DB::table($this->mstrtbl)->where(array("code"=>$_POST['code']));
            $a['count'] = $Q->count();    
            $a['data'] = $Q->get();
            $a['item_img'] = $this->get_item_img($_POST['code']);
            $a['public_path'] = public_path('img');

            if ($Q){
                $a['success'] = 1;
            }else{
                $a['errors'] = '0';
            }

        } catch(\Illuminate\Database\QueryException $ex){ 

            $a['errors'] = $ex->errorInfo; 
            $a['errors_a'] = 1; 

        }

        return $a;
    }



    public function get_item_img($code){

        if (file_exists(public_path()."/img/". $code.".jpg")){
            return $code;
        }else{
            return '';
        }

    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(){


        try { 
            
            if (DB::table($this->mstrtbl)->where('code', '=', $_POST['code'])->limit(1)->delete()){            
                return Response::json(['success' => '1','next_id' => $this->max_code()]);
            }else{
                $a['errors'] = '0';
            }
        
        } catch(\Illuminate\Database\QueryException $ex){ 
            
            $a['errors'] = $ex->errorInfo;
            $a['errors_a'] = 1;  
            // Note any method of class PDOException can be called on $ex.
        
        }



        return $a;
    }


    public function max_code(){
        return DB::table($this->mstrtbl)->max('code')+1;
    }    

    
    public function item_list($n='item_code'){
        $Q = DB::table($this->mstrtbl);       

        $d  = '<select class="selectpicker" data-live-search="true" name="'.$n.'" id="'.$n.'" required>';
        $d .= '<option value=""></option>';
        if ($Q->count() > 0){
            foreach($Q->get() as $r){
                $d .= '<option value="'.$r->code.'">'.$r->description.'</option>';
            }
        }
        $d .= '</select>';

        return $d;
    }    
}
