<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Http\Request;

class CustomfunctionsController extends Controller
{


	public function cluster_list(){
		$Q = DB::table('m_cluster');

        $d  = '<select class="selectpicker" data-live-search="true" name="cl" id="cl" required>';
        $d .= '<option value=""></option>';
        if ($Q->count() > 0){
            foreach($Q->get() as $r){
                $d .= '<option value="'.$r->code.'">'.$r->description.'</option>';
            }
        }
        $d .= '</select>';

        return $d;
	}

    public function bclistbycl(){
    	return DB::table('m_branch')->where("cl",$_POST['cl'])->get();
    }

    public function subcategorylistbycategory(){
        return DB::table('m_sub_category')->where("category_code",$_POST['category_code'])->get();
    }


    public function setItemDet(){
        $q =  DB::table('m_item')->where("code",$_POST['code'])->first();
        $a['sum'] = $q;
        echo json_encode($a);

    }


    public function loadFAregistry(){

        $q =  DB::table('t_serial')->select('t_serial.*','m_item.description as item','m_item.model')->where("trans_no",$_POST['trans_no'])->where("trans_code",$_POST['trans_code'])->join('m_item',"t_serial.item_code","=","m_item.code");
        
        if ($q->count() > 0){
            $a['sum'] = $q->first();
            $a['det'] = DB::table('t_serial_movement')->where("trans_no",$_POST['trans_no'])->where("trans_code",$_POST['trans_code'])->get();
            $a['s']   = 1;
        }else{
            $a['s']   = 0;
        }
        
        echo json_encode($a);

    }

    public function loadFAtransfer(){

        $q =  DB::table('t_transfer_sum')
                ->select('t_transfer_sum.*','m_item.description as item','m_item.model')
                ->where("t_transfer_sum.code",$_POST['code'])
                ->join('m_item',"t_transfer_sum.item_code","=","m_item.code");
        
        if ($q->count() > 0){
            $a['sum'] = $q->first();
            $a['det'] = DB::table('t_transfer_det')->where("code",$_POST['code'])->get();
            $a['s']   = 1;
        }else{
            $a['s']   = 0;
        }
        
        echo json_encode($a);

    }

}
