<?php

namespace App\Http\Controllers;

use App\Purchase;
use PDF;
use DB;

class StockReportController extends Controller
{

    public function report_stock_in_hand($rpt_selected, $location)
    {

			if($location !=""){
				$location="AND a.location='".$location."'";
			}else{
				$location="";
			}
		$date=date('Y-m-d');
	
        $sql ="SELECT
		a.item,
		a.`description`,
		a.`location`,
		a.location_name,
		SUM(a.tot_qty) AS tot_qty,
		a.trans_date
		FROM
		(SELECT
		t.item,
		i.`description`,
		t.`location`,
		l.`description` AS location_name,
		((t.qty_in) - (t.qty_out)) AS tot_qty,
		t.trans_date
		FROM
		t_item_movement t
		JOIN m_item i
			ON i.`code` = t.`item`
		left JOIN m_location l
			ON l.`code` = t.`location`

		UNION
		ALL
		SELECT
		t.item_code AS item,
		i.`description`,
		t.`location_code` AS location,
		l.`description` AS location_name,
		((t.qty_in) - (t.qty_out)) AS tot_qty ,
		t.trans_date
		FROM
		t_serial_movement t
		JOIN m_item i
			ON i.`code` = t.`item_code`
		LEFT  JOIN m_location l
			ON l.`code` = t.`location_code`
		) a WHERE a.trans_date <= '".$date."' $location

			GROUP BY a.`item` ";
			$data=DB::select($sql);
        // $data = Purchase::PurchaseSummary($fd, $td);

        PDF::SetTitle(str_replace('_', ' ', $rpt_selected));
        PDF::AddPage("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
        PDF::SetFont('helvetica', '', 8);
		$RPC = new reportsController;
		$RPC->headerPage();
        PDF::SetFont('helvetica', 'B', 12);
        PDF::MultiCell(100, 0, 'Stock In Hand', $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::SetFont('helvetica', '', 8);
        PDF::MultiCell(100, 0, 'As At  ' . $date, $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::ln();
        PDF::SetFont('helvetica', 'B', 8);
       
        PDF::MultiCell(20,5, 'Item Code', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::MultiCell(60,5, 'Item Description', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::MultiCell(25,5, 'Location', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::MultiCell(25,5, 'Location Name', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::MultiCell(25,5, 'Total Qty', $border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

        PDF::SetFont('helvetica', '', 9);

        $s_item = '';
		
        foreach ($data as $key => $value) {
			$heigh = 6 * (max(1, PDF::getNumLines($value->description, 60)));


            
            PDF::HaveMorePages($heigh);

            PDF::MultiCell(20, $heigh, $value->item, $border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
            PDF::MultiCell(60, $heigh, $value->description, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

            PDF::MultiCell(25, $heigh, $value->location, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
            PDF::MultiCell(25, $heigh, $value->location_name, $border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
            PDF::MultiCell(25, $heigh, $value->tot_qty, $border = '1', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        }

        PDF::Output($rpt_selected . '.pdf');

    }

 
}
