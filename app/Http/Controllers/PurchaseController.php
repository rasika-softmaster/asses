<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Http\Request;
use Auth;


class PurchaseController extends Controller
{
    private $tbl_itm_movement = 't_item_movement';
    private $tbl_sum = 't_grn_sum';
    private $tbl_det = 't_grn_det';
    private $mstrurl = 'purchase';

     public function index(){
        
       
        $SCLT = new SupplierController;
        $STCL = new StoresController;
       
        return view($this->mstrurl, [
       
            'supplier' => $SCLT->supplier_list(),
            'store' => $STCL->store_list(),
            'pagename' => $this->mstrurl,
            'next_id'=>DB::table($this->tbl_sum)->max('nno')+1
        ]);
    }

    public function itemsearch(){

        $terms= $_POST['s_code'];
        $limit=10;

        $Q = 
        DB::table("m_item")->select('m_item.code','m_item.description','m_item.model')
        ->where(function ($query) use ($terms) {
            $query->orWhere('m_item.code', 'LIKE','%'.$terms.'%')
                  ->orWhere('m_item.description', 'LIKE', '%'.$terms.'%')
                  ->orWhere('m_item.model', 'LIKE', '%'.$terms.'%');
                  
        })->where('is_fa',0)->limit($limit);
        $a['count'] =$limit;
      	 	 	
        $a['data'] = $Q->get();

        if ($Q){
            $a['success'] = 1;
        }else{
            $a['errors'] = '0';
        }

        return $a;

    
    }

    public function store(Request $request)
    {
        
        try{
            


        

            $validator = Validator::make($request->all(), [
                'supplier_code' => 'required',
                'description' => 'required',
                 'store_code' => 'required',
                'no' => 'required',
                'date'=>'required',
                'total'=>'required'
            ]);        

            // dd( $request->is_edit);

            if ($validator->passes()) {

                    // dd($request->all());
                $is_edit =  $request->is_edit; 
                //first saving record

            
                if ($is_edit == 0){
                      //save sum table

                        $sum['nno']=$_POST['no'];
                        $sum['ddate']=$_POST['date'];
                        $sum['supplier']=$_POST['supplier_code'];
                        $sum['memo']=$_POST['description'];
                        $sum['gross_amount']=$_POST['total'];
                        $sum['net_amount']=$_POST['total'];
                        $sum['oc']=Auth::user()->id;
                        $sum['tax_amount']=0;
                        $sum['po_no']=0;
                        $sum['store']=$_POST['store_code'];
                        DB::table($this->tbl_sum)->insert($sum);
                   for ($i=0; $i < 25 ; $i++) { 
                        //save to details table
                       if(isset($_POST['code_'.$i]) && $_POST['code_'.$i] !=""){

                        //grn_details
                        $det['nno']=$_POST['no'];
                        $det['code']=$_POST['code_'.$i];
                        $det['qty']=$_POST['qty_'.$i];
                        $det['price']=$_POST['price_'.$i];
                            

                        DB::table($this->tbl_det)->insert($det);


                        //item movement 
                        $_movement['item']=$_POST['code_'.$i];
                        $_movement['location']=$_POST['store_code'];
                        $_movement['trans_code']=3;
                        $_movement['trans_no']=$_POST['no'];
                        $_movement['qty_in']=$_POST['qty_'.$i];
                        $_movement['qty_out']=0;
                        $_movement['memo']=$_POST['description'];
                        $_movement['trans_date']=$_POST['date'];
                        

                        DB::table($this->tbl_itm_movement)->insert($_movement);
                       }

                       //save to sum table


                    }
                    return Response::json(['success' => '1','next_id' => DB::table($this->tbl_sum)->max('nno')+1,'message'=>'Successfuly Added']);
                  
                   //edit records
                }else{  


                    //update sum table

                  
                    $sum['ddate']=$_POST['date'];
                    $sum['supplier']=$_POST['supplier_code'];
                    $sum['memo']=$_POST['description'];
                    $sum['gross_amount']=$_POST['total'];
                    $sum['net_amount']=$_POST['total'];
                    $sum['oc']=Auth::user()->id;
                    $sum['tax_amount']=0;
                    $sum['po_no']=0;
                    $sum['store']=$_POST['store_code'];
                    DB::table($this->tbl_sum)->where('nno',$_POST['no'])->update($sum);

                    $this->delete_details($_POST['no'],3);
               for ($i=0; $i < 25 ; $i++) { 
                    //update to details table
                   if(isset($_POST['code_'.$i]) && $_POST['code_'.$i] !=""){

                    //grn_details
                    $det['nno']=$_POST['no'];
                    $det['code']=$_POST['code_'.$i];
                    $det['qty']=$_POST['qty_'.$i];
                    $det['price']=$_POST['price_'.$i];
                        

                    DB::table($this->tbl_det)->insert($det);


                    //update item movement 
                    $_movement['item']=$_POST['code_'.$i];
                    $_movement['location']=$_POST['store_code'];
                    $_movement['trans_code']=3;
                    $_movement['trans_no']=$_POST['no'];
                    $_movement['qty_in']=$_POST['qty_'.$i];
                    $_movement['qty_out']=0;
                    $_movement['memo']=$_POST['description'];
                    $_movement['trans_date']=$_POST['date'];
                    

                    DB::table($this->tbl_itm_movement)->insert($_movement);
                   }
                }

                   
                    return Response::json(['success' => '1','next_id' => DB::table($this->tbl_sum)->max('nno')+1,'message'=>'Successfuly Updated']);
                  


                }

                

            }
             return Response::json(['success' => '2','Message' =>"Something Went Wrong Please Contact Admin","validation"=> $validator->errors()]);
           

        } catch(\Illuminate\Database\QueryException $ex){ 

            $a['Message'] = $ex->errorInfo; 
            $a['errors_a'] = 1; 
            return $a;

        }

    }
    
    public function delete_details($no,$trans_code){

                //grn det table
                DB::table($this->tbl_det)->where('nno', '=', $no)->delete();

                //delete item movement

                DB::table($this->tbl_itm_movement)->where('trans_code', '=', $trans_code)->where('trans_no', '=', $no)->delete();

    }
    public function loadData(Request $request){

        try{
            


        

            $validator = Validator::make($request->all(), [
                'edit_id' => 'required'
               
            
            ]);        

            if ($validator->passes()) {

                $edit_id = $request->edit_id;
                $records=[];
                $records["sum"]=[];
                $records["det"]=[];
                $records["sum"]= DB::table($this->tbl_sum)->where("nno",$edit_id)->get();
                $records["det"]= DB::table($this->tbl_det)->join("m_item","m_item.code","=",$this->tbl_det.".code")->where("nno",$edit_id)->get();
                return Response::json(['success' => '1','records' => $records]);

            }else{
                return Response::json(['success' => '2','Message' =>"Something Went Wrong Please Contact Admin","validation"=> $validator->errors()]);
           
            }
            

        } catch(\Illuminate\Database\QueryException $ex){ 

            $a['Message'] = $ex->errorInfo; 
            $a['errors_a'] = 1; 
            return $a;

        }

    }

}
 