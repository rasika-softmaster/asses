<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CustomfunctionsController;
use App\Http\Controllers\LocationController;
use Auth;


class faTransferController extends Controller
{

    private $tbl_sum = 't_transfer_sum';
    private $tbl_det = 't_transfer_det';
    private $tbl_ser = 't_serial';
    private $tbl_mov = 't_serial_movement';
    private $mstrurl = 'fa_transfer';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        
        $CLST = new CustomfunctionsController;
        $LCLT = new LocationController;
        $DPST = new DepartmentController;
        $CTST = new CategoryController;
        $BRST = new BrandController;
        $SIST = new SizeController;
        $UIST = new UnitController;
        $CMAC = new createModifyAccountController;
        $SULR = new SupplierController;
        
        return view($this->mstrurl, [

            'code' => DB::table($this->tbl_sum)->max('code')+1 ,
            'lc' => $LCLT->location_list('from_location'),
            'lcn' => $LCLT->location_list('to_location'),
            'pagename' => $this->mstrurl,            
            
            's_department'=>$DPST->department_list('s_department_code'),
            's_category'=>$CTST->category_list('s_category_code'),
            's_brand'=>$BRST->brand_list('s_brand_code'),
            's_size'=>$SIST->size_list('s_size_code'),
            's_unit'=>$UIST->unit_list('s_unit_code'),            
            's_supplier_code'=>$SULR->supplier_list('pur_supplier'),
            
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            
            $messages = ['code.required' => 'required', 'serial_no.required' => 'required', 'from_location.required' => 'required', 'to_location.required' => 'required', ];
                
            $validator = Validator::make($request->all(), ['code' => 'required', 'serial_no' => 'required', 'from_location' => 'required', 'to_location' => 'required', ],$messages);

            
            unset($_POST['s_item_code'],$_POST['XXXX']);

            $input = $request->all();

            if ($validator->passes()) {

                $_POST['oc']                = Auth::id();
                $_POST['current_datetime']  = date('Y-m-d h:i:s');                
                $is_edit                    = $_POST['is_edit']; unset($_POST['is_edit']);

                if ($is_edit == 0){

                    $_POST_['code'] = $_POST['code'] =  DB::table($this->tbl_sum)->max('code')+1;
                    $_POST_['item_code'] = $_POST['item_code'];
                    $_POST_['date'] = $_POST['date'];
                    $_POST_['serial_no'] = $_POST['serial_no'];
                    $_POST_['oc'] = $_POST['oc'];
                    $_POST_['create_datetime'] = $_POST['current_datetime'];
                    $_POST_['modify_datetime'] = $_POST['current_datetime'];
                    
                    DB::table($this->tbl_sum)->insert($_POST_);
                    unset($_POST['date'],$_POST['serial_no'],$_POST['oc'],$_POST['current_datetime']);
                    DB::table($this->tbl_det)->insert($_POST);
                    

                    DB::table($this->tbl_ser)   ->where('serial_no', $_POST_['serial_no'])
                                                ->where('item_code', $_POST['item_code'])
                                                ->update(array("current_location"=>$_POST['to_location']));

                    // item move out

                    $_POST_OUT['serial_no'] = $_POST_['serial_no'];
                    $_POST_OUT['item_code'] = $_POST['item_code'];
                    $_POST_OUT['location_code'] = $_POST['from_location'];
                    $_POST_OUT['qty_in'] = 0;
                    $_POST_OUT['qty_out'] = 1;
                    $_POST_OUT['trans_code'] = 4;
                    $_POST_OUT['trans_no'] = $_POST_['code'];
                    $_POST_OUT['trans_date'] = $_POST_['date'];
                    DB::table($this->tbl_mov)->insert($_POST_OUT);

                    // item move in

                    $_POST_IN['serial_no'] = $_POST_['serial_no'];
                    $_POST_IN['item_code'] = $_POST['item_code'];
                    $_POST_IN['location_code'] = $_POST['to_location'];
                    $_POST_IN['qty_in'] = 1;
                    $_POST_IN['qty_out'] = 0;
                    $_POST_IN['trans_code'] = 4;
                    $_POST_IN['trans_no'] = $_POST_['code'];
                    $_POST_IN['trans_date'] = $_POST_['date'];
                    DB::table($this->tbl_mov)->insert($_POST_IN);

                }else{

                     return Response::json(['success' => 2, 'm'=>'Update not available.']);

                }


                return Response::json(['success' => '1']);

            }
            
            return Response::json(['errors' => $validator->errors()]);

        } catch(\Illuminate\Database\QueryException $ex){ 

            $a['errors'] = $ex->errorInfo; 
            $a['errors_a'] = 1; 
            return $a;

        }

    }

    /**
     * Display the specified resource.
     *
     
     */
    public function show(){        
        $Q = DB::table($this->tbl_sum)->orderby('code');
        $a['count'] = $Q->count();    
        $a['data'] = $Q->get();

        if ($Q){
            $a['success'] = 1;
        }else{
            $a['errors'] = '0';
        }

        return $a;        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(){
        
        $Q = DB::table($this->tbl_sum)->where(array("code"=>$_POST['code']));
        $a['count'] = $Q->count();    
        $a['data'] = $Q->get();

        if ($Q){
            $a['success'] = 1;
        }else{
            $a['errors'] = '0';
        }

        return $a;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(){
        if (DB::table($this->tbl_sum)->where('code', '=', $_POST['code'])->limit(1)->delete()){            
            return Response::json(['success' => '1','next_id' => DB::table($this->tbl_sum)->max('code')+1]);
        }else{
            $a['errors'] = '0';
        }

        return $a;
    }





    public function itemsearch(){


$Q = DB::table("t_serial")

->select(
't_serial.serial_no',
't_serial.location_code',
't_serial.current_location',
'm_item.code',
'm_item.description',
'm_department.description as department_code',
'm_category.description as category_code',
'm_sub_category.description as subcategory_code',
'm_brand.description as brand_code',
'm_item.model',
'm_size.description as size_code',
'm_unit.description as unit_code',
'm_item.purchase_price',
'm_location.code as current_location',
'm_location.description as current_location_desc'
)

->where('m_item.code','LIKE','%'.$_POST['s_code'].'%')
->where('m_item.description','LIKE','%'.$_POST['s_description'].'%')
->where('m_item.department_code','LIKE','%'.$_POST['s_department_code'].'%')
->where('m_item.category_code','LIKE','%'.$_POST['s_category_code'].'%')
->where('m_item.subcategory_code','LIKE','%'.$_POST['subcategory_code'].'%')
->where('m_item.brand_code','LIKE','%'.$_POST['s_brand_code'].'%')
->where('m_item.model','LIKE','%'.$_POST['model'].'%')
->where('m_item.size_code','LIKE','%'.$_POST['s_size_code'].'%')
->where('m_item.unit_code','LIKE','%'.$_POST['s_unit_code'].'%')
->where('m_item.purchase_price','LIKE','%'.$_POST['purchase_price'].'%')

->Join('m_item','m_item.code','=','t_serial.item_code')
->leftJoin('m_department','m_item.department_code','=','m_department.code')
->leftJoin('m_category','m_item.category_code','=','m_category.code')
->leftJoin('m_sub_category','m_item.subcategory_code','=','m_sub_category.code')
->leftJoin('m_brand','m_item.brand_code','=','m_brand.code')
->leftJoin('m_size','m_item.size_code','=','m_size.code')
->leftJoin('m_unit','m_item.unit_code','=','m_unit.code')
->leftJoin('m_location','t_serial.current_location','=','m_location.code');


        //$Q = DB::select("Select * from m_item");
        //$Q = DB::select( DB::raw("Select * from m_item") );




        $a['count'] = $Q->count();
        $a['data'] = $Q->get();

        if ($Q){
            $a['success'] = 1;
        }else{
            $a['errors'] = '0';
        }

        return $a;

    
    }








public function trans_code_dd($n='trans_code'){
        
    $Q = DB::table("t_trans_code");       

    $d  = '<select class="selectpicker" data-live-search="true" name="'.$n.'" id="'.$n.'" required>';
    $d .= '<option value=""></option>';
    if ($Q->count() > 0){
        foreach($Q->get() as $r){
            $d .= '<option value="'.$r->code.'">'.$r->description.'</option>';
        }
    }
    $d .= '</select>';

    return $d;    

}

    



}
