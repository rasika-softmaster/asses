<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Http\Request;


class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){        
        $CLST = new CustomfunctionsController;
        return view('branch', ['bc' => DB::table('m_branch')->max('bc')+1 , 'cl' => $CLST->cluster_list()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{

            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255|regex:/^[a-zA-Z ]+$/u',
                'email' => $request->email != null ? 'email|max:255|' : '',
                'tp' => $request->tp != null ? 'digits:10' : '',            
            ]);

            $input = $request->all();

            if ($validator->passes()) {

                $_POST['oc'] = 1;
                $is_edit = $_POST['is_edit']; unset($_POST['is_edit']);

                if ($is_edit == 0){
                    DB::table('m_branch')->insert($_POST);
                }else{               

                    if (!$validator->passes()) { return Response::json(['errors' => $validator->errors()]); }
                    DB::table('m_branch')->where('bc', $_POST['bc']) ->update($_POST); 

                }

                return Response::json(['success' => '1','next_id' => DB::table('m_branch')->max('bc')+1]);

            }
            
            return Response::json(['errors' => $validator->errors()]);

        } catch(\Illuminate\Database\QueryException $ex){ 

            $a['errors'] = $ex->errorInfo; 
            $a['errors_a'] = 1; 
            return $a;

        }

    }

    /**
     * Display the specified resource.
     *
     
     */
    public function show(){        
        $Q = DB::table('m_branch')->orderby('bc');
        $a['count'] = $Q->count();    
        $a['data'] = $Q->get();

        if ($Q){
            $a['success'] = 1;
        }else{
            $a['errors'] = '0';
        }

        return $a;        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(){
        
        $Q = DB::table('m_branch')->where(array("cl"=>$_POST['cl'],"bc"=>$_POST['bc']));
        $a['count'] = $Q->count();    
        $a['data'] = $Q->get();

        if ($Q){
            $a['success'] = 1;
        }else{
            $a['errors'] = '0';
        }

        return $a;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(){

        try{

            if (DB::table('m_branch')->where('bc', '=', $_POST['bc'])->limit(1)->delete()){            
                return Response::json(['success' => '1','next_id' => DB::table('m_branch')->max('bc')+1]);
            }else{
                $a['errors'] = '0';
            }

        } catch(\Illuminate\Database\QueryException $ex){ 

            $a['errors'] = $ex->errorInfo; 
            $a['errors_a'] = 1; 

        }

        return $a;
        
    }
}
