<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class CustomAuthController extends Controller
{
    public function showRegisterForm(){
    	return view('custom.register');
    }

    public function register(Request $request){

    	$this->validation($request);

    	$request['password'] = bcrypt($request->password);

    	User::create($request->all());
    	return redirect('/home')->with('Status','You are registed');

    	//return $request->all();
    }


    public function showLoginForm(){
    	return view('custom.login');
    }

    public function login(Request $request){

    	$this->validate($request,[
	        'username' => 'required|max:255',
	        'password' => 'required|max:255',
	        'branch' => 'required|max:255',	        
	    ]);

    	if (Auth::attempt(['username'=>$request->username,'password'=>$request->password,'branch'=>$request->branch])){
    		return redirect('/');
    	}

    	return redirect('/?l=0')->with('login_status','Invalid credentials');
    }


    public function validation($request){

	    return $this->validate($request,[
	        'username' => 'required|unique:users|max:255',
	        'password' => 'required|max:255',
	        'branch' => 'required|max:255',	        
	    ]);

	}




}
