<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CustomfunctionsController;


class StoresController extends Controller
{

    private $mstrtbl = 'm_stores';
    private $mstrurl = 'stores';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $CLST = new CustomfunctionsController;
        return view($this->mstrurl, ['code' => DB::table($this->mstrtbl)->max('code')+1 , 'cl' => $CLST->cluster_list() ]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        try {

            $validator = Validator::make($request->all(), [
                'cl' => 'required',
                'bc' => 'required',
                'description' => 'required|max:255|regex:/^[a-zA-Z0-9 ]+$/u',            
            ]);

            $input = $request->all();

            if ($validator->passes()) {

                $_POST['oc'] = 1;
                $_POST['purchase'] = isset($_POST['purchase']) ? 1 : 0;
                $_POST['transfer_location'] = isset($_POST['transfer_location']) ? 1 : 0;
                $is_edit = $_POST['is_edit']; unset($_POST['is_edit']);

                if ($is_edit == 0){
                    DB::table($this->mstrtbl)->insert($_POST);
                }else{               

                    if (!$validator->passes()) { return Response::json(['errors' => $validator->errors()]); }
                    DB::table($this->mstrtbl)->where('code', $_POST['code']) ->update($_POST); 

                }

                return Response::json(['success' => '1','next_id' => DB::table($this->mstrtbl)->max('code')+1]);

            }
            
            return Response::json(['errors' => $validator->errors()]);

        } catch(\Illuminate\Database\QueryException $ex){ 

            $a['errors'] = $ex->errorInfo; 
            $a['errors_a'] = 1; 
            return $a;

        }

    }

    /**
     * Display the specified resource.
     *
     
     */
    public function show(){        
        $Q = DB::table($this->mstrtbl)
                            ->select($this->mstrtbl.".*",'m_cluster.description as desc_cl','m_branch.name as bc_name')
                            ->join('m_cluster',$this->mstrtbl.'.cl','=','m_cluster.code')
                            ->join('m_branch',$this->mstrtbl.'.bc','=','m_branch.bc')
                            ->orderby($this->mstrtbl.'.code');

        $a['count'] = $Q->count();    
        $a['data'] = $Q->get();

        if ($Q){
            $a['success'] = 1;
        }else{
            $a['errors'] = '0';
        }

        return $a;        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(){
        
        $Q = DB::table($this->mstrtbl)->where(array("code"=>$_POST['code']));
        $a['count'] = $Q->count();    
        $a['data'] = $Q->get();

        if ($Q){
            $a['success'] = 1;
        }else{
            $a['errors'] = '0';
        }

        return $a;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(){
        if (DB::table($this->mstrtbl)->where('code', '=', $_POST['code'])->limit(1)->delete()){            
            return Response::json(['success' => '1','next_id' => DB::table($this->mstrtbl)->max('code')+1]);
        }else{
            $a['errors'] = '0';
        }

        return $a;
    }

    public function store_list($n='store_code'){
        $Q = DB::table($this->mstrtbl);       

        $d  = '<select class="selectpicker" data-live-search="true" name="'.$n.'" id="'.$n.'" required>';
        $d .= '<option value=""></option>';
        if ($Q->count() > 0){
            foreach($Q->get() as $r){
                $d .= '<option value="'.$r->code.'">'.$r->description.'</option>';
            }
        }
        $d .= '</select>';

        return $d;
    }
}
