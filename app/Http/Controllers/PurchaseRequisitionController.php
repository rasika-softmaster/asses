<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CustomfunctionsController;
use App\Http\Controllers\LocationController;


class PurchaseRequisitionController extends Controller
{

    private $tbl_sum = 't_pur_req_sum';
    private $tbl_det = 't_pur_req_det';
    private $mstrurl = 'purchase_requisition';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        
        $CLST = new CustomfunctionsController;
        $LCLT = new LocationController;
        $DPST = new DepartmentController;
        $CTST = new CategoryController;
        $BRST = new BrandController;
        $SIST = new SizeController;
        $UIST = new UnitController;
        $CMAC = new createModifyAccountController;
        
        return view($this->mstrurl, [

            'code' => DB::table($this->tbl_sum)->max('code')+1 ,
            'lc' => $LCLT->location_list(),
            'pagename' => $this->mstrurl,            
            
            's_department'=>$DPST->department_list('s_department_code'),
            's_category'=>$CTST->category_list('s_category_code'),
            's_brand'=>$BRST->brand_list('s_brand_code'),
            's_size'=>$SIST->size_list('s_size_code'),
            's_unit'=>$UIST->unit_list('s_unit_code'),

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        try {

            $validator = Validator::make($request->all(), [
                'cl' => 'required',
                'bc' => 'required',
                'description' => 'required|max:255|regex:/^[a-zA-Z0-9 ]+$/u',            
            ]);

            $input = $request->all();

            if ($validator->passes()) {

                $_POST['oc'] = 1;
                $_POST['purchase'] = isset($_POST['purchase']) ? 1 : 0;
                $_POST['transfer_location'] = isset($_POST['transfer_location']) ? 1 : 0;
                $is_edit = $_POST['is_edit']; unset($_POST['is_edit']);

                if ($is_edit == 0){
                    DB::table($this->mstrtbl)->insert($_POST);
                }else{               

                    if (!$validator->passes()) { return Response::json(['errors' => $validator->errors()]); }
                    DB::table($this->mstrtbl)->where('code', $_POST['code']) ->update($_POST); 

                }

                return Response::json(['success' => '1','next_id' => DB::table($this->mstrtbl)->max('code')+1]);

            }
            
            return Response::json(['errors' => $validator->errors()]);

        } catch(\Illuminate\Database\QueryException $ex){ 

            $a['errors'] = $ex->errorInfo; 
            $a['errors_a'] = 1; 
            return $a;

        }

    }

    /**
     * Display the specified resource.
     *
     
     */
    public function show(){        
        $Q = DB::table($this->mstrtbl)->orderby('code');
        $a['count'] = $Q->count();    
        $a['data'] = $Q->get();

        if ($Q){
            $a['success'] = 1;
        }else{
            $a['errors'] = '0';
        }

        return $a;        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(){
        
        $Q = DB::table($this->mstrtbl)->where(array("code"=>$_POST['code']));
        $a['count'] = $Q->count();    
        $a['data'] = $Q->get();

        if ($Q){
            $a['success'] = 1;
        }else{
            $a['errors'] = '0';
        }

        return $a;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(){
        if (DB::table($this->mstrtbl)->where('code', '=', $_POST['code'])->limit(1)->delete()){            
            return Response::json(['success' => '1','next_id' => DB::table($this->mstrtbl)->max('code')+1]);
        }else{
            $a['errors'] = '0';
        }

        return $a;
    }
}
