<?php

namespace App\Http\Controllers;

use App\Purchase;
use PDF;

class PurchaseReportsController extends Controller
{

    public function report_summary($rpt_selected, $fd, $td)
    {

        $data = Purchase::PurchaseSummary($fd, $td);

        PDF::SetTitle(str_replace('_', ' ', $rpt_selected));
        PDF::AddPage("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
        PDF::SetFont('helvetica', '', 8);
        $RPC = new reportsController;
        $RPC->headerPage();
        PDF::SetFont('helvetica', 'B', 12);
        PDF::MultiCell(100, 0, 'Asset Purchase Summary Report', $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::SetFont('helvetica', '', 8);
        PDF::MultiCell(100, 0, 'Date ' . $fd . ' To ' . $td, $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::ln();
        PDF::SetFont('helvetica', 'B', 8);
        $heigh = 6 * (max(1, PDF::getNumLines('Purchase Number', 20)));

        PDF::MultiCell(20, $heigh, 'Date', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::MultiCell(20, $heigh, 'Purchase Number', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::MultiCell(25, $heigh, 'Supplier', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::MultiCell(25, $heigh, 'Gross Amount', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::MultiCell(25, $heigh, 'Operator', $border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

        PDF::SetFont('helvetica', '', 9);

		$s_item = '';
		
		if(count($data) > 0){
			foreach ($data as $key => $value) {

				$heigh = 6 * (max(1, PDF::getNumLines("description", 40), PDF::getNumLines("serial_no", 40)));
				PDF::HaveMorePages($heigh);
	
				PDF::MultiCell(20, $heigh, $value->ddate, $border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				PDF::MultiCell(20, $heigh, $value->nno, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
					PDF::MultiCell(25, $heigh, $value->sup_name, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				PDF::MultiCell(25, $heigh, $value->gross_amount, $border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
				PDF::MultiCell(25, $heigh, $value->username, $border = '1', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			}
	
			PDF::Output($rpt_selected . '.pdf');

		}else{
			echo "<script>alert('No Data');window.close();</script>";
		}

      

    }

    public function report_detail($rpt_selected, $fd, $td)
    {

        $data = Purchase::PurchaseDetails($fd, $td);

        PDF::SetTitle(str_replace('_', ' ', $rpt_selected));
        PDF::AddPage("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
        PDF::SetFont('helvetica', '', 8);
        $RPC = new reportsController;
        $RPC->headerPage();
        PDF::SetFont('helvetica', 'B', 12);
        PDF::MultiCell(100, 0, 'Asset Purchase Details Report', $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::SetFont('helvetica', '', 8);
        PDF::MultiCell(100, 0, 'Date ' . $fd . ' To ' . $td, $border = '0', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::ln();
        PDF::SetFont('helvetica', 'B', 8);
        $heigh = 6 * (max(1, PDF::getNumLines('Item code', 25)));

        PDF::MultiCell(20, $heigh, 'Date', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::MultiCell(20, $heigh, 'No', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::MultiCell(25, $heigh, 'Supplier', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::MultiCell(25, $heigh, 'Item code', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::MultiCell(25, $heigh, 'Item', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::MultiCell(25, $heigh, 'Qty', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::MultiCell(25, $heigh, 'Price', $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
        PDF::MultiCell(25, $heigh, 'Total', $border = '1', $align = 'L', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);

        PDF::SetFont('helvetica', '', 9);

        $total = '';
		if(count($data) > 0){
			foreach ($data as $key => $value) {

				$heigh = 6 * (max(1, PDF::getNumLines($value->item_des, 25), PDF::getNumLines("serial_no", 40)));
				PDF::HaveMorePages($heigh);
	
				PDF::MultiCell(20, $heigh, $value->ddate, $border = '1', $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
				PDF::MultiCell(20, $heigh, $value->nno, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
				PDF::MultiCell(25, $heigh, $value->sup_name, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
				PDF::MultiCell(25, $heigh, $value->code, $border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
				PDF::MultiCell(25, $heigh, $value->item_des, $border = '1', $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
				PDF::MultiCell(25, $heigh, $value->qty, $border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
				PDF::MultiCell(25, $heigh, $value->price, $border = '1', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
				PDF::MultiCell(25, $heigh, number_format($value->price * $value->qty, 2), $border = '1', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
				$total += ($value->price * $value->qty);
			}
	
			PDF::MultiCell(140, $heigh, "", $border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			PDF::MultiCell(25, $heigh, "Grand Total", $border = '0', $align = 'R', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
			PDF::MultiCell(25, $heigh, number_format($total, 2), $border = '1', $align = 'R', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'M', $fitcell = false);
	
			PDF::Output($rpt_selected . '.pdf');

		}else{
			echo "<script>alert('No Data');window.close();</script>";
		}

    

    }
}
