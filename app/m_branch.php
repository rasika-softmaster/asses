<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class m_branch extends Model
{
    protected $table = "m_branch";

    $bc = DB::table('m_branch')->max('bc')+1;
    return view('branch', ['bc' => $bc]);
    
}
