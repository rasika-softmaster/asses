<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{    
	protected $table = "m_brand";

	protected $primaryKey = 'code'; // or null
    public $incrementing = false; 

    public function items()
    {
    	return $this->hasMany(Item::class,'brand_code','code');
    }

}
