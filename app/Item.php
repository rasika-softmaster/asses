<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Item extends Model
{
    protected $table = "m_item";

    protected $primaryKey = 'code'; // or null
    public $incrementing = false; 

    public function brand()
    {
    	return $this->belongsTo(Brand::class,'brand_code','code');
    }

    public function department(){

    	return $this->belongsTo(Department::class,'department_code','code');

    }



    public function scopeReportx(){

    	return DB::table('m_item')	->select('m_item.*' ,
    											'm_department.description as dept_desc',
    											'm_brand.description as brand_desc',
    											'm_category.description as cate_desc',
    											'm_sub_category.description as sub_cate_desc',
    											'm_size.description as size_desc',
    											'm_unit.description as unit_desc'

    										)
    								
    								->join('m_department', 'm_department.code', '=', 'm_item.department_code')
    								->join('m_category', 'm_category.code', '=', 'm_item.category_code')
    								->join('m_sub_category', 'm_sub_category.code', '=', 'm_category.code')
    								->join('m_brand', 'm_brand.code', '=', 'm_item.brand_code')
    								->leftjoin('m_size', 'm_size.code', '=', 'm_item.size_code')
    								->join('m_unit', 'm_unit.code', '=', 'm_item.unit_code')				
    								->orderBy('m_item.code', 'ASC')
    								->get();

    }

public static function ItemSerialCount($item_code){

    return DB::table('t_serial')->select(DB::raw("COUNT(*) as qty"))->where('item_code',$item_code)->value('qty');

}


}
