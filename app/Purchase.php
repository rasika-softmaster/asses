<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Purchase extends Model
{
   protected $table = 't_grn_sum';


   public static function PurchaseSummary($fd,$td){
   	  	return DB::table('t_grn_sum')
   	  			->select('t_grn_sum.*','users.username','m_supplier.name as sup_name')
    			->join('users', 'users.id', '=', 't_grn_sum.oc')
    			->join('m_supplier', 'm_supplier.code', '=', 't_grn_sum.supplier')
    			->whereBetween('t_grn_sum.ddate', [$fd, $td])
    			->orderBy('t_grn_sum.nno', 'ASC')
    			->get();

   	
   }

   public static function PurchaseDetails($fd,$td){

   		return DB::table('t_grn_sum')
   	  			->select('t_grn_sum.*','t_grn_det.*','m_supplier.name as sup_name','m_item.description as item_des')
            ->join('t_grn_det', 't_grn_det.nno', '=', 't_grn_sum.nno')
    		    ->join('m_item', 'm_item.code', '=', 't_grn_det.code')
      			->join('m_supplier', 'm_supplier.code', '=', 't_grn_sum.supplier')
      			->whereBetween('t_grn_sum.ddate', [$fd, $td])
      			->orderBy('t_grn_sum.nno', 'ASC')
      			->get();

   }
}
