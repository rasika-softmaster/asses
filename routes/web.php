<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::group(['middleware' => 'prevent-back-history'], function () {

    Route::get('/', function () {
        return view('dashboard');
    });

    Route::auth();

    //echo Request::segment(1)."<br>";
    //echo Request::segment(2)."<br>";
    //DB::table('m_branch')->max('bc')+1;

    /*approved = false;

    if ( is_edit ){
    if (got_edit_permi){
    approved = true;
    }
    }else{
    if (got_save_permi){
    approved = true
    }
    }

    if (approved){
    SAVE and UPDATE section must go here
    }    */

    //------------- VIEW section -----------------------

    Route::get('/fa_transfer', 'faTransferController@index');
    Route::get('/rpt_reports', 'reportsController@index');
    Route::get('/fa_registry', 'faRegistryController@index');
    Route::get('/location_division', 'locationDivisionController@index');
    Route::get('/location_department', 'locationDepartmentController@index');
    Route::get('/set_default_account', 'SetDefaultAccountController@index');
    Route::get('/create_modify_account', 'createModifyAccountController@index');
    Route::get('/account_category', 'accountCategoryController@index');
    Route::get('/purchase_requisition', 'PurchaseRequisitionController@index');
    Route::get('/purchase', 'PurchaseController@index');
    Route::get('/size', 'SizeController@index');
    Route::get('/item', 'ItemController@index');
    Route::get('/department', 'DepartmentController@index');
    Route::get('/supplier', 'SupplierController@index');
    Route::get('/suppliercategory', 'SuppliercategoryController@index');
    Route::get('/unit', 'UnitController@index');
    Route::get('/subcate', 'SubcateController@index');
    Route::get('/location', 'LocationController@index');
    Route::get('/color', 'ColorController@index');
    Route::get('/company', 'CompanyController@index');
    Route::get('/category', 'CategoryController@index');
    Route::get('/brand', 'BrandController@index');
    Route::get('/stores', 'StoresController@index');
    Route::get('/cluster', 'ClusterController@index');
    Route::get('/branch', 'BranchController@index');
    Route::get('/home', 'HomeController@index');
    Route::get('/customer', 'CustomerController@index');
    Route::get('/stock_transfer_non_serial', 'NonSerialStockTransController@index');
    Route::get('/item_damage', 'ItemDamageController@index');
    //------------- END VIEW section --------------------

    //------------- SAVE and UPDATE section -----------------------

    Route::post('/fa_transfer/store', 'faTransferController@store');
    Route::post('/fa_registry/store', 'faRegistryController@store');
    Route::post('/rpt_reports/generate', 'reportsController@generate');
    Route::post('/location_division/store', 'locationDivisionController@store');
    Route::post('/location_department/store', 'locationDepartmentController@store');
    Route::post('/set_default_account/store', 'SetDefaultAccountController@store');
    Route::post('/create_modify_account/store', 'createModifyAccountController@store');
    Route::post('/account_category/store', 'accountCategoryController@store');
    Route::post('/purchase_requisition/store', 'PurchaseRequisitionController@store');
    Route::post('/size/store', 'SizeController@store');
    Route::post('/item/store', 'ItemController@store');
    Route::post('/department/store', 'DepartmentController@store');
    Route::post('/supplier/store', 'SupplierController@store');
    Route::post('/suppliercategory/store', 'SuppliercategoryController@store');
    Route::post('/unit/store', 'UnitController@store');
    Route::post('/subcate/store', 'SubcateController@store');
    Route::post('/location/store', 'LocationController@store');
    Route::post('/color/store', 'ColorController@store');
    Route::post('/company/store', 'CompanyController@store');
    Route::post('/category/store', 'CategoryController@store');
    Route::post('/brand/store', 'BrandController@store');
    Route::post('/stores/store', 'StoresController@store');
    Route::post('/branch/store', 'BranchController@store');
    Route::post('/cluster/store', 'ClusterController@store');
    Route::post('/purchase/store', 'PurchaseController@store');
    Route::post('/stock_transfer_non_serial/store', 'NonSerialStockTransController@store');
    Route::post('/item_damage/store', 'ItemDamageController@store');
    //------------- END SAVE section -----------------------

    //------------- SELECT section -----------------------

    Route::post('/fa_transfer/show', 'faTransferController@show');
    Route::post('/fa_registry/show', 'faRegistryController@show');
    Route::post('/location_division/show', 'locationDivisionController@show');
    Route::post('/location_department/show', 'locationDepartmentController@show');
    Route::post('/set_default_account/show', 'SetDefaultAccountController@show');
    Route::post('/create_modify_account/show', 'createModifyAccountController@show');
    Route::post('/account_category/show', 'accountCategoryController@show');
    Route::post('/purchase_requisition/show', 'PurchaseRequisitionController@show');
    Route::post('/size/show', 'SizeController@show');
    Route::post('/item/show', 'ItemController@show');
    Route::post('/department/show', 'DepartmentController@show');
    Route::post('/supplier/show', 'SupplierController@show');
    Route::post('/suppliercategory/show', 'SuppliercategoryController@show');
    Route::post('/unit/show', 'UnitController@show');
    Route::post('/subcate/show', 'SubcateController@show');
    Route::post('/location/show', 'LocationController@show');
    Route::post('/color/show', 'ColorController@show');
    Route::post('/company/show', 'CompanyController@show');
    Route::post('/category/show', 'CategoryController@show');
    Route::post('/brand/show', 'BrandController@show');
    Route::post('/stores/show', 'StoresController@show');
    Route::post('/branch/show', 'BranchController@show');
    Route::post('/cluster/show', 'ClusterController@show');
    //------------- END SELECT section -----------------------

    //------------- EDIT section -----------------------

    Route::post('/fa_transfer/edit', 'faTransferController@edit');
    Route::post('/fa_registry/edit', 'faRegistryController@edit');
    Route::post('/location_division/edit', 'locationDivisionController@edit');
    Route::post('/location_department/edit', 'locationDepartmentController@edit');
    Route::post('/set_default_account/edit', 'SetDefaultAccountController@edit');
    Route::post('/create_modify_account/edit', 'createModifyAccountController@edit');
    Route::post('/account_category/edit', 'accountCategoryController@edit');
    Route::post('/purchase_requisition/edit', 'PurchaseRequisitionController@edit');
    Route::post('/size/edit', 'SizeController@edit');
    Route::post('/item/edit', 'ItemController@edit');
    Route::post('/department/edit', 'DepartmentController@edit');
    Route::post('/supplier/edit', 'SupplierController@edit');
    Route::post('/suppliercategory/edit', 'SuppliercategoryController@edit');
    Route::post('/unit/edit', 'UnitController@edit');
    Route::post('/subcate/edit', 'SubcateController@edit');
    Route::post('/location/edit', 'LocationController@edit');
    Route::post('/color/edit', 'ColorController@edit');
    Route::post('/company/edit', 'CompanyController@edit');
    Route::post('/category/edit', 'CategoryController@edit');
    Route::post('/brand/edit', 'BrandController@edit');
    Route::post('/stores/edit', 'StoresController@edit');
    Route::post('/branch/edit', 'BranchController@edit');
    Route::post('/cluster/edit', 'ClusterController@edit');
    //------------- END EDIT section -----------------------

    //------------- DELETE section -----------------------

    Route::post('/fa_transfer/destroy', 'faTransferController@destroy');
    Route::post('/fa_registry/destroy', 'faRegistryController@destroy');
    Route::post('/location_division/destroy', 'locationDivisionController@destroy');
    Route::post('/location_department/destroy', 'locationDepartmentController@destroy');
    Route::post('/set_default_account/destroy', 'SetDefaultAccountController@destroy');
    Route::post('/create_modify_account/destroy', 'createModifyAccountController@destroy');
    Route::post('/account_category/destroy', 'accountCategoryController@destroy');
    Route::post('/purchase_requisition/destroy', 'PurchaseRequisitionController@destroy');
    Route::post('/size/destroy', 'SizeController@destroy');
    Route::post('/item/destroy', 'ItemController@destroy');
    Route::post('/department/destroy', 'DepartmentController@destroy');
    Route::post('/supplier/destroy', 'SupplierController@destroy');
    Route::post('/suppliercategory/destroy', 'SuppliercategoryController@destroy');
    Route::post('/unit/destroy', 'UnitController@destroy');
    Route::post('/subcate/destroy', 'SubcateController@destroy');
    Route::post('/location/destroy', 'LocationController@destroy');
    Route::post('/color/destroy', 'ColorController@destroy');
    Route::post('/company/destroy', 'CompanyController@destroy');
    Route::post('/category/destroy', 'CategoryController@destroy');
    Route::post('/brand/destroy', 'BrandController@destroy');
    Route::post('/stores/destroy', 'StoresController@destroy');
    Route::post('/branch/destroy', 'BranchController@destroy');
    Route::post('/cluster/destroy', 'ClusterController@destroy');
    //------------- END DELETE section -----------------------


    //-------------------- start Load Data Url -----------------


    
    Route::post('/purchase/load_data', 'PurchaseController@loadData');
    Route::post('/item_damage/load_data', 'ItemDamageController@loadData');
    Route::post('/stock_transfer_non_serial/load_data', 'NonSerialStockTransController@loadData');
  
   

    //--------------------end load Data url






    //------------- Custom section -----------------------
    Route::post('/fa_registry/checkSerialExit', 'faRegistryController@checkSerialExit');
    Route::post('/Customfunctions/loadFAtransfer', 'CustomfunctionsController@loadFAtransfer');
    Route::post('/fa_transfer/itemsearch', 'faTransferController@itemsearch');
    Route::post('/Customfunctions/loadFAregistry', 'CustomfunctionsController@loadFAregistry');
    Route::post('/Customfunctions/setItemDet', 'CustomfunctionsController@setItemDet');
    Route::post('/fa_registry/itemsearch', 'faRegistryController@itemsearch');
    Route::post('/Customfunctions/bclistbycl', 'CustomfunctionsController@bclistbycl');
    Route::post('/Customfunctions/subcategorylistbycategory', 'CustomfunctionsController@subcategorylistbycategory');
    Route::post('/account_category/set_parent_dd', 'accountCategoryController@set_parent_dd');
    //purchase
    Route::post('/purchase/itemsearch', 'PurchaseController@itemsearch');
    //non serial item transfer

    Route::post('/stock_transfer_non_serial/itemsearch', 'NonSerialStockTransController@itemsearch');

    //item damage controller
    Route::post('/item_damage/itemsearch', 'ItemDamageController@itemsearch');
    //------------- End Custom section -----------------------

    Route::get('custom-register', 'CustomAuthController@showRegisterForm')->name('custom.register');
    Route::post('custom-register', 'CustomAuthController@register');

    Route::get('custom-login', 'CustomAuthController@showloginForm')->name('custom.login');
    Route::post('custom-login', 'CustomAuthController@login');

});
