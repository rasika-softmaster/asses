
var mstrurl = 'item';
var glob_next_id = '';

$(document).ready(function(){

	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	load_data();
	
});

$(document).on("click",".btnAddNew",function(e){

	e.preventDefault();	
	$("#ModalMasterPopup").modal();

});

$(document).on("change","select[name=category_code]",function(){
	set_sub_category_list_by_category($(this));
});


$('#ModalMasterPopup').on('hidden.bs.modal', function () {
	resetControlErrorsing();
	$(".imgview").html("");
});


function resetControlErrorsing(){
	$("#f input[type='text'],select,input[type='number']").css("border","1px solid #ccc");
	$("#f input[type='text'],select,input[type='number']").next().remove();	
	$(".mm").html("");	
	reset_form(glob_next_id);
}


function set_sub_category_list_by_category(obj){

	$.ajax({
	    url: '/Customfunctions/subcategorylistbycategory',
	    dataType: 'json',
	    type: 'post',	    
	    data: { category_code : jQuery.type( obj ) === "object" ? obj.val() : obj  }  ,	    
	    async: false,
	    success: function( D ){	    	           
	        
	    	var d = '<select class="selectpicker" data-live-search="true" id="subcategory_code" name="subcategory_code" required>';
	    		d += '<option value="">Select Sub Category</option>';
	    	for(var i = 0 ; i < D.length ; i++ ){
    			d += '<option value="'+D[i].code+'">'+D[i].description+'</option>';
	    	}	d += '</select>';

	    	$("#divsubcategory").html(d);
	    	$('.selectpicker').selectpicker();

	    }
	});
}

function load_data(){

	showProgessBox('Loading data...');

	$.ajax({
	    url: '/'+mstrurl+'/show',
	    dataType: 'json',
	    type: 'post',	    
	    data: {  }  ,
	    success: function( D ){	 

	    	showProgessBox();  	
            
	    	if (D.success == 1){

	    		var t = '';	    		
	    		
	    		for (var i = 0 ; i < D.data.length; i++) {
	    			
	    			t += '<tr>'; 
	    			t += '<td>'+D.data[i].code+'</td>'; 
	    			t += '<td>'+D.data[i].description+'</td>';

					t += '<td>'+D.data[i].dept_desc+'</td>';
					t += '<td>'+D.data[i].category_code+'</td>';
					t += '<td>'+D.data[i].subcategory_code+'</td>';
					t += '<td>'+D.data[i].brand_code+'</td>';
					t += '<td>'+D.data[i].model+'</td>';
					t += '<td>'+D.data[i].size_code+'</td>';
					t += '<td>'+D.data[i].unit_code+'</td>';
					t += '<td>'+D.data[i].purchase_price+'</td>';
					t += '<td>'+D.data[i].inactive+'</td>';
					t += '<td>'+D.data[i].is_fa+'</td>';
					t += '<td>'+D.data[i].dip_period+'</td>';
					t += '<td>'+D.data[i].dip_per+'</td>';
					t += '<td>'+D.data[i].is_batch+'</td>';
					t += '<td>'+D.data[i].fa_acc+'</td>';
					t += '<td>'+D.data[i].dip_acc+'</td>';
					t += '<td>'+D.data[i].ca_period+'</td>';
					t += '<td>'+D.data[i].ca_per  +'</td>';


	    			t += '<td><a class="action_link" code="'+D.data[i].code+'" action="edit">Edit</a> | <a class="action_link" code="'+D.data[i].code+'" action="delete">Delete</a></td>'; 
	    			t += '</tr>';
	    		}

	    		

	    		$("#list_view").html(t);

	    	}else{
            	alert("Error")
	    	}            
	        
	    }
	});

}

$(document).on("click",".action_link",function(){
	
	if ($(this).attr("action") == "edit"){
		set_edit( $(this) );
	}

	if ($(this).attr("action") == "delete"){
		
		var obj = $(this);

		BootstrapDialog.confirm({
	        title: 'Confirm',
	        message: 'Do you want delete this record?',	        
	        callback: function(result) {
	            if(result) { make_delete(obj); }
	        }
	    });

		
	}
})

function make_delete(OBJ){

	showProgessBox('Deleting...');

	$.ajax({
	    url: '/'+mstrurl+'/destroy',
	    dataType: 'json',
	    type: 'post',	    
	    data: {code : OBJ.attr("code") },
	    success: function( D ){	    	
    
	    	$("#ModalProgress").modal('hide');

	    	if (D.success == 1){
	    		
	    		reset_form(D.next_id);
	    		load_data();

	    	}else{

	    		showInfoBox("Database Error:"+D.errors[1],D.errors[2],'c');	    		

	    	}            
	        
	    }
	});

}

function set_edit(OBJ){

	showProgessBox('Loading data...');

	$.ajax({
	    url: '/'+mstrurl+'/edit',
	    dataType: 'json',
	    type: 'post',	    
	    data: {code : OBJ.attr("code")},
	    success: function( D ){	    	
            
			showProgessBox();

	    	if (D.success == 1){

	    		glob_next_id = $("#code").val();

	    		$("#is_edit").val(1);
	    		$(".btnAddItem").text("Update");

	    		$("#code").val( D.data[0].code )
				$("#description").val(D.data[0].description);
	    		$('select[name=department_code]').val(D.data[0].department_code);
	    		$('select[name=category_code]').val(D.data[0].category_code);
	    		$.when( set_sub_category_list_by_category(D.data[0].category_code) ).then( $('select[name=subcategory_code]').val(D.data[0].subcategory_code) ,$('.selectpicker').selectpicker('refresh'));	    		
	    		$('select[name=brand_code]').val(D.data[0].brand_code);
	    		$("#model").val(D.data[0].model);
	    		$('select[name=size_code]').val(D.data[0].size_code);
	    		$('select[name=unit_code]').val(D.data[0].unit_code);
	    		$('.selectpicker').selectpicker('refresh');
	    		$("#purchase_price").val(D.data[0].purchase_price);	    		
	    		D.data[0].inactive == 1 ? $("#inactive").prop("checked",true) : $("#inactive").prop("checked",false);
	    		//D.data[0].is_fa == 1 ? $("#is_fa").prop("checked",true) : $("#is_fa").prop("checked",false);
	    		$("#dip_period").val(D.data[0].dip_period);
	    		$("#dip_per").val(D.data[0].dip_per);
	    		D.data[0].is_batch == 1 ? $("#is_batch").prop("checked",true) : $("#is_batch").prop("checked",false);
	    		D.data[0].is_fa == 1 ? $("#is_fa").prop("checked",true) : $("#is_fa").prop("checked",false);
	    		$("#fa_acc").val(D.data[0].fa_acc);
	    		$("#dip_acc").val(D.data[0].dip_acc);
	    		$("#ca_period").val(D.data[0].ca_period);
	    		$("#ca_per").val(D.data[0].ca_per);	


	    		if (D.item_img != ''){	    			
	    			$(".imgview").html("<img width='140' height='140' src='img/"+D.item_img+".jpg'>");
	    		}else{
	    			$(".imgview").html("");
	    		}


	    		$("#ModalMasterPopup").modal();			

	    	}else{
            	
	    	}            
	        
	    }
	});

}


$(".btnAddItem").on("click",function(e){
	
	e.preventDefault();
	$(".mm").html("<span class='mmspn1'>Saving...</span>");

	var f = $("#f");

	var formData = new FormData();	
	
	
	$("#f *").filter(":input[type='text'],[type='hidden'],[type='number']").each(function(){		
		if ($(this).attr('name') != undefined){
			formData.append($(this).attr('name'), $(this).val());
		}
	});

	$("#f select").each(function(){
		formData.append($(this).attr('name'), $(this).val());		
	});

	$("#f *").filter(':input[type="checkbox"],[type="radio"]').each(function(){		
		if ( $(this).is(":checked") ){			
			formData.append($(this).attr('name'), 1);		
		}		
	});
	
	formData.append('image', $('input[type=file]')[0].files[0]);

	$.ajax({
	    url: f.attr('action'),
	    dataType: 'json',
	    type: f.attr('method'),	    
	    data: formData ,
	    contentType: false, 
	    processData: false,
	    success: function( D ){	    	
            // showProgessBox();
	    	if (D.success == 1){	    		
	    		load_data();
	    		reset_form(D.next_id);
	    		$("#is_edit").val(0);
	    		$(".btnAddItem").text("Save");	    
	    		glob_next_id	= D.next_id;	
	    		$("#code").val(glob_next_id);

	    		$(".mm").html("<span class='mmspn2'>Saving Success</span>");
	    		setTimeout('$("#ModalMasterPopup").modal("hide");$(".mm").html("")',1000);

	    	}else{
            	
            	//set_error_msgs(D);

            	if (D.errors_a === undefined ){	
					$.each(D.errors, function (key, val) {                
					    $( "#"+key ).css("border","1px solid red");
					    $( "."+key ).remove();
					    $( "#"+key ).after( "<p class='"+key+" error_text_after'>"+val+"</p>" );
					    $( "#"+key ).focus();
					});	

					$(".mm").html("<span class='mmspn3'>Incorrect Values: Data not saved</span>");

				}else{					
					//showInfoBox("Database Error:"+D.errors[1],D.errors[2],'c');
					$(".mm").html("<span class='mmspn3'>"+"Error:"+D.errors[1] + " " +D.errors[2]+"</span>");
				}

	    	}            
	        
	    }
	});

});


function reset_form(next_id){
	$("#f").find('input').val('');
	$("#f").find('input[type="checkbox"]').prop('checked',false);
	$("#code").val(next_id);
	
	$('select[name=department_code]').val('0');
	$('select[name=category_code]').val('0');
	$('select[name=subcategory_code]').val('0');
	$('select[name=brand_code]').val('0');
	$('select[name=size_code]').val('0');
	$('select[name=unit_code]').val('0');

	$('.selectpicker').selectpicker('refresh');
}
