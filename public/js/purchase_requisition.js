
var mstrurl = 'purchase_requisition';

$(document).ready(function(){

	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});
	
});

$(document).on("click","#item_code",function(e){

	e.preventDefault();	
	$("#ItemSearchPopup").modal();

});

function make_delete(OBJ){

	showProgessBox('Deleting...');

	$.ajax({
	    url: '/'+mstrurl+'/destroy',
	    dataType: 'json',
	    type: 'post',	    
	    data: {code : OBJ.attr("code") },
	    success: function( D ){	    	
            
	    	showProgessBox();

	    	if (D.success == 1){
	    		
	    		reset_form(D.next_id);
	    		load_data();

	    	}else{
            	
	    	}            
	        
	    }
	});

}

function set_edit(OBJ){

	showProgessBox('Loading data...');

	$.ajax({
	    url: '/'+mstrurl+'/edit',
	    dataType: 'json',
	    type: 'post',	    
	    data: {code : OBJ.attr("code")},
	    success: function( D ){	    

	    	showProgessBox();	
            
	    	if (D.success == 1){
	    		
	    		$("#is_edit").val(1);
	    		$(".btnx").text("Update");

	    		$("#code").val( D.data[0].code );
	    		
	    		$('select[name=cl]').val(D.data[0].cl);
	    		$('.selectpicker').selectpicker('refresh');
				$.when( set_bclistbycl(D.data[0].cl) ).then( $("#bc").val(D.data[0].bc),$('.selectpicker').selectpicker('refresh'));

	    		D.data[0].purchase == 1 ? $("#purchase").prop("checked",true) : $("#purchase").prop("checked",false);
	    		D.data[0].transfer_location == 1 ? $("#transfer_location").prop("checked",true) : $("#transfer_location").prop("checked",false);

				$("#description").val(D.data[0].description);				

	    	}else{
            	
	    	}            
	        
	    }
	});

}


$("#f").on("submit",function(e){
	
	e.preventDefault();	
	showProgessBox('Saving...');

	$.ajax({
	    url: $(this).attr('action'),
	    dataType: 'json',
	    type: $(this).attr('method'),	    
	    data: $(this).serializeArray() ,
	    success: function( D ){	    	

	    	showProgessBox();
            
	    	if (D.success == 1){	    		
	    		load_data();
	    		reset_form(D.next_id);
	    		$("#is_edit").val(0);
	    		$(".btnx").text("Save");
	    		$("#code").val(D.next_id);
	    	}else{
            	set_error_msgs(D.errors);
	    	}            
	        
	    }
	});

});


function reset_form(next_id){
	$("#f").find('input').val('').prop("checked",false);
	$("#code").val(next_id);
	$('select[name=cl]').val('0');
	$('select[name=bc]').val('0');
	$('.selectpicker').selectpicker('refresh');
}
