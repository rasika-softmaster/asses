
var mstrurl = 'fa_registry';

$(document).ready(function(){

	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});


	$('select[name=trans_code]').val(2);
	$('select[name=pur_type]').val(2);
	$('.selectpicker').selectpicker('refresh');
	
});

$(document).on("blur","#serial_no",function(){
	checkSerialExit($(this).val());
});

$(document).on("click","#s_item_code",function(e){
	e.preventDefault();	
	$("#ItemSearchPopup").modal();
});

$(document).on("change","select[name=s_category_code]",function(){
	set_sub_category_list_by_category($(this));
});

function checkSerialExit(serial_no){

	//showProgessBox('Checking serial no...');

	$.ajax({
	    url: '/fa_registry/checkSerialExit',
	    dataType: 'json',
	    type: 'post',	    
	    data: {serial_no : serial_no },
	    success: function( D ){	    	
            
	    	// showProgessBox();

	    	if (D.s == 1){
	    		
	    		alert("Serial number already add");
	    		$("#serial_no").focus().val('').select();
	    		

	    	}else{
            	
	    	}            
	        
	    }
	});

}

function make_delete(OBJ){

	showProgessBox('Deleting...');

	$.ajax({
	    url: '/'+mstrurl+'/destroy',
	    dataType: 'json',
	    type: 'post',	    
	    data: {code : OBJ.attr("code") },
	    success: function( D ){	    	
            
	    	showProgessBox();

	    	if (D.success == 1){
	    		
	    		reset_form(D.next_id);	    		

	    	}else{
            	
	    	}            
	        
	    }
	});

}

function set_edit(OBJ){

	showProgessBox('Loading data...');

	$.ajax({
	    url: '/'+mstrurl+'/edit',
	    dataType: 'json',
	    type: 'post',	    
	    data: {code : OBJ.attr("code")},
	    success: function( D ){	    

	    	showProgessBox();	
            
	    	if (D.success == 1){
	    		
	    		$("#is_edit").val(1);
	    		$(".btnx").text("Update");

	    		$("#code").val( D.data[0].code );
	    		
	    		$('select[name=cl]').val(D.data[0].cl);
	    		$('.selectpicker').selectpicker('refresh');
				$.when( set_bclistbycl(D.data[0].cl) ).then( $("#bc").val(D.data[0].bc),$('.selectpicker').selectpicker('refresh'));

	    		D.data[0].purchase == 1 ? $("#purchase").prop("checked",true) : $("#purchase").prop("checked",false);
	    		D.data[0].transfer_location == 1 ? $("#transfer_location").prop("checked",true) : $("#transfer_location").prop("checked",false);

				$("#description").val(D.data[0].description);				

	    	}else{
            	
	    	}            
	        
	    }
	});

}


$("#f").on("submit",function(e){
	
	e.preventDefault();

	if ($("#is_edit").val() == 0) { showProgessBox('Saving...'); }else{showProgessBox('Updating...');}

	$(".btnSave").attr("disabled",true);	

	$.ajax({
	    url: $(this).attr('action'),
	    dataType: 'json',
	    type: $(this).attr('method'),	    
	    data: $(this).serializeArray() ,
	    success: function( D ){	    	

	    	showProgessBox();
            
	    	if (D.success == 1){
	    		
	    		//reset_form(D.next_id);

	    		alert("Saved success");

	    		location.href = '';
	    		
	    		$("#is_edit").val(0);
	    		$(".btnx").text("Save");
	    		$("#code").val(D.next_id);
	    		
	    	}else{

	    		if (D.errors.item_code != undefined){
		    		if (D.errors.item_code[0] == 'required'){
		    			alert("Selecting an item is required");
		    			$("#s_item_code").focus();
		    			return;
		    		}
		    	}

            	set_error_msgs(D);
	    	} 

	    	$(".btnSave").attr("disabled",false);	           
	        
	    }
	});

});


function reset_form(next_id){

	//alert('Reset form now');

	var d = $("#trans_date").val();

	$("#f").find('input').val('').prop("checked",false);
	$("#is_edit").val(0);
	$("#code").val(next_id);
	$("#item_code").val("");
	$("#trans_date").val(d);
	$(".btnSave").attr("disabled",false);
	$("#serial_no").attr("readonly",false);
	$('select[name=trans_code]').val('0');
	$('select[name=location_code]').val('0');
	$('select[name=supplier_code]').val('0');	
	$('select[name=pur_type]').val('');	
	$('select[name=pur_supplier]').val('');	
	$('.selectpicker').selectpicker('refresh');
}

$(document).on("click",".btnSearch",function(e){
	var d = $("#fs");

	e.preventDefault();	
	$(".btnSearch").html('Searching...').addClass('disabled');

	$.ajax({
	    url: d.attr('action'),
	    dataType: 'json',
	    type: d.attr('method'),	    
	    data: d.serializeArray() ,
	    success: function( D ){	    	

	    	$(".btnSearch").html('Search').removeClass('disabled');

	        var T = '<table border="0" width="100%" class="tbl_item_search_a">';

	        	T += '<tr>';
				T += '<td>code</td>';
				T += '<td>Description</td>';
				T += '<td>Department</td>';
				T += '<td>Category</td>';
				T += '<td>Sub Category</td>';
				T += '<td>Brand</td>';
				T += '<td>Model</td>';
				T += '<td>Size</td>';
				T += '<td>Unit</td>';
				T += '<td>Purchase Price</td>';
				T += '</tr>';

	        for(n = 0 ; n < D.count ; n++ ){

				T += '<tr class="s_item_search_row" code="'+D.data[n].code+'" description="'+D.data[n].description+'" model="'+D.data[n].model+'">';
				T += '<td>' + D.data[n].code + '</td>';
				T += '<td>' + D.data[n].description + '</td>';
				T += '<td>' + D.data[n].department_code + '</td>';
				T += '<td>' + D.data[n].category_code + '</td>';
				T += '<td>' + D.data[n].subcategory_code + '</td>';
				T += '<td>' + D.data[n].brand_code + '</td>';
				T += '<td>' + D.data[n].model + '</td>';
				T += '<td>' + D.data[n].size_code + '</td>';
				T += '<td>' + D.data[n].unit_code + '</td>';
				T += '<td>' + D.data[n].purchase_price + '</td>';
				T += '</tr>';

	        }

	        T += '</table>';

	    	$(".div-search-result").html(T);


	    }
	});
});

function set_sub_category_list_by_category(obj){

	$.ajax({
	    url: '/Customfunctions/subcategorylistbycategory',
	    dataType: 'json',
	    type: 'post',	    
	    data: { category_code : jQuery.type( obj ) === "object" ? obj.val() : obj  }  ,	    
	    async: false,
	    success: function( D ){
	        
	    	var d = '<select class="selectpicker" data-live-search="true" id="subcategory_code" name="subcategory_code" required>';
	    		d += '<option value="">Select Sub Category</option>';
	    	for(var i = 0 ; i < D.length ; i++ ){
    			d += '<option value="'+D[i].code+'">'+D[i].description+'</option>';
	    	}	d += '</select>';

	    	$("#divsubcategory").html(d);
	    	$('.selectpicker').selectpicker();

	    }
	});
}




$(document).on("click",".s_item_search_row",function(){

	$("#s_item_code").val($(this).attr("description") + " - " + $(this).attr("model"));
	$("#item_code").val($(this).attr("code"));
	getItemDet($(this).attr("code"));
	$('#ItemSearchPopup').modal('hide');

	clearSearchInput();

});


function clearSearchInput(){

	$("#s_code").val('');
	$("#s_description").val('');
	$("#model").val('');
	$("#purchase_price").val('');
	
	$('select[name=s_department_code]').val('0');	
	$('select[name=s_category_code]').val('0');	
	$('select[name=subcategory_code]').val('0');	
	$('select[name=s_brand_code]').val('0');	
	$('select[name=s_size_code]').val('0');	
	$('select[name=s_unit_code]').val('0');	
	
	
	$('.selectpicker').selectpicker('refresh');

	$(".tbl_item_search_a").html('');

}

function getItemDet(code){


	$.ajax({
	    url: '/Customfunctions/setItemDet',
	    dataType: 'json',
	    type: 'post',	    
	    data: { code : code  }  ,	    
	    async: false,
	    success: function( D ){
	        
	    	$("#dep_rate").val(D.sum.dip_per);
	    	$("#dep_period").val(D.sum.dip_period);	    	
	    	$("#dep_acc_dep").val(D.sum.dip_acc);
	    	$("#pur_cost").val(D.sum.purchase_price);
	    	$("#cap_period").val(D.sum.ca_period);
	    	$("#cap_rate").val(D.sum.ca_per);
	    	

	    }
	});


}


$(document).on("change","#trans_code",function(){
	
	alert("trans_code chaged");

});


$("#trans_no").on('keyup', function (e) {
    if (e.keyCode == 13) {        
    	loadFAregistry($(this).val());
    }
});


function loadFAregistry(trans_no){

	showProgessBox('Please wait...');

	$.ajax({
	    url: '/Customfunctions/loadFAregistry',
	    dataType: 'json',
	    type: 'post',	    
	    data: { trans_no : trans_no , trans_code : $("#trans_code :selected").val()  }  ,	    
	    async: false,
	    success: function( D ){    	

	    	showProgessBox();

	    	if (D.s == 1){

	    		$("#is_edit").val( trans_no );

				$("#s_item_code").val(D.sum.item + " - " + D.sum.model);
				$("#item_code").val(D.sum.item_code);
				$("#trans_no").val(D.sum.trans_no);
				$("#trans_date").val(D.sum.trans_date);
				// $("#serial_no").val(D.sum.serial_no).attr("readonly",true);
				$("#serial_no").val(D.sum.serial_no).attr("readonly",false);
				$("#pur_no").val(D.sum.pur_no);
				$("#pur_date").val(D.sum.pur_date);
				$("#pur_cost").val(D.sum.pur_cost);
				$("#dep_rate").val(D.sum.dep_rate);
				$("#dep_period").val(D.sum.dep_period);
				$("#dep_value").val(D.sum.dep_value);
				$("#dep_opb_date").val(D.sum.dep_opb_date);
				$("#dep_month").val(D.sum.dep_month);
				$("#dep_acc_dep").val(D.sum.dep_acc_dep);
				$("#dep_opb_value").val(D.sum.dep_opb_value);
				$("#dep_nbv").val(D.sum.dep_nbv);
				$("#cap_rate").val(D.sum.cap_rate);
				$("#cap_period").val(D.sum.cap_period);
				$("#cap_n_years").val(D.sum.cap_n_years);
				$("#cap_total").val(D.sum.cap_total);
				$("#cap_for_year").val(D.sum.cap_for_year);
				$("#cap_balance").val(D.sum.cap_balance);
				$("#cap_op_amount").val(D.sum.cap_op_amount);
				$("#cap_temp_diff").val(D.sum.cap_temp_diff);

				$("#location_code").val(D.sum.location_code);
				$("#pur_type").val(D.sum.pur_type);
				$("#pur_supplier").val(D.sum.pur_supplier);
				$('.selectpicker').selectpicker('refresh');

				if (D.sum.inactive == 1){
					$("#inactive").prop("checked",true);
				}else{
					$("#inactive").prop("checked",false);
				}

	    	}
	    	

	    }
	});

}