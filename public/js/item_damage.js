	

var mstrurl = 'item_damage';
var sid_no="";


$(document).ready(function(){


		$.ajaxSetup({

	    headers: {

	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

	    }

	});

	$('select[name=trans_code]').val(2);

	$('select[name=pur_type]').val(2);

	$('.selectpicker').selectpicker('refresh');


	// f2

	$(".cr").keyup(function(e) {
       
     
        set_cid($(this).attr("id"));
        if (e.keyCode == 112) {
           $("#ItemSearchPopup").modal();
				load_items();
        }
		if(e.keyCode == 46){
			$("#code_" + sid_no).val("");
			$("#des_" + sid_no).val("");
			$("#model_" + sid_no).val("");
			$("#price_" + sid_no).val("");
			$("#amount_" + sid_no).val("");
			$("#qty_" + sid_no).val("");

		
		}


    });

    $(".qty").keyup(function(e) {
       
     
        set_cid($(this).attr("id"));
      
       
       

    });
    
	$("#no").keydown(function (e) {
		if (e.keyCode == 13) {
		
			$("#edit_id").val($(this).val());
			$("#is_edit").val("1");
			load_data($(this).val());
		}

	});
     $("#save").click(function(){
     	if(validation()){
     		save();
     		

     	}
     	
     })
    
	
	

});
function empty_grid(){
	for (var i = 0; i < 25; i++) {
		$("#code_" + i).val("");
		$("#des_" + i).val("");
		$("#model_" + i).val("");
		$("#price_" + i).val("");
		$("#amount_" + i).val("");
		$("#qty_" + i).val("");
	}
}
function load_data(id) {
	showProgessBox('Loading data...');

	empty_grid();
	$("#edit_id").val(id);
	$("#is_edit").val("1");
	   $.ajax({

		   url: '/item_damage/load_data',
		   dataType: 'json',
		   type: 'post',
		   data: {
			   edit_id: id
		   
		   },
		   success: function (D) {
			showProgessBox();

			   console.log(D);
			   if (D.success == "1") {
				   console.log(D.records.sum.length);

				   if (D.records.sum.length > 0 && D.records.det.length > 0) {

					   //for sum details bind 
					   
						   $("#store_code").val(D.records.sum[0].to_location);
					   
						 
						   
						   $("#description").val(D.records.sum[0].description);
						   $("#date").val(D.records.sum[0].ddate);


					   //for item details list
					   D.records.det.forEach(function (d, index){
						   console.log(d.code);
						   $("#code_" + index).val(d.code);
						   $("#des_" + index).val(d.description);
						   $("#model_" + index).val(d.model);
						   
						   $("#price_" + index).val(d.price);
						   $("#amount_" + index).val(d.price*d.qty);
						   $("#qty_" + index).val(d.qty);
						

					   });

					   $('.selectpicker').selectpicker('refresh')

				   } else {
					   alert("Record Not Found");
				   }


			   } else {
				   alert(D.Message);

			   }

		   

		   }

	   }, "json");
   }


function validation(){

	 if ($("#store_code").val() ==""){
		alert("Please Select Store");
		return false;

	}else if ($("#description").val() ==""){
		alert("Description Can not Be Empty !");
		return false;

	}else if ($("#date").val() ==""){
		alert("Date Can not Be Empty !");
		return false;

    }else{
		return true;
	}
	
}
function set_cid(id){
    id = id.split('_');
    sid_no = id[1];
}

$("#s_code").keyup(function(){
	load_items();
})

$(document).on("click",".s_item_search_row",function(){

	$("#code_"+sid_no).val($(this).attr("code"));
	$("#des_"+sid_no).val($(this).attr("description"));
	$("#model_"+sid_no).val($(this).attr("model"));
	$("#ItemSearchPopup").modal("hide");

});

function load_items(){

	var d = $("#fs");
	 $("#myTable").find("tr:gt(0)").remove();
	$.ajax({

	    url: d.attr('action'),

	    dataType: 'json',

	    type: d.attr('method'),	    

	    data: d.serializeArray() ,

	    success: function( D ){	 
            console.log(D.data.length);   	
	    	$(".btnSearch").html('Search').removeClass('disabled');
	        var T = '<table border="0" width="100%" class="tbl_item_search_a" id="myTable">';
	        	T += '<tr>';
				T += '<td>code</td>';
				T += '<td>Description</td>';
				T += '<td>Model</td>';
				T += '</tr>';
	        for(n = 0 ; n < D.data.length ; n++ ){
				T += '<tr class="s_item_search_row" code="'+D.data[n].code+'" description="'+D.data[n].description+'" model="'+D.data[n].model+'">';

				T += '<td>' + D.data[n].code + '</td>';
				T += '<td>' + D.data[n].description + '</td>';
				T += '<td>' + D.data[n].model + '</td>';
				T += '</tr>';
	        }

	        T += '</table>';

	    	$(".div-search-result").html(T);

	    }

	});

}





function save(){

	 var frm = $('#item_damage');
	$.ajax({

	    url: '/item_damage/store',
	    dataType: 'json',
	    type: 'post',	    
	    data: frm.serialize(),
	    success: function( D ){	 
	    console.log(D);   	
	    	if (D.success == "1"){

	    		alert(D.message);
	    		location.reload(); 

	    	}else{
	    		alert(D.Message);
            	
	    	}            

	        

	    }

	},"json");

}












