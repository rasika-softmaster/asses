
var mstrurl = 'supplier';

$(document).ready(function(){

	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	load_data();
	
});

function load_data(){

	showProgessBox('Loading data...');

	$.ajax({
	    url: '/'+mstrurl+'/show',
	    dataType: 'json',
	    type: 'post',	    
	    data: {  }  ,
	    success: function( D ){	 

	    	showProgessBox();  	
            
	    	if (D.success == 1){

	    		var t =  '<table border="1" width="100%" class="tbl_master">';

					t += '<thead>'; 
					t += '<tr>'; 
					t += '<td>Code</td>'; 
					t += '<td>Supplier Category</td>'; 
					t += '<td>Name</td>';

					t += '<td>Address 1</td>';					
					t += '<td>Tel 1</td>';
					t += '<td>Tel 2</td>';
					t += '<td>Tel 3</td>';
					t += '<td>Fax</td>';
					t += '<td>Contact Person</td>';
					t += '<td>Contact Mobile</td>';
					t += '<td>Credit Limit</td>';
					t += '<td>Credit Period</td>';



					t += '<td>Action</td>'; 
					t += '</tr>'; t += '</thead>';
	    		
	    		for (var i = 0 ; i < D.data.length; i++) {
	    			
	    			t += '<tr>'; 
	    			t += '<td>'+D.data[i].code+'</td>'; 
	    			t += '<td>'+D.data[i].suppliercategory_code+'</td>'; 
	    			t += '<td>'+D.data[i].name+'</td>';  

	    			t += '<td>'+D.data[i].address1+'</td>';	    			
	    			t += '<td>'+D.data[i].tel1+'</td>';
	    			t += '<td>'+D.data[i].tel2+'</td>';
	    			t += '<td>'+D.data[i].tel3+'</td>';
	    			t += '<td>'+D.data[i].fax+'</td>';
	    			t += '<td>'+D.data[i].con_person+'</td>';
	    			t += '<td>'+D.data[i].con_mobile+'</td>';
	    			t += '<td>'+D.data[i].credit_limit+'</td>';
	    			t += '<td>'+D.data[i].credit_period+'</td>';

	    			t += '<td><a class="action_link" code="'+D.data[i].code+'" action="edit">Edit</a> | <a class="action_link" code="'+D.data[i].code+'" action="delete">Delete</a></td>'; 
	    			t += '</tr>';
	    		}

	    		t += '</table>';

	    		$("#list_view").html(t);

	    	}else{
            	alert("Error")
	    	}            
	        
	    }
	});

}

$(document).on("click",".action_link",function(){
	
	if ($(this).attr("action") == "edit"){
		set_edit( $(this) );
	}

	if ($(this).attr("action") == "delete"){
		
		var obj = $(this);

		BootstrapDialog.confirm({
	        title: 'Confirm',
	        message: 'Do you want delete this record?',	        
	        callback: function(result) {
	            if(result) { make_delete(obj); }
	        }
	    });

		
	}
})

function make_delete(OBJ){

	showProgessBox('Deleting...');

	$.ajax({
	    url: '/'+mstrurl+'/destroy',
	    dataType: 'json',
	    type: 'post',	    
	    data: {code : OBJ.attr("code") },
	    success: function( D ){	    	
    
	    	$("#ModalProgress").modal('hide');

	    	if (D.success == 1){
	    		
	    		reset_form(D.next_id);
	    		load_data();

	    	}else{

	    		showInfoBox("Database Error:"+D.errors[1],D.errors[2],'c');	    		

	    	}            
	        
	    }
	});

}

function set_edit(OBJ){

	showProgessBox('Loading data...');

	$.ajax({
	    url: '/'+mstrurl+'/edit',
	    dataType: 'json',
	    type: 'post',	    
	    data: {code : OBJ.attr("code")},
	    success: function( D ){	    	
            
			showProgessBox();

	    	if (D.success == 1){
	    		
	    		$("#is_edit").val(1);
	    		$(".btns").text("Update");

	    		$("#code").val( D.data[0].code );
	    		$('select[name=suppliercategory_code]').val(D.data[0].suppliercategory_code);
	    		$('.selectpicker').selectpicker('refresh');
				$("#name").val(D.data[0].name);

				$("#address1").val(D.data[0].address1);    			
    			
    			$("#tel1").val(D.data[0].tel1);
    			$("#tel2").val(D.data[0].tel2);
    			$("#tel3").val(D.data[0].tel3);
    			$("#fax").val(D.data[0].fax);
    			$("#con_person").val(D.data[0].con_person);
    			$("#con_mobile").val(D.data[0].con_mobile);
    			$("#credit_limit").val(D.data[0].credit_limit);
    			$("#credit_period").val(D.data[0].credit_period);



				

	    	}else{
            	
	    	}            
	        
	    }
	});

}


$("#f").on("submit",function(e){
	e.preventDefault();
	showProgessBox('Saving...');
	
	$.ajax({
	    url: $(this).attr('action'),
	    dataType: 'json',
	    type: $(this).attr('method'),	    
	    data: $(this).serializeArray() ,
	    success: function( D ){	    	
            showProgessBox();
	    	if (D.success == 1){	    		
	    		load_data();
	    		reset_form(D.next_id);
	    		$("#is_edit").val(0);
	    		$(".btns").text("Save");
	    		$("#code").val(D.next_id);
	    	}else{
            	set_error_msgs(D);
	    	}            
	        
	    }
	});

});


function reset_form(next_id){
	$("#f").find('input').val('');
	$("#code").val(next_id);
}
