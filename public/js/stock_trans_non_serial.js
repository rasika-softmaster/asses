var mstrurl = 'stock_transfer_non_serial';



$(document).ready(function () {

	$("#trans_date,#pur_date,#dep_opb_date").datepicker({

		dateFormat: 'yy-mm-dd'

	});


	$.ajaxSetup({

		headers: {

			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

		}

	});





	$('select[name=trans_code]').val(2);

	$('select[name=pur_type]').val(2);

	$('.selectpicker').selectpicker('refresh');



});



$(document).on("click", "#s_item_code", function (e) {

	e.preventDefault();

	$("#ItemSearchPopup").modal();

});



$(document).on("change", "select[name=s_category_code]", function () {

	set_sub_category_list_by_category($(this));

});



function make_delete(OBJ) {



	showProgessBox('Deleting...');



	$.ajax({

		url: '/' + mstrurl + '/destroy',

		dataType: 'json',

		type: 'post',

		data: {
			code: OBJ.attr("code")
		},

		success: function (D) {



			showProgessBox();



			if (D.success == 1) {



				reset_form(D.next_id);



			} else {



			}



		}

	});



}



function set_edit(OBJ) {



	showProgessBox('Loading data...');



	$.ajax({

		url: '/' + mstrurl + '/load_data',

		dataType: 'json',

		type: 'post',

		data: {
			code: OBJ.attr("code")
		},

		success: function (D) {



			showProgessBox();



			if (D.success == 1) {



				$("#is_edit").val(1);

				$(".btnx").text("Update");



				$("#code").val(D.data[0].code);



				$('select[name=cl]').val(D.data[0].cl);

				$('.selectpicker').selectpicker('refresh');

				$.when(set_bclistbycl(D.data[0].cl)).then($("#bc").val(D.data[0].bc), $('.selectpicker').selectpicker('refresh'));



				D.data[0].purchase == 1 ? $("#purchase").prop("checked", true) : $("#purchase").prop("checked", false);

				D.data[0].transfer_location == 1 ? $("#transfer_location").prop("checked", true) : $("#transfer_location").prop("checked", false);



				$("#description").val(D.data[0].description);



			} else {



			}



		}

	});



}





$("#saveForm").on("click", function (e) {

	if (validation()) {
		save(e);
	}

});


function save(e) {




	// e.preventDefault();

	var frm = $('#f');

	if ($("#is_edit").val() == 0) {
		showProgessBox('Saving...');
	} else {
		showProgessBox('Updating...');
	}



	$(".btnSave").attr("disabled", true);



	$.ajax({

		url: frm.attr('action'),

		dataType: 'json',

		type: frm.attr('method'),

		data: frm.serializeArray(),

		success: function (D) {



			showProgessBox();



			if (D.success == 1) {



				//reset_form(D.next_id);



				alert(D.m);



				location.reload();


				$("#is_edit").val(0);

				$(".btnx").text("Save");

				$("#code").val(D.next_id);



			} else if (D.success == 2) {



				alert(D.m);



			} else {



				if (D.errors.item_code != undefined) {

					if (D.errors.item_code[0] == 'required') {

						alert("Selecting an item is required");

						$("#s_item_code").focus();

						return;

					}

				}



				set_error_msgs(D);

			}



			$(".btnSave").attr("disabled", false);



		}

	});



}




function reset_form(next_id) {



	//alert('Reset form now');



	var d = $("#trans_date").val();



	$("#f").find('input').val('').prop("checked", false);

	$("#is_edit").val(0);

	$("#code").val(next_id);

	$("#item_code").val("");

	$("#trans_date").val(d);

	$(".btnSave").attr("disabled", false);

	$("#serial_no").attr("readonly", false);

	$('select[name=trans_code]').val('0');

	$('select[name=location_code]').val('0');

	$('select[name=supplier_code]').val('0');

	$('select[name=pur_type]').val('');

	$('select[name=pur_supplier]').val('');

	$('.selectpicker').selectpicker('refresh');

}



$(document).on("click", ".btnSearch", function (e) {

	var d = $("#fs");



	e.preventDefault();

	$(".btnSearch").html('Searching...').addClass('disabled');



	$.ajax({

		url: d.attr('action'),

		dataType: 'json',

		type: d.attr('method'),

		data: d.serializeArray(),

		success: function (D) {



			$(".btnSearch").html('Search').removeClass('disabled');



			var T = '<table border="0" width="100%" class="tbl_item_search_a">';



			T += '<tr>';

			T += '<td>code</td>';



			T += '<td>Description</td>';



			T += '<td>Department</td>';

			T += '<td>Category</td>';

			T += '<td>Sub Category</td>';

			T += '<td>Brand</td>';

			T += '<td>Model</td>';

			T += '<td>Size</td>';

			T += '<td>Unit</td>';

			T += '<td>Purchase Price</td>';

			T += '</tr>';



			for (n = 0; n < D.count; n++) {



				T += '<tr class="s_item_search_row" code="' + D.data[n].code + '" description="' + D.data[n].description + '" model="' + D.data[n].model + '"    >';

				T += '<td>' + D.data[n].code + '</td>';



				T += '<td>' + D.data[n].description + '</td>';



				T += '<td>' + D.data[n].department_code + '</td>';

				T += '<td>' + D.data[n].category_code + '</td>';

				T += '<td>' + D.data[n].subcategory_code + '</td>';

				T += '<td>' + D.data[n].brand_code + '</td>';

				T += '<td>' + D.data[n].model + '</td>';

				T += '<td>' + D.data[n].size_code + '</td>';

				T += '<td>' + D.data[n].unit_code + '</td>';

				T += '<td>' + D.data[n].purchase_price + '</td>';

				T += '</tr>';



			}



			T += '</table>';



			$(".div-search-result").html(T);





		}

	});

});



function set_sub_category_list_by_category(obj) {



	$.ajax({

		url: '/Customfunctions/subcategorylistbycategory',

		dataType: 'json',

		type: 'post',

		data: {
			category_code: jQuery.type(obj) === "object" ? obj.val() : obj
		},

		async: false,

		success: function (D) {



			var d = '<select class="selectpicker" data-live-search="true" id="subcategory_code" name="subcategory_code" required>';

			d += '<option value="">Select Sub Category</option>';

			for (var i = 0; i < D.length; i++) {

				d += '<option value="' + D[i].code + '">' + D[i].description + '</option>';

			}
			d += '</select>';



			$("#divsubcategory").html(d);

			$('.selectpicker').selectpicker();



		}

	});

}









$(document).on("click", ".s_item_search_row", function () {



	$("#s_item_code").val($(this).attr("description") + " - " + $(this).attr("model"));

	$("#item_code").val($(this).attr("code"));

	getItemDet($(this).attr("code"), $(this).attr("serial_no"), $(this).attr("current_location"), $(this).attr("current_location_desc"));

	$('#ItemSearchPopup').modal('hide');



	clearSearchInput();



});





function clearSearchInput() {



	$("#s_code").val('');

	$("#s_description").val('');

	$("#model").val('');

	$("#purchase_price").val('');



	$('select[name=s_department_code]').val('0');

	$('select[name=s_category_code]').val('0');

	$('select[name=subcategory_code]').val('0');

	$('select[name=s_brand_code]').val('0');

	$('select[name=s_size_code]').val('0');

	$('select[name=s_unit_code]').val('0');





	$('.selectpicker').selectpicker('refresh');



	$(".tbl_item_search_a").html('');



}



function getItemDet(code, serial_no, current_location, current_location_desc) {



	$.ajax({

		url: '/Customfunctions/setItemDet',

		dataType: 'json',

		type: 'post',

		data: {
			code: code,
			serial_no: serial_no
		},

		async: false,

		success: function (D) {



			$("#item_code").val(D.sum.code);

			$("#serial_no").val(serial_no);



			// $("#from_location").html("");

			// $("#from_location").html("<option value='"+current_location+"'>"+current_location_desc+"</option>");

			$('.selectpicker').selectpicker('refresh');



		}

	});





}





$(document).on("change", "#trans_code", function () {



	alert("trans_code chaged");



});





$("#code").on('keyup', function (e) {

	if (e.keyCode == 13) {
		$("#edit_id").val($(this).val());
		$("#is_edit").val("1");
		loadData($(this).val());

	}

});





function loadData(code) {

	$("#edit_id").val(code);
	$("#is_edit").val("1");

	showProgessBox('Please wait...');



	$.ajax({

		url: '/' + mstrurl + '/load_data',

		dataType: 'json',

		type: 'post',

		data: {
			edit_id: code
		},

		async: false,

		success: function (D) {



			showProgessBox();



			if (D.success == "1") {
				console.log(D.records.sum.length);

				if (D.records.sum.length > 0 && D.records.det.length > 0) {

					//for sum details bind 

					$("#from_location").val(D.records.sum[0].from_location);
					$("#s_item_code").val(D.records.det[0].description);
					$("#item_code").val(D.records.det[0].code);

					$("#to_location").val(D.records.sum[0].to_location);
					$("#qty").val(D.records.det[0].qty);
					$("#note").val(D.records.sum[0].memo);





					$('.selectpicker').selectpicker('refresh')

				} else {
					alert("Record Not Found");
				}


			} else {
				alert(D.Message);

			}






		}

	});



}

function validation() {
	if ($("#item_code").val() == "") {
		alert("Item Can not Be Empty");
		$("#s_item_code").focus();
		return false;

	} else if ($("#from_location").val() == "") {
		alert("From Location Can not Be Empty");
		$("#from_location").focus();
		return false;

	} else if ($("#to_location").val() == "") {
		alert("To Location not Be Empty");
		$("#to_location").focus();
		return false;

	} else if ($("#from_location").val() == $("#to_location").val()) {
		alert("From Location And To Location Can Not Be Same");
		$("#to_location").val("");
		return false;

	} else if ($("#qty").val() =="" || $("#qty").val() ==0)  {
		alert("Qty Can not Be Empty");
		$("#qty").focus();
		return false;
	} else {

		return true;
	}
}