
var mstrurl = 'stores';

$(document).ready(function(){

	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	load_data();
	
});

$(document).on("change","select[name=cl]",function(){
	set_bclistbycl($(this));
});

function set_bclistbycl(obj){

	$.ajax({
	    url: '/Customfunctions/bclistbycl',
	    dataType: 'json',
	    type: 'post',	    
	    data: { cl : jQuery.type( obj ) === "object" ? obj.val() : obj  }  ,	    
	    async: false,
	    success: function( D ){	    	           
	        
	    	var d = '<select class="selectpicker" data-live-search="true" id="bc" name="bc" required>';
	    		d += '<option value="">Select Branch</option>';
	    	for(var i = 0 ; i < D.length ; i++ ){
    			d += '<option value="'+D[i].bc+'">'+D[i].name+'</option>';
	    	}	d += '</select>';	    	

	    	$("#divbc").html(d);
	    	$('.selectpicker').selectpicker();

	    }
	});
}

function load_data(){

	showProgessBox('Loading data...');

	$.ajax({
	    url: '/'+mstrurl+'/show',
	    dataType: 'json',
	    type: 'post',	    
	    data: {  }  ,
	    success: function( D ){	    

	    	showProgessBox();	
            
	    	if (D.success == 1){

	    		var t =  '<table border="1" width="100%" class="tbl_master">';

					t += '<thead>'; 
					t += '<tr>'; 
					t += '<td>Code</td>'; 
					t += '<td>Cluster</td>'; 
					t += '<td>Branch</td>';  
					t += '<td>Description</td>'; 
					t += '<td>Purchase</td>'; 
					t += '<td>Transfer Location</td>'; 
					t += '<td>Date Time</td>'; 
					t += '<td>Action</td>'; 
					t += '</tr>'; 
					t += '</thead>';
	    		
	    		for (var i = 0 ; i < D.data.length; i++) {
	    			
	    			t += '<tr>'; 
	    			t += '<td>'+D.data[i].code+'</td>'; 
	    			t += '<td>'+D.data[i].desc_cl+'</td>'; 
	    			t += '<td>'+D.data[i].bc_name+'</td>'; 
	    			t += '<td>'+D.data[i].description+'</td>'; 
	    			t += '<td>'+D.data[i].purchase+'</td>'; 	    			
	    			t += '<td>'+D.data[i].transfer_location+'</td>'; 	    			
	    			t += '<td>'+D.data[i].action_date+'</td>'; 
	    			t += '<td><a class="action_link" code="'+D.data[i].code+'" action="edit">Edit</a> | <a class="action_link" code="'+D.data[i].code+'" action="delete">Delete</a></td>'; 
	    			t += '</tr>';
	    		}

	    		t += '</table>';

	    		$("#list_view").html(t);

	    	}else{
            	alert("Error")
	    	}            
	        
	    }
	});

}

$(document).on("click",".action_link",function(){
	
	if ($(this).attr("action") == "edit"){
		set_edit( $(this) );
	}

	if ($(this).attr("action") == "delete"){		

		obj = $(this);

		BootstrapDialog.confirm({
	        title: 'Confirm',
	        message: 'Do you want delete this record?',	        
	        callback: function(result) {
	            if(result) { make_delete(obj); }
	        }
	    });

	}
})

function make_delete(OBJ){

	showProgessBox('Deleting...');

	$.ajax({
	    url: '/'+mstrurl+'/destroy',
	    dataType: 'json',
	    type: 'post',	    
	    data: {code : OBJ.attr("code") },
	    success: function( D ){	    	
            
	    	showProgessBox();

	    	if (D.success == 1){
	    		
	    		reset_form(D.next_id);
	    		load_data();

	    	}else{
            	
	    	}            
	        
	    }
	});

}

function set_edit(OBJ){

	showProgessBox('Loading data...');

	$.ajax({
	    url: '/'+mstrurl+'/edit',
	    dataType: 'json',
	    type: 'post',	    
	    data: {code : OBJ.attr("code")},
	    success: function( D ){	    

	    	showProgessBox();	
            
	    	if (D.success == 1){
	    		
	    		$("#is_edit").val(1);
	    		$(".btnx").text("Update");

	    		$("#code").val( D.data[0].code );
	    		
	    		$('select[name=cl]').val(D.data[0].cl);
	    		$('.selectpicker').selectpicker('refresh');
				$.when( set_bclistbycl(D.data[0].cl) ).then( $("#bc").val(D.data[0].bc),$('.selectpicker').selectpicker('refresh'));

	    		D.data[0].purchase == 1 ? $("#purchase").prop("checked",true) : $("#purchase").prop("checked",false);
	    		D.data[0].transfer_location == 1 ? $("#transfer_location").prop("checked",true) : $("#transfer_location").prop("checked",false);

				$("#description").val(D.data[0].description);				

	    	}else{
            	
	    	}            
	        
	    }
	});

}


$("#f").on("submit",function(e){
	
	e.preventDefault();	
	showProgessBox('Saving...');

	$.ajax({
	    url: $(this).attr('action'),
	    dataType: 'json',
	    type: $(this).attr('method'),	    
	    data: $(this).serializeArray() ,
	    success: function( D ){	    	

	    	showProgessBox();
            
	    	if (D.success == 1){	    		
	    		load_data();
	    		reset_form(D.next_id);
	    		$("#is_edit").val(0);
	    		$(".btnx").text("Save");
	    		$("#code").val(D.next_id);
	    	}else{
            	set_error_msgs(D.errors);
	    	}            
	        
	    }
	});

});


function reset_form(next_id){
	$("#f").find('input').val('').prop("checked",false);
	$("#code").val(next_id);
	$('select[name=cl]').val('0');
	$('select[name=bc]').val('0');
	$('.selectpicker').selectpicker('refresh');
}
