
var mstrurl = 'fa_GRN';

$(document).ready(function(){

	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});


	$('select[name=trans_code]').val(2);
	$('select[name=pur_type]').val(2);
	$('.selectpicker').selectpicker('refresh');
	
});

$(document).on("click","#s_item_code",function(e){
	e.preventDefault();	
	$("#ItemSearchPopup").modal();
});

$(document).on("change","select[name=s_category_code]",function(){
	set_sub_category_list_by_category($(this));
});

function make_delete(OBJ){

	showProgessBox('Deleting...');

	$.ajax({
	    url: '/'+mstrurl+'/destroy',
	    dataType: 'json',
	    type: 'post',	    
	    data: {code : OBJ.attr("code") },
	    success: function( D ){	    	
            
	    	showProgessBox();

	    	if (D.success == 1){
	    		
	    		reset_form(D.next_id);	    		

	    	}else{
            	
	    	}            
	        
	    }
	});

}

function set_edit(OBJ){

	showProgessBox('Loading data...');

	$.ajax({
	    url: '/'+mstrurl+'/edit',
	    dataType: 'json',
	    type: 'post',	    
	    data: {code : OBJ.attr("code")},
	    success: function( D ){	    

	    	showProgessBox();	
            
	    	if (D.success == 1){
	    		
	    		$("#is_edit").val(1);
	    		$(".btnx").text("Update");

	    		$("#code").val( D.data[0].code );
	    		
	    		$('select[name=cl]').val(D.data[0].cl);
	    		$('.selectpicker').selectpicker('refresh');
				$.when( set_bclistbycl(D.data[0].cl) ).then( $("#bc").val(D.data[0].bc),$('.selectpicker').selectpicker('refresh'));

	    		D.data[0].purchase == 1 ? $("#purchase").prop("checked",true) : $("#purchase").prop("checked",false);
	    		D.data[0].transfer_location == 1 ? $("#transfer_location").prop("checked",true) : $("#transfer_location").prop("checked",false);

				$("#description").val(D.data[0].description);				

	    	}else{
            	
	    	}            
	        
	    }
	});

}


$("#f").on("submit",function(e){


	var net_amount = isNaN( $("#net_amount").val() ) ? 0 :  $("#net_amount").val() ;

	if (net_amount <= 0 || net_amount == ''){
		alert("Please select an items");
		return false;; 
	}
	
	e.preventDefault();

	if ($("#is_edit").val() == 0) { showProgessBox('Saving...'); }else{showProgessBox('Updating...');}

	$(".btnSave").attr("disabled",true);	

	$.ajax({
	    url: $(this).attr('action'),
	    dataType: 'json',
	    type: $(this).attr('method'),	    
	    data: $(this).serializeArray() ,
	    success: function( D ){	    	

	    	showProgessBox();
            
	    	if (D.success == 1){
	    		
	    		//reset_form(D.next_id);

	    		alert("Saved success");

	    		location.href = '';
	    		
	    		$("#is_edit").val(0);
	    		$(".btnx").text("Save");
	    		$("#code").val(D.next_id);

	    	}else if(D.success == 2){

	    		alert(D.m);
	    		
	    	}else{

	    		if (D.errors.item_code != undefined){
		    		if (D.errors.item_code[0] == 'required'){
		    			alert("Selecting an item is required");
		    			$("#s_item_code").focus();
		    			return;
		    		}
		    	}

            	set_error_msgs(D);
	    	} 

	    	$(".btnSave").attr("disabled",false);	           
	        
	    }
	});

});


function reset_form(next_id){

	//alert('Reset form now');

	var d = $("#trans_date").val();

	$("#f").find('input').val('').prop("checked",false);
	$("#is_edit").val(0);
	$("#code").val(next_id);
	$("#item_code").val("");
	$("#trans_date").val(d);
	$(".btnSave").attr("disabled",false);
	$("#serial_no").attr("readonly",false);
	$('select[name=trans_code]').val('0');
	$('select[name=location_code]').val('0');
	$('select[name=supplier_code]').val('0');	
	$('select[name=pur_type]').val('');	
	$('select[name=pur_supplier]').val('');	
	$('.selectpicker').selectpicker('refresh');
}

$(document).on("click",".btnSearch",function(e){
	var d = $("#fs");

	e.preventDefault();	
	$(".btnSearch").html('Searching...').addClass('disabled');

	$.ajax({
	    url: d.attr('action'),
	    dataType: 'json',
	    type: d.attr('method'),	    
	    data: d.serializeArray() ,
	    success: function( D ){	    	

	    	$(".btnSearch").html('Search').removeClass('disabled');

	        var T = '<table border="0" width="100%" class="tbl_item_search_a">';

	        	T += '<tr>';
				T += '<td>code</td>';
				T += '<td>Serial No</td>';
				T += '<td>Description</td>';
				T += '<td>Current Location</td>';
				T += '<td>Department</td>';
				T += '<td>Category</td>';
				T += '<td>Sub Category</td>';
				T += '<td>Brand</td>';
				T += '<td>Model</td>';
				T += '<td>Size</td>';
				T += '<td>Unit</td>';
				T += '<td>Purchase Price</td>';
				T += '</tr>';

	        for(n = 0 ; n < D.count ; n++ ){

				T += '<tr class="s_item_search_row" code="'+D.data[n].code+'" description="'+D.data[n].description+'" model="'+D.data[n].model+'" serial_no="'+D.data[n].serial_no+'" current_location="'+D.data[n].current_location+'"   current_location_desc="'+D.data[n].current_location_desc+'"   >';
				T += '<td>' + D.data[n].code + '</td>';
				T += '<td>' + D.data[n].serial_no + '</td>';
				T += '<td>' + D.data[n].description + '</td>';
				T += '<td>' + D.data[n].current_location_desc + '</td>';
				T += '<td>' + D.data[n].department_code + '</td>';
				T += '<td>' + D.data[n].category_code + '</td>';
				T += '<td>' + D.data[n].subcategory_code + '</td>';
				T += '<td>' + D.data[n].brand_code + '</td>';
				T += '<td>' + D.data[n].model + '</td>';
				T += '<td>' + D.data[n].size_code + '</td>';
				T += '<td>' + D.data[n].unit_code + '</td>';
				T += '<td>' + D.data[n].purchase_price + '</td>';
				T += '</tr>';

	        }

	        T += '</table>';

	    	$(".div-search-result").html(T);


	    }
	});
});

function set_sub_category_list_by_category(obj){

	$.ajax({
	    url: '/Customfunctions/subcategorylistbycategory',
	    dataType: 'json',
	    type: 'post',	    
	    data: { category_code : jQuery.type( obj ) === "object" ? obj.val() : obj  }  ,	    
	    async: false,
	    success: function( D ){
	        
	    	var d = '<select class="selectpicker" data-live-search="true" id="subcategory_code" name="subcategory_code" required>';
	    		d += '<option value="">Select Sub Category</option>';
	    	for(var i = 0 ; i < D.length ; i++ ){
    			d += '<option value="'+D[i].code+'">'+D[i].description+'</option>';
	    	}	d += '</select>';

	    	$("#divsubcategory").html(d);
	    	$('.selectpicker').selectpicker();

	    }
	});
}




$(document).on("click",".s_item_search_row",function(){

	$("#s_item_code").val($(this).attr("description") + " - " + $(this).attr("model"));
	$("#item_code").val($(this).attr("code"));
	getItemDet($(this).attr("code") , $(this).attr("serial_no") , $(this).attr("current_location") , $(this).attr("current_location_desc") );
	$('#ItemSearchPopup').modal('hide');

	clearSearchInput();

});


function clearSearchInput(){

	$("#s_code").val('');
	$("#s_description").val('');
	$("#model").val('');
	$("#purchase_price").val('');
	
	$('select[name=s_department_code]').val('0');	
	$('select[name=s_category_code]').val('0');	
	$('select[name=subcategory_code]').val('0');	
	$('select[name=s_brand_code]').val('0');	
	$('select[name=s_size_code]').val('0');	
	$('select[name=s_unit_code]').val('0');	
	
	$('.selectpicker').selectpicker('refresh');

	$(".tbl_item_search_a").html('');

}

function getItemDet(code,serial_no,current_location,current_location_desc){

	$.ajax({
	    url: '/Customfunctions/setItemDet',
	    dataType: 'json',
	    type: 'post',	    
	    data: { code : code , serial_no : serial_no  }  ,	    
	    async: false,
	    success: function( D ){
	        
	    	$("#item_code").val(D.sum.code);
	    	$("#serial_no").val(serial_no);
	    	
			$("#from_location").html("");
			$("#from_location").html("<option value='"+current_location+"'>"+current_location_desc+"</option>");
	    	$('.selectpicker').selectpicker('refresh');	    	

	    }
	});


}


$(document).on("change","#trans_code",function(){
	
	alert("trans_code chaged");

});


$("#code").on('keyup', function (e) {
    if (e.keyCode == 13) {        
    	loadFAGRN($(this).val());
    }
});


function loadFAGRN(code){

	showProgessBox('Please wait...');

	$.ajax({
	    url: '/Customfunctions/loadFAGRN',
	    dataType: 'json',
	    type: 'post',	    
	    data: { code : code }  ,	    
	    async: false,
	    success: function( D ){    	

	    	showProgessBox();

	    	if (D.s == 1){


	    		if (D.sum.is_cancel == 1){
	    			alert("This GRN was cancelled");
	    			$(".btnSave").prop("disabled",true);
	    		}else{
	    			$(".btnSave").prop("disabled",false);
	    		}



	    		$("#is_edit").val( code );

	    		$("#store_code").val(D.sum.store_code);
	    		$("#supplier_code").val(D.sum.supplier_code);
	    		$("#ref_no").val(D.sum.ref_no);
	    		$("#date").val(D.sum.date);

	    		var t = '';

	    		for(n = 0 ; n < D.det.length ; n++){

	    			var item_code = 			D.det[n].item_code;
					var item_desc = 			D.det[n].item_desc + " - " + D.det[n].model;
					var batch_no = 			D.det[n].batch_no;
					var price = 			D.det[n].price;
					var qty = 			D.det[n].qty;
					var net_value = 			D.det[n].net_value;


					t += '<tr> <td colspan="2" class="" style="padding-top: 10px"> <div class="form-group"> <input type="text" class="form-control" readonly="readonly" style="background-color:#ffffff" value="'+item_desc+'"> <input type="hidden" name="item_code[]" value="'+item_code+'"> </div> </td> <td class="" style="padding-top: 10px"> <div class="form-group"> <input type="text" class="form-control" name="batch_no[]" readonly="readonly" value="'+batch_no+'" style="text-align:center"> </div> </td> <td class="" style="padding-top: 10px"> <div class="form-group"> <input type="text" class="form-control" name="price[]" value="'+price+'" readonly="readonly" style="text-align:right"> </div> </td> <td class="" style="padding-top: 10px"> <div class="form-group"> <input type="text" class="form-control" name="qty[]" value="'+qty+'" readonly="readonly" style="text-align:center"> </div> </td> <td class="" style="padding-top: 10px"> <div class="form-group"> <input type="text" class="form-control net_row" name="net_value[]" value="'+net_value+'" readonly="readonly" style="text-align:right"> </div> </td> <td class="" style="padding-top: 10px" align="Center"> <div class="form-group"> <a class="remove">remove</a> </div> </td> </tr>';

	    		}

				$(".tblMaster tbody").append(t);


	    		$("#description").val(D.sum.description);
	    		$("#total_value").val(D.sum.total_value);
	    		$("#emp_code").val(D.sum.emp_code);
	    		$("#cost_center").val(D.sum.cost_center);
	    		$("#tax").val(D.sum.tax);
	    		$("#net_amount").val(D.sum.net_amount);
				
				$('.selectpicker').selectpicker('refresh');

				

	    	}
	    	

	    }
	});

}



$(document).on("click",".btnAdd",function(){

	var item_code = $("#item_code").val();
	var item_desc = $("#s_item_code").val();
	var batch_no = $("#batch_no").val();
	var price = $("#price").val();
	var qty = $("#qty").val();
	var net_value = $("#net_value").val();

	if (item_code == ""){
		alert("Please select an item");
		$("#item_code").focus();
		return;
	}

	if (batch_no == ""){
		alert("Please enter batch number");
		$("#batch_no").focus();
		return;
	}

	if (price == ""){
		alert("Please enter item price");
		$("#price").focus();
		return;
	}

	if (qty == ""){
		alert("Please enter item quantity");
		$("#qty").focus();
		return;
	}

	if (net_value <= 0){
		alert("Invalid net value");
		return;
	}


	var t = '<tr> <td colspan="2" class="" style="padding-top: 10px"> <div class="form-group"> <input type="text" class="form-control" readonly="readonly" style="background-color:#ffffff" value="'+item_desc+'"> <input type="hidden" name="item_code[]" value="'+item_code+'"> </div> </td> <td class="" style="padding-top: 10px"> <div class="form-group"> <input type="text" class="form-control" name="batch_no[]" readonly="readonly" value="'+batch_no+'" style="text-align:center"> </div> </td> <td class="" style="padding-top: 10px"> <div class="form-group"> <input type="text" class="form-control" name="price[]" value="'+price+'" readonly="readonly" style="text-align:right"> </div> </td> <td class="" style="padding-top: 10px"> <div class="form-group"> <input type="text" class="form-control" name="qty[]" value="'+qty+'" readonly="readonly" style="text-align:center"> </div> </td> <td class="" style="padding-top: 10px"> <div class="form-group"> <input type="text" class="form-control net_row" name="net_value[]" value="'+net_value+'" readonly="readonly" style="text-align:right"> </div> </td> <td class="" style="padding-top: 10px" align="Center"> <div class="form-group"> <a class="remove">remove</a> </div> </td> </tr>';


	$(".tblMaster tbody").append(t);

	set_net_amount();

	reset_item_inputs();

});

function reset_item_inputs(){

	$("#s_item_code").val("");
	$("#item_code").val("");
	$("#batch_no").val("");
	$("#price").val("");
	$("#qty").val("");
	$("#net_value").val("");

}

function set_net_amount(){

	var net_amount = 0;

	$(".net_row").each(function(){		
		net_amount += parseFloat( $(this).val() );
	});

	$("#total_value").val(net_amount);

	var tax =  isNaN( $("#tax").val() ) ? 0 :  $("#tax").val()  ;

	$("#net_amount").val( net_amount - tax  );

}

$(document).on("click",".remove",function(){
	if (confirm("Do you want remove this item?")){
		$(this).parent().parent().parent().remove();
		set_net_amount();
	}
});


$(document).on("keyup",".item_add_top_row",function(){
	cal_net_val();
});

$(document).on("blur",".item_add_top_row",function(){
	cal_net_val();
});

$(document).on("keyup","#tax",function(){
	set_net_amount();
});


function cal_net_val(){

	var price =  isNaN( $("#price").val() ) ? 0 :  $("#price").val() ;
	var qty = isNaN( $("#qty").val() ) ? 0 :  $("#qty").val() ;
	net_value = price * qty;
	
	$("#net_value").val(net_value);	

}


$(document).on("click",".cancel_grn",function(){

	if ($("#is_edit").val() == 0){
		alert("Please load GRN first");
	}else{

		if (confirm("Do you want cancel this GRN?")){

			$.ajax({
			    url: '/Customfunctions/cancelGRN',
			    dataType: 'json',
			    type: 'post',	    
			    data: { code : $("#is_edit").val() }  ,	    
			    async: false,
			    success: function( D ){
			        
			    	

			    }
			});

		}

	}



});