
$(document).ready(function(){

	$(".form-control[type='text']").keyup(function(){
		$(this).css("border","1px solid #ccc");
		$(this).next().remove();
	});

	$(".form-control[type='number']").change(function(){
		$(this).css("border","1px solid #ccc");
		$(this).next().remove();
	});

	$("select").change(function(){
		$(this).css("border","1px solid #ccc");
		$(this).next().remove();
	});

});



function set_error_msgs(OBJ,err = ''){

	if (OBJ.errors_a === undefined ){	
		$.each(OBJ.errors, function (key, val) {                
		    $( "#"+key ).css("border","1px solid red");
		    $( "."+key ).remove();
		    $( "#"+key ).after( "<p class='"+key+" error_text_after'>"+val+"</p>" );
		    $( "#"+key ).focus();
		});	
	}else{		
		showInfoBox("Database Error:"+OBJ.errors[1],OBJ.errors[2],'c');
	}

}

function showInfoBox(title,m,info_type='i'){

	if (info_type == 'c'){
		$("#infoModal").find(".modal-header").css({'background-color':'red','color':'#fff'});
	}else if (info_type == 'w') {
		$("#infoModal").find(".modal-header").css({'background-color':'yellow','color':'#000'});
	}else{
		$("#infoModal").find(".modal-header").css({'background-color':'#2a88bd','color':'#fff'});
	}

	$("#infoModal").find(".modal-title").html(title);
	$("#infoModal").find(".modal-body").html( m );
	$('#infoModal').modal();	

}

function showProgessBox(t=''){
	if (t == ''){

		$(".modal-backdrop").remove();
		$("#ModalProgress,#infoModal").modal('hide');	
	}else{
		$("#ModalProgress").find(".modal-body").html(t);
		$("#ModalProgress").modal();
	}
}