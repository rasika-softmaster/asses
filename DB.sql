/*
SQLyog Ultimate v9.63 
MySQL - 5.5.5-10.1.25-MariaDB : Database - lav
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `m_branch` */

DROP TABLE IF EXISTS `m_branch`;

CREATE TABLE `m_branch` (
  `cl` int(11) NOT NULL,
  `bc` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(100) DEFAULT '',
  `tp` varchar(20) DEFAULT '',
  `fax` varchar(20) DEFAULT '',
  `email` varchar(50) DEFAULT '',
  `oc` varchar(20) NOT NULL,
  `action_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`cl`,`bc`),
  UNIQUE KEY `bc` (`bc`),
  CONSTRAINT `m_branch_ibfk_1` FOREIGN KEY (`cl`) REFERENCES `m_cluster` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_branch` */

insert  into `m_branch`(`cl`,`bc`,`name`,`address`,`tp`,`fax`,`email`,`oc`,`action_date`,`is_active`) values (2,1,'sas','','','','fghdf@asdas.lk','1','2018-01-24 10:25:01',1),(2,2,'ABC','','','','','1','2018-01-23 15:04:21',1),(2,3,'retw ertewrt','34534534','','','','1','2018-01-24 10:21:38',1),(2,4,'ewrweqrwq','','','','','1','2018-01-24 10:25:32',1),(2,8,'df gh','','','','','1','2018-01-27 10:57:54',1),(4,6,'fghdfghdfg','','','','','1','2018-01-24 10:26:59',1),(6,5,'hfghdfgh','','','','','1','2018-01-24 10:26:53',1),(6,7,'hfgh','','','','','1','2018-01-27 10:57:47',1);

/*Table structure for table `m_brand` */

DROP TABLE IF EXISTS `m_brand`;

CREATE TABLE `m_brand` (
  `code` varchar(10) NOT NULL,
  `description` varchar(50) NOT NULL,
  `oc` varchar(20) NOT NULL,
  `action_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `max_no` int(11) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_brand` */

/*Table structure for table `m_category` */

DROP TABLE IF EXISTS `m_category`;

CREATE TABLE `m_category` (
  `code` varchar(10) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `de_code` varchar(10) DEFAULT NULL,
  `oc` varchar(20) DEFAULT NULL,
  `action_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`code`),
  KEY `r_department_r_category` (`de_code`),
  CONSTRAINT `r_department_r_category` FOREIGN KEY (`de_code`) REFERENCES `m_department` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_category` */

insert  into `m_category`(`code`,`description`,`de_code`,`oc`,`action_date`) values ('ELTV','Television','EL','1','2018-01-05 15:28:20');

/*Table structure for table `m_cluster` */

DROP TABLE IF EXISTS `m_cluster`;

CREATE TABLE `m_cluster` (
  `code` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `oc` varchar(20) NOT NULL,
  `action_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_cluster` */

insert  into `m_cluster`(`code`,`description`,`oc`,`action_date`) values (2,'Colombo GBP XXXXXX','1','2018-01-26 15:30:37'),(3,'Kandy','1','2018-01-23 13:27:57'),(4,'Galle','1','2018-01-23 13:28:09'),(5,'Peraadeniya','1','2018-01-23 13:28:23'),(6,'Panadura','1','2018-01-23 13:28:30'),(7,'5345','1','2018-01-24 09:47:00'),(8,'5345','1','2018-01-24 09:47:02'),(9,'5345','1','2018-01-24 09:47:03'),(10,'345','1','2018-01-24 09:47:04'),(11,'5345','1','2018-01-24 09:47:04'),(12,'4534','1','2018-01-24 09:47:05'),(13,'345','1','2018-01-24 09:47:06'),(14,'5345','1','2018-01-24 09:47:06'),(15,'4534','1','2018-01-24 09:47:07'),(16,'3453','1','2018-01-24 09:47:08'),(17,'5654','1','2018-01-24 10:13:31'),(19,'jkjmgjkg','1','2018-01-26 15:55:05');

/*Table structure for table `m_color` */

DROP TABLE IF EXISTS `m_color`;

CREATE TABLE `m_color` (
  `code` varchar(4) NOT NULL,
  `description` varchar(50) NOT NULL,
  `oc` varchar(20) NOT NULL,
  `code_gen` varchar(3) NOT NULL,
  `action_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `max_no` int(11) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_color` */

/*Table structure for table `m_company` */

DROP TABLE IF EXISTS `m_company`;

CREATE TABLE `m_company` (
  `name` varchar(200) NOT NULL,
  `address01` varchar(100) NOT NULL DEFAULT '',
  `address02` varchar(100) NOT NULL DEFAULT '',
  `address03` varchar(100) NOT NULL DEFAULT '',
  `phone01` varchar(12) NOT NULL DEFAULT '',
  `phone02` varchar(12) NOT NULL DEFAULT '',
  `phone03` varchar(12) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_company` */

/*Table structure for table `m_department` */

DROP TABLE IF EXISTS `m_department`;

CREATE TABLE `m_department` (
  `code` varchar(10) NOT NULL,
  `description` varchar(50) DEFAULT '',
  `oc` varchar(20) DEFAULT NULL,
  `action_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_department` */

insert  into `m_department`(`code`,`description`,`oc`,`action_date`) values ('EL','Electronic','1','2018-01-05 15:27:38');

/*Table structure for table `m_item` */

DROP TABLE IF EXISTS `m_item`;

CREATE TABLE `m_item` (
  `code` varchar(15) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `dep_code` varchar(10) DEFAULT NULL,
  `cat_code` varchar(10) DEFAULT NULL,
  `sub_cat_code` varchar(10) DEFAULT NULL,
  `brand` varchar(10) DEFAULT NULL,
  `model` varchar(10) DEFAULT NULL,
  `size` varchar(10) DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `purchase_price` decimal(12,2) DEFAULT '0.00',
  `inactive` tinyint(1) DEFAULT NULL,
  `is_fa` tinyint(1) DEFAULT '0',
  `dip_period` int(2) DEFAULT NULL,
  `dip_per` decimal(5,2) DEFAULT NULL,
  `is_batch` int(1) DEFAULT '0',
  `fa_acc` varchar(20) DEFAULT NULL,
  `dip_acc` varchar(20) DEFAULT NULL,
  `ca_period` int(2) DEFAULT '0',
  `ca_per` decimal(5,2) DEFAULT '0.00',
  `oc` varchar(20) DEFAULT NULL,
  `action_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`code`),
  KEY `item_dep` (`dep_code`),
  KEY `item_cat` (`cat_code`),
  KEY `item_subCat` (`sub_cat_code`),
  CONSTRAINT `item_cat` FOREIGN KEY (`cat_code`) REFERENCES `m_category` (`code`),
  CONSTRAINT `item_dep` FOREIGN KEY (`dep_code`) REFERENCES `m_department` (`code`),
  CONSTRAINT `item_subCat` FOREIGN KEY (`sub_cat_code`) REFERENCES `m_sub_category` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_item` */

/*Table structure for table `m_stores` */

DROP TABLE IF EXISTS `m_stores`;

CREATE TABLE `m_stores` (
  `cl` int(11) NOT NULL,
  `bc` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `description` varchar(100) NOT NULL DEFAULT '',
  `purchase` tinyint(1) NOT NULL DEFAULT '0',
  `oc` varchar(20) NOT NULL,
  `action_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `transfer_location` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cl`,`bc`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_stores` */

insert  into `m_stores`(`cl`,`bc`,`code`,`description`,`purchase`,`oc`,`action_date`,`transfer_location`) values (2,1,1,'Colombo ST1 ',1,'1','2018-01-24 10:16:03',0),(2,2,2,'423',1,'1','2018-01-27 10:58:39',1);

/*Table structure for table `m_sub_category` */

DROP TABLE IF EXISTS `m_sub_category`;

CREATE TABLE `m_sub_category` (
  `code` varchar(10) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `category` varchar(10) NOT NULL,
  `oc` varchar(20) DEFAULT NULL,
  `action_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`code`),
  KEY `r_sub_category_r_category` (`category`),
  CONSTRAINT `r_sub_category_r_category` FOREIGN KEY (`category`) REFERENCES `m_category` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_sub_category` */

/*Table structure for table `m_unit` */

DROP TABLE IF EXISTS `m_unit`;

CREATE TABLE `m_unit` (
  `code` varchar(4) NOT NULL,
  `description` varchar(50) NOT NULL,
  `oc` varchar(20) NOT NULL,
  `code_gen` varchar(2) NOT NULL,
  `action_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `max_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_unit` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`) values (1,'supun','123@abc.lk','$2y$10$C9aAcRpEk2NXubxm7gnYHuyN8tKHIJVwADoEX3SAooemH0CkPx7r6','NO8w6P3FCiqqcwe1blEhShLmyRL5eZs0JyqB1IxHFfCY2kvamCpHcpr7EtB1','2018-01-17 08:47:09','2018-01-17 08:47:09');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
