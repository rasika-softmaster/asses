@extends('main')

@section('title',' | FA Transfer')
@section('page_content')
<p class="page_heading_large">FA Transfer</p>

<link href="{{ asset('css/jquery-ui.css') }}" rel="stylesheet">

<form method="post" action="/<?=$pagename;?>/store" id="f" novalidate="" onkeydown="return !(event.keyCode==13)">
    <table border="0" width="900" class="tblMaster" align="Center">
        <tr>            
            <td>

                <table width="100%">
                    <tr>
                        <td>
                            <div class="form-group">
                                <label for="s_item_code">Item</label>
                                <input type="text" class="form-control" id="s_item_code" autocomplete="off" readonly="readonly" style="background-color:#ffffff"> 
                                <input type="hidden" name="item_code" id="item_code" value="">                               
                                <input type="hidden" name="is_edit" id="is_edit" value="0">
                            </div>
                        </td>
                        <td width="75">
                            <div class="form-group">
                                <label for="code">Trans No</label><br>
                                <input type="text" class="form-control" id="code" name="code" required autocomplete="off" value="<?=$code?>">
                            </div>
                        </td>
                        <td width="100">
                            <div class="form-group">
                                <label for="date">Date</label><br>
                                <input type="text" class="form-control" id="date" name="date" autocomplete="off" value="<?=date('Y-m-d');?>" readonly="readonly">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
            <td></td>
        </tr>

        <tr>            
            <td>
                <div class="form-group">
                    <label for="serial_no">Serial No </label>
                    <input type="text" class="form-control" id="serial_no" name="serial_no" required autocomplete="off" readonly="readonly">
                </div>
            </td>
            <td></td>
        </tr>

        <tr>            
            <td>
                <div class="form-group">
                    <label for="description">Current Location</label><br><?php echo $lc; ?>
                </div>
            </td>
            <td></td>
        </tr>

        <tr>            
            <td>
                <div class="form-group">
                    <label for="description">New Location</label><br><?php echo $lcn; ?>
                </div>
            </td>
            <td></td>
        </tr>

        <tr>            
            <td>
                <div class="form-group">
                    <label for="note">Note</label><br>
                    <textarea class="form-control" id="note" name="note"></textarea>
                </div>
            </td>
            <td></td>
        </tr>

    </table>
    
    <hr>
    
    <div class="fixed_button_div">
        
        <table border=0 align="Center" width="900">
            <tr>
                <td>
                    <button type="submit" class="btn btnSave btn-primary">Save</button>
                    &nbsp;&nbsp;<a href = ''>Reset</a>
                </td>
            </tr>  
        </table>      
    </div>

</form>

<!-- Item Search window - A Bootstrap Modal dialog-->
<div class="modal fade" id="ItemSearchPopup" role="dialog">
    <div class="modal-dialog modal-lg"  style="width:96%">

      <!-- Modal content-->
    <div class="modal-content">
        
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Item</h4>
        </div>

        <div class="modal-body">

            <form method="post" action="/<?=$pagename;?>/itemsearch" id="fs" novalidate>

                <table border="0" cellpadding="0" cellspacing="0" class="tbl_item_search" width="100%">

                    <tr>
                        <td valign="top">
                            <div class="form-group">
                                <label for="code">Item Code</label>
                                <input type="text" class="form-control" id="s_code" name="s_code" value="" autocomplete="off" >
                            </div>                            
                        </td>
                        <td colspan="3" valign="top">
                            <div class="form-group">
                                <label for="s_description">Description</label>
                                <input type="text" class="form-control" id="s_description" name="s_description" required autocomplete="off">
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="model">Model</label>
                                <input type="text" class="form-control" id="model" name="model" required autocomplete="off">
                            </div>
                        </td>              
                        <td valign="top">
                            <div class="form-group">
                                <label for="purchase_price">Purchase Price</label>
                                <input type="text" class="form-control" id="purchase_price" name="purchase_price" required autocomplete="off">
                            </div>
                        </td>
                    </tr>
                    
                    <tr>
                        <td valign="top">
                            <div class="form-group">
                                <label for="department_code">Department</label><br><?php echo $s_department; ?>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="category_code">Category</label><br><?php echo $s_category; ?>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="subcategory_code">Sub Category</label><br>                    
                                <div id="divsubcategory">
                                <select class="selectpicker" data-live-search="true" id="subcategory_code" name="subcategory_code" required>
                                  <option value="">Please select category first</option>
                                </select></div>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="brand_code">Brand</label><br><?php echo $s_brand; ?>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="size_code">Size</label><br><?php echo $s_size; ?>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="unit_code">Unit</label><br><?php echo $s_unit; ?>
                            </div>
                        </td>                        
                        
                    </tr>
                    
                </table>                

            </form>
        </div>

        <div class="modal-body">
            <div class="div-search-result"></div>
        </div>
        <div class="modal-footer">
            <div class="mm" style="float: left; padding-top: 10px;">                    
                <button type="button" class="btn btnSearch btn-primary">Search</button>
            </div>            
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>        
    </div>
      
    </div>
</div>



<!-- Scripts -->

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/bootstrap-dialog.min.js') }}"></script>
<script src="{{ asset('js/UI_DOM.js') }}"></script>
<script src="{{ asset('js/fa_transfer.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.js') }}"></script>
<script src="{{ asset('js/jquery-ui.js') }}"></script>


<script>
    
    $(document).ready(function(){
        $( "#trans_date,#pur_date,#dep_opb_date" ).datepicker({ 
            dateFormat: 'yy-mm-dd' 
        });
    });

</script>


@endsection