@extends('main')



@section('title',' | Item Damage')

@section('page_content')

<p class="page_heading_large">Item Damage</p>





<form method="post" action="" id="item_damage" novalidate="" onkeydown="return !(event.keyCode==13)">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="edit_id" id="edit_id">
    <input type="hidden" name="is_edit" id="is_edit">
    <table border="0" width="900" class="tblMaster" align="Center">

        <tr>

            <td>
                <div class="form-group">

                    <label for="description">Store</label><br>
                    <?php echo $store; ?>

                </div>

                <div class="form-group">

                    <label for="description">Description</label><br>
                    <textarea row="3" class="form-control" id="description" name="description" required
                        autocomplete="off"></textarea>

                </div>


            </td>

            <td style="width:100px"></td>

            <td style="width:100px"></td>

            <td style="width:100px">

                <div class="">

                    <label for="no">No</label>

                    <input type="text" class="form-control" id="no" name="no" required autocomplete="off" size="3"
                        value="<?=$next_id?>">

                </div>
                <div class="">

                    <label for="date">Date</label>

                    <input type="date" class="form-control" id="date" name="date" required autocomplete="off" size="3">

                </div>
             
            </td>

        </tr>

        <tr>



            <td colspan="4" style="padding: 0px">

                <table width="100%" border="0">

                    <thead>

                        {{--   <tr>

                            <td>

                                <div class="form-group">

                                    <label for="item_code">Item Code</label>

                                    <input type="text" class="form-control" id="item_code" required autocomplete="off" size="3">

                                </div>

                            </td>
                            <td style="width:75px;text-align: right">

                                <button type="button" class="btn btnAdd btn-primary">Add</button>

                            </td>

                        </tr> --}}

                    </thead>

                    <tbody>

                        {{--    <tr>

                            <td colspan="4"><div class="message_p">Item(s) not added</div></td>

                        </tr> --}}
                        <table>

                            <thead style="background-color: #e6e6e6;">
                                <tr>
                                    <th class="tb_head_th" style="width: 200px;">Code</th>
                                    <th class="tb_head_th" style="width: 350px;">Description</th>
                                    <th class="tb_head_th" style="width: 200px;">Model</th>
                                    <th class="tb_head_th" style="width: 100px;">Qty</th>
                                  
                                </tr>
                            </thead>
                            <tbody>

                                @for ($i = 0; $i < 25; $i++) <tr class="items">
                                    <td class="border_td " value="{{ $i }}">
                                        <input type='text' class='g_input_txt cr' readonly='readonly' id='code_{{ $i }}'
                                            name='code_{{ $i }}' />
                                    </td>
                                    <td class="border_td" value="{{ $i }}">
                                        <input type='text' class='g_input_txt' readonly='readonly' id='des_{{ $i }}'
                                            name='des_{{ $i }}' />
                                    </td>
                                    <td class="border_td" value="{{ $i }}">
                                        <input type='text' class='g_input_txt' readonly='readonly' id='model_{{ $i }}'
                                            name='model_{{ $i }}' />
                                    </td>
                                    <td class="border_td" value="">
                                        <input type="text" class='qty' name="qty_{{ $i }}" id="qty_{{ $i }}"></td>
                                  
        </tr>
        @endfor
        </tbody>


    </table>

    </tbody>

    <tfoot>
      
        <tr>



            <td colspan="4">

                <button type="button" class="btn btnSave btn-primary" style="float: right; " id="save">Save</button>

            </td>

        </tr>
    </tfoot>

    </table>

    </td>



    </tr>







    </table>
 

</form>





<!-- Item Search window - A Bootstrap Modal dialog-->
<div class="modal fade" id="ItemSearchPopup" role="dialog">

    <div class="modal-dialog modal-lg" style="width:50%">



        <!-- Modal content-->

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select Item</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/<?=$pagename;?>/itemsearch" id="fs" novalidate>
                    <table border="0" cellpadding="0" cellspacing="0" class="tbl_item_search" width="100%">
                        <tr>
                            <td valign="top">

                                <div class="form-group" style="width:200px;">

                                    <label for="code">Search</label>

                                    <input type="text" class="form-control" id="s_code" name="s_code" value=""
                                        autocomplete="off">

                                </div>

                            </td>
                        </tr>
                    </table>

                </form>

            </div>



            <div class="modal-body">

                <div class="div-search-result"></div>

            </div>

            <div class="modal-footer">

                <div class="mm" style="float: left; padding-top: 10px;">
                </div>

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>

        </div>



    </div>

</div>








<!-- Scripts -->



<script src="{{ asset('js/app.js') }}"></script>

<script src="{{ asset('js/bootstrap-dialog.min.js') }}"></script>

<script src="{{ asset('js/UI_DOM.js') }}"></script>

<script src="{{ asset('js/item_damage.js') }}"></script>

<script src="{{ asset('js/bootstrap-select.js') }}"></script>



@endsection