@extends('main')

@section('title',' | Stores')
@section('page_content')
<p class="page_heading_large">Stores</p>

<table border="0" width="100%">
    <tr>
        <td><span class="page_heading_medium">Create New Store</span><br><br></td>
        <td style="padding-left: 30px"><span class="page_heading_medium">Exist Store(s)</span><br><br></td>
    </tr>
    <tr>
        <td style="width: 300px" valign="top">
            <form method="post" action="/stores/store" id="f">                
                
                <div class="form-group">
                    <label for="code">Code</label>
                    <input type="text" class="form-control" id="code" name="code" readonly="readonly" value="{{ $code }}" autocomplete="off" >
                    <input type="hidden" name="is_edit" id="is_edit" value="0">
                </div>

                <div class="form-group">
                    <label for="description">Cluster</label><br><?php echo $cl; ?>
                </div>


                <div class="form-group">
                    <label for="description">Branch</label><br>                    
                    <div id="divbc">
                    <select class="selectpicker" data-live-search="true" id="bc" name="bc" required>
                      <option value="">Please select cluster first</option>
                    </select></div>
                </div>
                
                
                <div class="form-group">
                    <label for="description">Description:</label>
                    <input type="text" class="form-control" id="description" name="description" required autocomplete="off">
                </div>

                <div class="form-group">                    
                    <label><input type="checkbox" name="purchase" id="purchase"> Purchase</label>&nbsp;&nbsp;&nbsp;&nbsp;
                    <label><input type="checkbox" name="transfer_location" id="transfer_location"> Transfer Location</label>
                </div>       

                <button type="submit" class="btn btn-primary btnx">Save</button>
                <button type="reset" class="btn btn-link">Reset</button>

            </form>
        </td>
        <td align="right" valign="top" id='list_view' style="padding-left: 30px"></td>
    </tr>
</table>

<!-- Scripts -->

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/bootstrap-dialog.min.js') }}"></script>
<script src="{{ asset('js/UI_DOM.js') }}"></script>
<script src="{{ asset('js/stores.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.js') }}"></script>

@endsection