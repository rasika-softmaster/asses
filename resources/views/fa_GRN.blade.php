@extends('main')

@section('title',' | FA GRN')
@section('page_content')
<p class="page_heading_large">FA GRN</p>

<link href="{{ asset('css/jquery-ui.css') }}" rel="stylesheet">

<form method="post" action="/<?=$pagename;?>/store" id="f" novalidate="" onkeydown="return !(event.keyCode==13)">


    <table border="0" width="900" class="tblMaster" align="Center">

        <thead>
        
            <tr>
                <td>
                    <div class="form-group">
                        <label for="store_code">Store</label><br><?php echo $store; ?>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <label for="supplier_code">Supplier</label><br><?php echo $supplier; ?>
                    </div>
                </td>
                <td></td>
                <td></td>
                <td>
                    <div class="form-group">
                        <label for="ref_no">Ref No</label>
                        <input type="text" size="3" class="form-control" id="ref_no" name="ref_no" required autocomplete="off">
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <label for="code">No</label>
                        <input type="hidden" name="is_edit" id="is_edit" value="0">
                        <input type="text" size="3" class="form-control" id="code" name="code" required autocomplete="off" value="<?=$code?>">
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <label for="date">Date</label>
                        <input type="text" size="5" class="form-control" id="date" name="date" required autocomplete="off" value="<?=date('Y-m-d')?>" style="width:90px">
                    </div>
                </td>
            </tr>

        </thead>

        


        <tbody>

            <tr style="border:1px solid #cccccc; background-color: #eaeaea">
                <td colspan="2">
                    
                    <div class="form-group">
                        <label for="s_item_code">Item</label>
                        <input type="text" class="form-control" id="s_item_code" autocomplete="off" readonly="readonly" style="background-color:#ffffff"> 
                        <input type="hidden" id="item_code" value="">
                    </div>

                </td>
                <td align="Center">
                    <div class="form-group">
                        <label for="batch_no">Batch No</label>
                        <input type="text" class="form-control" id="batch_no" required autocomplete="off" style="text-align: center">
                    </div>
                </td>
                <td align="right">
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="text" class="form-control item_add_top_row" id="price" required autocomplete="off" style="text-align: right">
                    </div>
                </td>
                <td align="Center">
                    <div class="form-group">
                        <label for="qty">Qty</label>
                        <input type="text" class="form-control item_add_top_row" id="qty" required autocomplete="off" style="text-align: center">
                    </div>
                </td>
                <td align="right">
                    <div class="form-group">
                        <label for="net_value">Net Value</label>
                        <input type="text" class="form-control item_add_top_row" id="net_value" required autocomplete="off" readonly="readonly" style="text-align: right">
                    </div>
                </td>
                <td>
                    <div class="form-group" style="padding-top: 19px">                        
                        <button type="button" class="btn btnAdd btn-primary" style="width: 100%">Add</button>                    
                    </div>
                </td>

            </tr>


            

        </tbody>

        
        

        <tfoot>

            <tr>
                <td colspan="7"><hr></td>
            </tr>

            <tr>
                <td colspan="4">
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="text" class="form-control" id="description" name="description" required autocomplete="off">
                    </div>
                </td>                
                
                <td></td>
                <td>
                    <div class="form-group">
                        <label for="total_value">Total Value</label>
                        <input type="text" class="form-control" id="total_value" name="total_value" required autocomplete="off" readonly="readonly">
                    </div>
                </td>
                <td></td>
            </tr>

            <tr>
                <td colspan="2">
                    <div class="form-group">
                        <label for="emp_code">Emp</label>
                        <input type="text" class="form-control" id="emp_code" name="emp_code" required autocomplete="off">
                    </div>
                </td>                
                <td colspan="2">
                    <div class="form-group">
                        <label for="cost_center">Cost Center</label>
                        <input type="text" class="form-control" id="cost_center" name="cost_center" required autocomplete="off">
                    </div>
                </td>                
                <td></td>
                <td>
                    <div class="form-group">
                        <label for="tax">Tax</label>
                        <input type="text" class="form-control" id="tax" name="tax" required autocomplete="off" value="0">
                    </div>
                </td>
                <td></td>
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <div class="form-group">
                        <label for="net_amount">Net Amount</label>
                        <input type="text" class="form-control" id="net_amount" name="net_amount" required autocomplete="off" readonly="readonly">
                    </div>
                </td>
                <td></td>
            </tr>

        </tfoot>




    </table>    
    
    
    <div class="fixed_button_div">
        
        <table border=0 align="Center" width="900">
            <tr>
                <td>
                    <button type="submit" class="btn btnSave btn-primary">Save</button>
                    &nbsp;&nbsp;<a href = ''>Reset</a>
                </td>
                <td align="right">&nbsp;&nbsp;<a href = '#' style="color:#999999" class="cancel_grn">Cancel this GRN</a></td>
            </tr>  
        </table>

    </div>

</form>

<!-- Item Search window - A Bootstrap Modal dialog-->
<div class="modal fade" id="ItemSearchPopup" role="dialog">
    <div class="modal-dialog modal-lg"  style="width:96%">

      <!-- Modal content-->
    <div class="modal-content">
        
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Item</h4>
        </div>

        <div class="modal-body">

            <form method="post" action="/<?=$pagename;?>/itemsearch" id="fs" novalidate>

                <table border="0" cellpadding="0" cellspacing="0" class="tbl_item_search" width="100%">

                    <tr>
                        <td valign="top">
                            <div class="form-group">
                                <label for="code">Item Code</label>
                                <input type="text" class="form-control" id="s_code" name="s_code" value="" autocomplete="off" >
                            </div>                            
                        </td>
                        <td colspan="3" valign="top">
                            <div class="form-group">
                                <label for="s_description">Description</label>
                                <input type="text" class="form-control" id="s_description" name="s_description" required autocomplete="off">
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="model">Model</label>
                                <input type="text" class="form-control" id="model" name="model" required autocomplete="off">
                            </div>
                        </td>              
                        <td valign="top">
                            <div class="form-group">
                                <label for="purchase_price">Purchase Price</label>
                                <input type="text" class="form-control" id="purchase_price" name="purchase_price" required autocomplete="off">
                            </div>
                        </td>
                    </tr>
                    
                    <tr>
                        <td valign="top">
                            <div class="form-group">
                                <label for="department_code">Department</label><br><?php echo $s_department; ?>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="category_code">Category</label><br><?php echo $s_category; ?>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="subcategory_code">Sub Category</label><br>                    
                                <div id="divsubcategory">
                                <select class="selectpicker" data-live-search="true" id="subcategory_code" name="subcategory_code" required>
                                  <option value="">Please select category first</option>
                                </select></div>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="brand_code">Brand</label><br><?php echo $s_brand; ?>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="size_code">Size</label><br><?php echo $s_size; ?>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="unit_code">Unit</label><br><?php echo $s_unit; ?>
                            </div>
                        </td>                        
                        
                    </tr>
                    
                </table>                

            </form>
        </div>

        <div class="modal-body">
            <div class="div-search-result"></div>
        </div>
        <div class="modal-footer">
            <div class="mm" style="float: left; padding-top: 10px;">                    
                <button type="button" class="btn btnSearch btn-primary">Search</button>
            </div>            
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>        
    </div>
      
    </div>
</div>



<!-- Scripts -->

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/bootstrap-dialog.min.js') }}"></script>
<script src="{{ asset('js/UI_DOM.js') }}"></script>
<script src="{{ asset('js/fa_GRN.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.js') }}"></script>
<script src="{{ asset('js/jquery-ui.js') }}"></script>


<script>
    
    $(document).ready(function(){
        $( "#date" ).datepicker({ 
            dateFormat: 'yy-mm-dd' 
        });
    });

</script>


@endsection