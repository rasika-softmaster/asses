@extends('main')

@section('title',' | Purchase Requisition')
@section('page_content')
<p class="page_heading_large">Purchase Requisition</p>


<form method="post" action="/<?=$pagename;?>/store" id="f">
    <table border="0" width="900" class="tblMaster" align="Center">
        <tr>        
            <td>
                <div class="form-group">
                    <label for="description">Location</label><br><?php echo $lc; ?>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <label for="ref_no">Ref No:</label>
                    <input type="text" class="form-control" id="ref_no" name="ref_no" required autocomplete="off" size="5">
                </div>
            </td>
            <td style="width:100px">
                <div class="form-group">
                    <label for="date">Date</label>
                    <input type="text" class="form-control" id="date" name="date" required autocomplete="off" size="3">
                </div>
            </td>
            <td style="width:75px">
                <div class="form-group">
                    <label for="code">No</label>
                    <input type="text" class="form-control" id="code" name="code" required autocomplete="off" size="3">
                </div>
            </td>
        </tr>


        <tr>
            
            <td colspan="4" style="padding: 0px">
                <table width="100%" border="0">
                    <thead>
                        <tr>
    
                            <!-- <td>
                                <div class="form-group">
                                    <label for="description">Item</label><br>
                                </div>
                            </td> -->
    

                            <td>
                                <div class="form-group">
                                    <label for="item_code">Item Code</label>
                                    <input type="text" class="form-control" id="item_code" required autocomplete="off" size="3">
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <label for="item_desc">Item</label>
                                    <input type="text" class="form-control" id="item_desc" required autocomplete="off">
                                </div>
                            </td>
                            <td style="width:100px">
                                <div class="form-group">
                                    <label for="item_qty">Quantity</label>
                                    <input type="text" class="form-control" id="item_qty" required autocomplete="off" size="1">
                                </div>
                            </td>
                            <td style="width:75px;text-align: right">
                                <button type="button" class="btn btnAdd btn-primary">Add</button>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="4"><div class="message_p">Item(s) not added</div></td>
                        </tr>
                    </tbody>
                </table>
            </td>

        </tr>

        <tr>        
            <td colspan="4"><br>
                <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" class="form-control" id="description" name="description" required autocomplete="off" size="3">
                </div>
            </td>
        </tr>

        <tr>        
            <td colspan="4">            
                <div class="form-group">
                    <label for="employee_code">Employee</label>
                    <input type="text" class="form-control" id="employee_code" name="employee_code" required autocomplete="off" size="3">
                </div>
            </td>
        </tr>

        <tr>        
            <td colspan="4">            
                <div class="form-group">
                    <label for="cost_center_code">Cost Center</label>
                    <input type="text" class="form-control" id="cost_center_code" name="cost_center_code" required autocomplete="off" size="3">
                </div>
            </td>
        </tr>

        <tr>
            <td colspan="4">
                <button type="button" class="btn btnSave btn-primary">Save</button>
            </td>
        </tr>

    </table>
</form>


<!-- Item Search window - A Bootstrap Modal dialog-->
<div class="modal fade" id="ItemSearchPopup" role="dialog">
    <div class="modal-dialog modal-lg"  style="width:96%">

      <!-- Modal content-->
    <div class="modal-content">
        
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Item</h4>
        </div>

        <div class="modal-body">

            <form method="" action="" id="fs" novalidate>

                <table border="0" cellpadding="0" cellspacing="0" class="tbl_item_search" width="100%">

                    <tr>
                        <td valign="top">
                            <div class="form-group">
                                <label for="code">Item Code</label>
                                <input type="text" class="form-control" id="s_code" name="s_code" value="" autocomplete="off" >
                            </div>                            
                        </td>
                        <td colspan="3" valign="top">
                            <div class="form-group">
                                <label for="s_description">Description</label>
                                <input type="text" class="form-control" id="s_description" name="s_description" required autocomplete="off">
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="model">Model</label>
                                <input type="text" class="form-control" id="model" name="model" required autocomplete="off">
                            </div>
                        </td>              
                        <td valign="top">
                            <div class="form-group">
                                <label for="purchase_price">Purchase Price</label>
                                <input type="text" class="form-control" id="purchase_price" name="purchase_price" required autocomplete="off">
                            </div>
                        </td>
                    </tr>
                    
                    <tr>
                        <td valign="top">
                            <div class="form-group">
                                <label for="department_code">Department</label><br><?php echo $s_department; ?>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="category_code">Category</label><br><?php echo $s_category; ?>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="subcategory_code">Sub Category</label><br>                    
                                <div id="divsubcategory">
                                <select class="selectpicker" data-live-search="true" id="subcategory_code" name="subcategory_code" required>
                                  <option value="">Please select category first</option>
                                </select></div>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="brand_code">Brand</label><br><?php echo $s_brand; ?>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="size_code">Size</label><br><?php echo $s_size; ?>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="unit_code">Unit</label><br><?php echo $s_unit; ?>
                            </div>
                        </td>                        
                        
                    </tr>
                    
                </table>                

            </form>

            

        </div>
        <div class="modal-footer">
            <div class="mm" style="float: left; padding-top: 10px;"></div>            
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>        
    </div>
      
    </div>
</div>



<!-- Scripts -->

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/bootstrap-dialog.min.js') }}"></script>
<script src="{{ asset('js/UI_DOM.js') }}"></script>
<script src="{{ asset('js/purchase_requisition.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.js') }}"></script>

@endsection