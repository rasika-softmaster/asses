@guest
    
    @include('partils.header')
    <div class="container">
	    <div class="row">
	        <div class="col-md-8 col-md-offset-2"><br>
	            	@if (isset($_GET['l']))
						<p class="alert alert-danger">Invalid login credentials </p>
                	@endif
	            <div class="panel panel-default">


	                <div class="panel-heading">Login</div>

	                <div class="panel-body">
	                    <form class="form-horizontal" method="POST" action="{{ route('custom.login') }}">
	                        {{ csrf_field() }}

	                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
	                            <label for="username" class="col-md-4 control-label">Username</label>

	                            <div class="col-md-6">
	                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus autocomplete="off">

	                                @if ($errors->has('username'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('username') }}</strong>
	                                    </span>
	                                @endif
	                            </div>
	                        </div>

	                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
	                            <label for="password" class="col-md-4 control-label">Password</label>

	                            <div class="col-md-6">
	                                <input id="password" type="password" class="form-control" name="password" required>

	                                @if ($errors->has('password'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('password') }}</strong>
	                                    </span>
	                                @endif
	                            </div>
	                        </div>

	                        <div class="form-group{{ $errors->has('branch') ? ' has-error' : '' }}">
	                            <label for="branch" class="col-md-4 control-label">Branch</label>

	                            <div class="col-md-6">
	                                <input id="branch" type="branch" class="form-control" name="branch" required autocomplete="off">

	                                @if ($errors->has('branch'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('branch') }}</strong>
	                                    </span>
	                                @endif
	                            </div>
	                        </div>	                        

	                        <!-- <div class="form-group">
	                            <div class="col-md-6 col-md-offset-4">
	                                <div class="checkbox">
	                                    <label>
	                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
	                                    </label>
	                                </div>
	                            </div>
	                        </div> -->

	                        <div class="form-group">
	                            <div class="col-md-8 col-md-offset-4">
	                                <button type="submit" class="btn btn-primary">
	                                    Login
	                                </button>

	                                <!-- <a class="btn btn-link" href="{{ route('password.request') }}">
	                                    Forgot Your Password?
	                                </a> -->
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	@include('partils.footer')

@else

	@include('partils.header')
	<div class="page_wrap">	@yield('page_content') </div>
	@include('partils.footer')

@endguest