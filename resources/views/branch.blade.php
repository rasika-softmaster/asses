@extends('main')

@section('title',' | Branch')
@section('page_content')
<p class="page_heading_large">Branches</p>

<table border="0" width="100%">
    <tr>
        <td><span class="page_heading_medium">Create New Branch</span><br><br></td>
        <td style="padding-left: 30px"><span class="page_heading_medium">Exist Branch(es)</span><br><br></td>
    </tr>
    <tr>
        <td style="width: 300px" valign="top">
            <form method="post" action="/branch/store" id="f">                
                <div class="form-group">
                    <label for="bc">Branch Code</label>
                    <input type="text" class="form-control" id="bc" name="bc" readonly="readonly" value="{{ $bc }}" autocomplete="off" >
                    <input type="hidden" name="is_edit" id="is_edit" value="0">
                </div>
                <div class="form-group">
                    <label for="description">Cluster</label><br><?php echo $cl; ?>
                </div>
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name" name="name" required autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="address">Address:</label>
                    <input type="text" class="form-control" id="address" name="address">
                </div>
                <div class="form-group">
                    <label for="tp">Telephone:</label>
                    <input type="text" class="form-control" id="tp" name="tp" maxlength="10">
                </div>
                <div class="form-group">
                    <label for="fax">Fax:</label>
                    <input type="text" class="form-control" id="fax" name="fax">
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <!-- <input type="text" class="form-control" id="email" name="email"> -->
                    <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}">
                </div>
                <!-- <div class="checkbox">
                    <label><input type="checkbox"> Remember me</label>
                </div> -->
                <button type="submit" class="btn btn-primary btnx">Save</button>
            </form>
        </td>
        <td align="right" valign="top" id='list_view' style="padding-left: 30px"></td>
    </tr>
</table>

<!-- Scripts -->

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/bootstrap-dialog.min.js') }}"></script>
<script src="{{ asset('js/UI_DOM.js') }}"></script>
<script src="{{ asset('js/branch.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.js') }}"></script>

@endsection