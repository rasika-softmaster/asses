@extends('main')

@section('title',' | Branch')
@section('page_content')
<p class="page_heading_large">Accounts</p>

<table border="0" width="100%">
    <tr>
        <td><span class="page_heading_medium">Create/Modify New Account</span><br><br></td>
        <td style="padding-left: 30px"><span class="page_heading_medium">Exist Account(s)</span><br><br></td>
    </tr>
    <tr>
        <td style="width: 300px" valign="top">
            <form method="post" action="/<?=$pagename;?>/store" id="f" novalidate>                
                
                <div class="form-group">
                    <label for="description">Account Category </label><br><div class="account_category_dd"><?php echo $account_category; ?></div>
                </div>

                <div class="form-group">
                    <!-- <label for="code">Account Code</label> -->
                    <input type="hidden" class="form-control" id="code" name="code" readonly="readonly" value="" autocomplete="off" >
                    <input type="hidden" name="is_edit" id="is_edit" value="0">
                </div>

                <div class="form-group">
                    <label for="description">Account Description</label>
                    <input type="text" class="form-control" id="description" name="description" required autocomplete="off">
                </div>

                <div class="form-group">
                    <label for="account_group">Account Group</label><br>    
                        <select class="selectpicker" data-live-search="true" name="account_group" id="account_group">
                            <option value="">None</option>
                            <option value="Cash">Cash</option>
                            <option value="Income">Income</option>
                            <option value="Expense">Expense</option>
                            <option value="Bank">Bank</option>
                        </select>
                </div>

                <div class="form-group">                    
                    <label><input type="checkbox" name="active" id="active"> Inactive</label><br>                    
                </div>

                
                
                <button type="submit" class="btn btn-primary btnx">Save</button>

            </form>
        </td>
        <td align="right" valign="top" id='list_view' style="padding-left: 30px"></td>
    </tr>
</table>

<!-- Scripts -->

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/bootstrap-dialog.min.js') }}"></script>
<script src="{{ asset('js/UI_DOM.js') }}"></script>
<script src="{{ asset('js/'.$pagename.'.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.js') }}"></script>

@endsection