@extends('main')

@section('title',' | Branch')
@section('page_content')
<p class="page_heading_large">Default Accounts</p>

<table border="0" width="100%">
    <tr>
        <td><span class="page_heading_medium">Create/Modify Default Account</span><br><br></td>
        <td style="padding-left: 30px"><span class="page_heading_medium">Exist Default Account(s)</span><br><br></td>
    </tr>
    <tr>
        <td style="width: 300px" valign="top">
            <form method="post" action="/<?=$pagename;?>/store" id="f" novalidate>                
                
                <div class="form-group">
                    <input type="hidden" name="code" id="code" value="{{ $code }}">
                    <label for="def_acc_code">Default Account Code</label>
                    <input type="hidden" name="is_edit" id="is_edit" value="0">
                    <input type="text" class="form-control" id="def_acc_code" name="def_acc_code" autocomplete="off" style="text-transform: uppercase;">
                </div>

                <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" class="form-control" id="description" name="description" autocomplete="off">
                </div>                

                <div class="form-group">
                    <label for="account_code">Account</label><br><div class="account_dd"><?php echo $account; ?></div>
                </div>               
                
                <button type="submit" class="btn btn-primary btnx">Save</button>

            </form>
        </td>
        <td align="right" valign="top" id='list_view' style="padding-left: 30px"></td>
    </tr>
</table>

<!-- Scripts -->

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/bootstrap-dialog.min.js') }}"></script>
<script src="{{ asset('js/UI_DOM.js') }}"></script>
<script src="{{ asset('js/'.$pagename.'.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.js') }}"></script>

@endsection