@extends('main')



@section('title',' | FA Registry')

@section('page_content')

<p class="page_heading_large">FA Registry</p>



<link href="{{ asset('css/jquery-ui.css') }}" rel="stylesheet">



<form method="post" action="/<?=$pagename;?>/store" id="f" novalidate="" onkeydown="return !(event.keyCode==13)">

    <table border="0" width="900" class="tblMaster" align="Center">

        <tr>            

            <td>



                <table width="100%">

                    <tr>

                        <td>

                            <div class="form-group">

                                <label for="s_item_code">Item</label>

                                <input type="text" class="form-control" id="s_item_code" autocomplete="off" readonly="readonly" style="background-color:#ffffff">

                                <input type="hidden" name="item_code" id="item_code" value="">

                                <input type="hidden" name="is_edit" id="is_edit" value="0">

                            </div>

                        </td>

                        <td width="150">

                            <div class="form-group">

                                <label for="description">Trans Type</label><br><?php echo $s_trans_code; ?>

                            </div>

                        </td>

                        <td width="75">

                            <div class="form-group">

                                <label for="trans_no">Trans No</label><br>

                                <input type="text" class="form-control" id="trans_no" name="trans_no" required autocomplete="off" value="<?=$tr_no?>">

                            </div>

                        </td>

                        <td width="100">

                            <div class="form-group">

                                <label for="trans_date">Date</label><br>

                                <input type="text" class="form-control" id="trans_date" name="trans_date" autocomplete="off" value="<?=date('Y-m-d');?>" readonly="readonly">

                            </div>

                        </td>

                    </tr>

                </table>

            </td>

            <td></td>

        </tr>



        <tr>            

            <td>

                <div class="form-group">

                    <label for="serial_no">Serial No </label>

                    <input type="text" class="form-control" id="serial_no" name="serial_no" required autocomplete="off">

                </div>

            </td>

            <td></td>

        </tr>



        <tr>            

            <td>

                <div class="form-group">

                    <label for="description">Location</label><br><?php echo $lc; ?>

                </div>

            </td>

            <td></td>

        </tr>



    </table>



    <hr>



    <table border="0" width="900" class="tblMaster" align="Center">

        <tr><td colspan="3"><label><b style="font-size: 16px">Purchase</b></label><br><br></td></tr>

        <tr>

            <td>

                <div class="form-group">

                    <div class="form-group">

                        <label for="description">Type</label><br><?php echo $s_pur_type; ?>

                    </div>

                </div>

            </td>

            <td>

                <div class="form-group">

                    <label for="pur_no">No </label>

                    <input type="text" class="form-control" id="pur_no" name="pur_no" required autocomplete="off">

                </div>

            </td>

            <td>

                <div class="form-group">

                    <label for="pur_date">Purchase Date </label>

                    <input type="text" class="form-control" id="pur_date" name="pur_date" required autocomplete="off" readonly="readonly">

                </div>

            </td>                        

        </tr>

        <tr>

            <td colspan="2">

                <div class="form-group">                    

                    <div class="form-group">

                        <label for="description">Supplier</label><br><?php echo $s_supplier_code; ?>

                    </div>                    

                </div>

            </td>

            <td>

                <div class="form-group">

                    <label for="pur_cost">Cost </label>

                    <input type="text" class="form-control" id="pur_cost" name="pur_cost" required autocomplete="off">

                </div>

            </td>               

        </tr>



    </table>



    <hr>

    

    <table border="0" width="900" class="tblMaster" align="Center">

        <tr><td colspan="3"><label><b style="font-size: 16px">Depreciation</b></label><br><br></td></tr>

        <tr>

            <td>

                <div class="form-group">

                    <label for="dep_rate">Rate </label>

                    <input type="text" class="form-control" id="dep_rate" name="dep_rate" required autocomplete="off" value="0">

                </div>

            </td>

            <td>

                <div class="form-group">

                    <label for="dep_period">Period </label>

                    <input type="text" class="form-control" id="dep_period" name="dep_period" required autocomplete="off" value="0">

                </div>

            </td>

            <td>

                <div class="form-group">

                    <label for="dep_value">Value</label>

                    <input type="text" class="form-control" id="dep_value" name="dep_value" required autocomplete="off" value="0">

                </div>

            </td>                        

        </tr>

        <tr>

            <td>

                <div class="form-group">

                    <label for="dep_opb_date">OPB for Date </label>

                    <input type="text" class="form-control" id="dep_opb_date" name="dep_opb_date" required autocomplete="off" readonly="readonly" value="0">

                </div>

            </td>

            <td>

                <div class="form-group">

                    <label for="dep_month">Dep Month </label>

                    <input type="text" class="form-control" id="dep_month" name="dep_month" required autocomplete="off" value="0">

                </div>

            </td>

            <td>

                <div class="form-group">

                    <label for="dep_acc_dep">Depreciate Account</label>

                    <input type="text" class="form-control" id="dep_acc_dep" name="dep_acc_dep" required autocomplete="off" value="0">

                </div>

            </td>               

        </tr>

        <tr>

            <td colspan="2">

                <div class="form-group">

                    <label for="dep_opb_value">OPB Depreciation Value </label>

                    <input type="text" class="form-control" id="dep_opb_value" name="dep_opb_value" required autocomplete="off" value="0">

                </div>

            </td>

            <td>

                <div class="form-group">

                    <label for="dep_nbv">NBV </label>

                    <input type="text" class="form-control" id="dep_nbv" name="dep_nbv" required autocomplete="off" value="0">

                </div>

            </td>

        </tr>



    </table>



    <hr>



    <table border="0" width="900" class="tblMaster" align="Center">

        <tr><td colspan="3"><label><b style="font-size: 16px">Capital Allowance</b></label><br><br></td></tr>

        <tr>

            <td>

                <div class="form-group">

                    <label for="cap_rate">Rate </label>

                    <input type="text" class="form-control" id="cap_rate" name="cap_rate" required autocomplete="off" value="0">

                </div>

            </td>

            <td>

                <div class="form-group">

                    <label for="cap_period">Period </label>

                    <input type="text" class="form-control" id="cap_period" name="cap_period" required autocomplete="off" value="0">

                </div>

            </td>

        </tr>

        <tr>

            <td>

                <div class="form-group">

                    <label for="cap_n_years">No of Years</label>

                    <input type="text" class="form-control" id="cap_n_years" name="cap_n_years" required autocomplete="off" value="0">

                </div>

            </td>

            <td>

                <div class="form-group">

                    <label for="cap_total">Total</label>

                    <input type="text" class="form-control" id="cap_total" name="cap_total" required autocomplete="off" value="0">

                </div>

            </td>

        </tr>        

        <tr>

            <td>

                <div class="form-group">

                    <label for="cap_for_year">For the Year</label>

                    <input type="text" class="form-control" id="cap_for_year" name="cap_for_year" required autocomplete="off" value="0">

                </div>

            </td>

            <td>

                <div class="form-group">

                    <label for="cap_balance">Balance</label>

                    <input type="text" class="form-control" id="cap_balance" name="cap_balance" required autocomplete="off" value="0">

                </div>

            </td>

        </tr>

        <tr>

            <td>

                <div class="form-group">

                    <label for="cap_op_amount">OP Amount</label>

                    <input type="text" class="form-control" id="cap_op_amount" name="cap_op_amount" required autocomplete="off" value="0">

                </div>

            </td>

            <td>

                <div class="form-group">

                    <label for="cap_temp_diff">Temporary Difference</label>

                    <input type="text" class="form-control" id="cap_temp_diff" name="cap_temp_diff" required autocomplete="off" value="0">

                </div>

            </td>

        </tr>



        <tr>

            <td colspan="2">

                <div class="form-group">

                    <label for="inactive"><input type="checkbox" id="inactive" name="inactive">

                    Make Inactive</label>

                </div>

            </td>

        </tr>

        



    </table>



    

    <div class="fixed_button_div">

        

        <table border=0 align="Center" width="900">

            <tr>

                <td>

                    <button type="submit" class="btn btnSave btn-primary">Save</button>

                    &nbsp;&nbsp;<a href = ''>Reset</a>

                </td>

            </tr>  

        </table>      

    </div>



</form>



<!-- Item Search window - A Bootstrap Modal dialog-->

<div class="modal fade" id="ItemSearchPopup" role="dialog">

    <div class="modal-dialog modal-lg"  style="width:96%">



      <!-- Modal content-->

    <div class="modal-content">

        

        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Add Item</h4>

        </div>



        <div class="modal-body">



            <form method="post" action="/<?=$pagename;?>/itemsearch" id="fs" novalidate>



                <table border="0" cellpadding="0" cellspacing="0" class="tbl_item_search" width="100%">



                    <tr>

                        <td valign="top">

                            <div class="form-group">

                                <label for="code">Item Code</label>

                                <input type="text" class="form-control" id="s_code" name="s_code" value="" autocomplete="off" >

                            </div>                            

                        </td>

                        <td colspan="3" valign="top">

                            <div class="form-group">

                                <label for="s_description">Description</label>

                                <input type="text" class="form-control" id="s_description" name="s_description" required autocomplete="off">

                            </div>

                        </td>

                        <td valign="top">

                            <div class="form-group">

                                <label for="model">Model</label>

                                <input type="text" class="form-control" id="model" name="model" required autocomplete="off">

                            </div>

                        </td>              

                        <td valign="top">

                            <div class="form-group">

                                <label for="purchase_price">Purchase Price</label>

                                <input type="text" class="form-control" id="purchase_price" name="purchase_price" required autocomplete="off">

                            </div>

                        </td>

                    </tr>

                    

                    <tr>

                        <td valign="top">

                            <div class="form-group">

                                <label for="department_code">Department</label><br><?php echo $s_department; ?>

                            </div>

                        </td>

                        <td valign="top">

                            <div class="form-group">

                                <label for="category_code">Category</label><br><?php echo $s_category; ?>

                            </div>

                        </td>

                        <td valign="top">

                            <div class="form-group">

                                <label for="subcategory_code">Sub Category</label><br>                    

                                <div id="divsubcategory">

                                <select class="selectpicker" data-live-search="true" id="subcategory_code" name="subcategory_code" required>

                                  <option value="">Please select category first</option>

                                </select></div>

                            </div>

                        </td>

                        <td valign="top">

                            <div class="form-group">

                                <label for="brand_code">Brand</label><br><?php echo $s_brand; ?>

                            </div>

                        </td>

                        <td valign="top">

                            <div class="form-group">

                                <label for="size_code">Size</label><br><?php echo $s_size; ?>

                            </div>

                        </td>

                        <td valign="top">

                            <div class="form-group">

                                <label for="unit_code">Unit</label><br><?php echo $s_unit; ?>

                            </div>

                        </td>                        

                        

                    </tr>

                    

                </table>                



            </form>

        </div>



        <div class="modal-body">

            <div class="div-search-result"></div>

        </div>

        <div class="modal-footer">

            <div class="mm" style="float: left; padding-top: 10px;">                    

                <button type="button" class="btn btnSearch btn-primary">Search</button>

            </div>            

            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

        </div>        

    </div>

      

    </div>

</div>







<!-- Scripts -->



<script src="{{ asset('js/app.js') }}"></script>

<script src="{{ asset('js/bootstrap-dialog.min.js') }}"></script>

<script src="{{ asset('js/UI_DOM.js') }}"></script>

<script src="{{ asset('js/fa_registry.js') }}"></script>

<script src="{{ asset('js/bootstrap-select.js') }}"></script>

<script src="{{ asset('js/jquery-ui.js') }}"></script>





<script>

    

    $(document).ready(function(){

        $( "#trans_date,#pur_date,#dep_opb_date" ).datepicker({ 

            dateFormat: 'yy-mm-dd' 

        });

    });



</script>





@endsection