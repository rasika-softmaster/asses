@extends('main')

@section('title',' | Cluster')
@section('page_content')
<p class="page_heading_large">Cluster</p>

<div style="float: left"><span class="page_heading_medium">Exist Cluster(s)</span></div>
<div style="float: right"><!-- <span class="page_heading_medium"></span> -->

<button type="button" class="btn btn-primary btnss">Create New Cluster</button><br><br>

</div>
<div id="list_view"></div>

<div id="m_data_add_form">    
    @include('master.cluster', ['master_page' => 1])
</div>

<!-- Scripts -->

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/bootstrap-dialog.min.js') }}"></script>
<script src="{{ asset('js/UI_DOM.js') }}"></script>
<script src="{{ asset('js/cluster.js') }}"></script>

@endsection