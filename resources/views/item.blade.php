@extends('main')

@section('title',' | Branch')
@section('page_content')
<p class="page_heading_large">Item</p>

<!-- <tr>
    <td><span class="page_heading_medium">Create New Item</span><br><br></td>
    <td style="padding-left: 30px"><span class="page_heading_medium">Exist Item(s)</span><br><br></td>
</tr> -->

<div style="float: left"><span class="page_heading_medium">Exist Item(s)</span></div>
<div style="float: right"><!-- <span class="page_heading_medium"></span> -->

<button type="button" class="btn btn-primary btnAddNew">Create New Item</button><br><br>

</div>

<table border="00" width="100%">
    <tr>
        <td align="right" valign="top">
        		
        	<table border="0" cellpadding="0" cellspacing="0" width="100%" class="tbl_master_item">
				<thead>
					<tr>
						<td>Code</td>
						<!-- <td>Description <input type="text" class="form-control" id="q_description" name="q_description" required autocomplete="off"></td>
						<td>Department <div><?php echo $q_department; ?></div></td>
						<td>Category <div><?php echo $q_category; ?></div></td>
						<td>Sub Category <div id="q_divsubcategory"> <select class="selectpicker" data-live-search="true" id="q_subcategory_code" name="q_subcategory_code" required> <option value="">Please select category first</option> </select></div> </td>
						<td>Brand <div><?php echo $q_brand; ?></div></td>
						<td>Model <input type="text" class="form-control" id="q_model" name="q_model" required autocomplete="off"></td>
						<td>Size <div><?php echo $q_size; ?></div></td>
						<td>Unit <div><?php echo $q_unit; ?></div></td> -->
						<td>Description</td>
						<td>Department</td>
						<td>Category</td>
						<td>Sub Category</td>
						<td>Brand</td>
						<td>Model</td>
						<td>Size</td>
						<td>Unit</td>
						<td>Purchase Price</td>
						<td>Inactive</td>
						<td>Is FA</td>
						<td>Dep Period</td>
						<td>Dep %</td>
						<td>Is Batch</td>
						<td>FA Acc</td>
						<td>Dep Acc</td>
						<td>CA Period</td>
						<td>CA %</td>
						<td>Action</td>
					</tr>
				</thead>
				
				<tbody id='list_view'></tbody>

			</table>

        </td>
    </tr>
</table>

<div id="m_data_add_form">    
    @include('master.item', ['master_page' => 1])
</div>

<!-- Scripts -->

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/bootstrap-dialog.min.js') }}"></script>
<script src="{{ asset('js/UI_DOM.js') }}"></script>
<script src="{{ asset('js/'.$pagename.'.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.js') }}"></script>

@endsection