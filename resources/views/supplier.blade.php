@extends('main')

@section('title',' | Branch')
@section('page_content')
<p class="page_heading_large">Supplier</p>

<table border="0" width="100%">
    <tr>
        <td><span class="page_heading_medium">Create New Supplier</span><br><br></td>
        <td style="padding-left: 30px"><span class="page_heading_medium">Exist Supplier(s)</span><br><br></td>
    </tr>
    <tr>
        <td style="width: 300px" valign="top">
            <form method="post" action="/<?=$pagename;?>/store" id="f">
                <div class="form-group">
                    <label for="code">Supplier Code</label>
                    <input type="text" class="form-control" id="code" name="code" readonly="readonly" value="{{ $code }}" autocomplete="off" >
                    <input type="hidden" name="is_edit" id="is_edit" value="0">
                </div>

                <div class="form-group">
                    <label for="description">Supplier Category</label><br><?php echo $suppliercategory; ?>
                </div>

                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" required autocomplete="off">
                </div>

                <div class="form-group">
                    <label for="address1">Address 1</label>
                    <input type="text" class="form-control" id="address1" name="address1" required autocomplete="off">
                </div>                

                <div class="form-group">
                    <label for="tel1">Tel 1</label>
                    <input type="text" class="form-control" id="tel1" name="tel1" maxlength="10"  required autocomplete="off">
                </div>

                <div class="form-group">
                    <label for="tel2">Tel 2</label>
                    <input type="text" class="form-control" id="tel2" name="tel2"  maxlength="10"  autocomplete="off">
                </div>

                <div class="form-group">
                    <label for="tel3">Tel 3</label>
                    <input type="text" class="form-control" id="tel3" name="tel3"  maxlength="10"  autocomplete="off">
                </div>

                <div class="form-group">
                    <label for="fax">Fax</label>
                    <input type="text" class="form-control" id="fax" name="fax" maxlength="10" autocomplete="off">
                </div>

                <div class="form-group">
                    <label for="con_person">Contcat Person</label>
                    <input type="text" class="form-control" id="con_person" name="con_person" required autocomplete="off">
                </div>

                <div class="form-group">
                    <label for="con_mobile">Contcat Mobile</label>
                    <input type="text" class="form-control" id="con_mobile" name="con_mobile" required autocomplete="off">
                </div>

                <div class="form-group">
                    <label for="credit_limit">Credit Limit</label>
                    <input type="text" class="form-control" id="credit_limit" name="credit_limit"  autocomplete="off" value="0">
                </div>

                <div class="form-group">
                    <label for="credit_period">Credit Period</label>
                    <input type="text" class="form-control" id="credit_period" name="credit_period"  autocomplete="off" value="0">
                </div>

                <button type="submit" class="btn btn-primary btnx">Save</button>

            </form>
        </td>
        <td align="right" valign="top" id='list_view' style="padding-left: 30px"></td>
    </tr>
</table>

<!-- Scripts -->

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/bootstrap-dialog.min.js') }}"></script>
<script src="{{ asset('js/UI_DOM.js') }}"></script>
<script src="{{ asset('js/'.$pagename.'.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.js') }}"></script>

@endsection