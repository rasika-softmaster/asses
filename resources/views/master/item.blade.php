

<!-- Modal info dialog-->
<div class="modal fade" id="ModalMasterPopup" role="dialog">
    <div class="modal-dialog modal-lg" style="width:840px;">

      <!-- Modal content-->
    <div class="modal-content">
        
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New Item</h4>
        </div>

        <div class="modal-body">

            <form method="post" action="/<?=$pagename;?>/store" id="f" enctype="multipart/form-data" novalidate >                


                <table border="0" cellpadding="0" cellspacing="0" class="tbl_item_add">

                    <tr>
                        <td valign="top">
                            <div class="form-group">
                                <label for="code">Item Code</label>
                                <input type="text" class="form-control" id="code" name="code" value="{{ $code }}" autocomplete="off" >
                            </div>
                            <input type="hidden" name="is_edit" id="is_edit" value="0">
                        </td>
                        <td colspan="4" valign="top">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <input type="text" maxlength="50" class="form-control" id="description" name="description" required autocomplete="off">
                            </div>
                        </td>
                    </tr>


                    <tr>
                        <td valign="top">
                            <div class="form-group">
                                <label for="department_code">Department</label><br><?php echo $department; ?>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="category_code">Category</label><br><?php echo $category; ?>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="subcategory_code">Sub Category</label><br>                    
                                <div id="divsubcategory">
                                <select class="selectpicker" data-live-search="true" id="subcategory_code" name="subcategory_code" required>
                                  <option value="">Please select category first</option>
                                </select></div>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="brand_code">Brand</label><br><?php echo $brand; ?>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="model">Model</label>
                                <input type="text" class="form-control" id="model" name="model" required autocomplete="off">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        
                        <td valign="top">
                            <div class="form-group">
                                <label for="size_code">Size</label><br><?php echo $size; ?>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="unit_code">Unit</label><br><?php echo $unit; ?>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="purchase_price">Purchase Price</label>
                                <input type="text" class="form-control" id="purchase_price" name="purchase_price" required autocomplete="off">
                            </div>
                        </td>
                        <td colspan="2" valign="top">
                            <div class="form-group">                    
                                <label><input type="checkbox" name="inactive" id="inactive"> Inactive</label><br>
                                <label><input type="checkbox" name="is_batch" id="is_batch"> Is Batch?</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                <label><input type="checkbox" name="is_fa" id="is_fa"> Is Fixed Asset?</label>
                                <!-- make enable down controlls when click this -->
        

                                <!-- Set m_item's is_fa def value as 0 when use in non fixed asset system -->
                                <!-- <label><input type="checkbox" name="is_fa" id="is_fa"> Is Fixed Asset</label> -->
                            </div>
                        </td>
                    </tr>
                    <tr>                        
                        <td valign="top">
                            <div class="form-group">
                                <label for="dip_period">Depreciate Period</label>
                                <input type="number" class="form-control" id="dip_period" name="dip_period" required autocomplete="off">
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="dip_per">Depreciate Percentage</label>
                                <input type="number" class="form-control" id="dip_per" name="dip_per" maxlength="5" required autocomplete="off">
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="fa_acc">Fixed Asset Account</label><br><?php echo $fa_acc; ?>
                            </div>
                        </td>
                        <td valign="top" colspan="2" rowspan="2" style="padding: 0px">
                            
                            <div class="img_holder">
                                <label for="fa_acc">Item Picture</label>
                                <div class="imgview"><img class='aa' src='{{ asset('/img/') }}'></div>
                                <input type="file" name="item_image" id="item_image">   

                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <div class="form-group">
                                <label for="ca_period">Capital Alliance Period</label>
                                <input type="number" class="form-control" id="ca_period" name="ca_period" required autocomplete="off">
                            </div>
                        </td>
                        <td valign="top">
                            <div class="form-group">
                                <label for="ca_per">Capital Alliance Percentage</label>
                                <input type="number" class="form-control" id="ca_per" name="ca_per" maxlength="5" required autocomplete="off">
                            </div> 
                        </td>
                        <td valign="top">
                            <div class="form-group">                                
                                <label for="dip_acc">Depreciate Account</label><br><?php echo $dip_acc; ?>
                            </div>
                        </td>
                        
                    </tr>
                </table>                

            </form>

            <?php if ( !$master_page == 1 ){ ?>    
            <script src="{{ asset('js/app.js') }}"></script>        
            <script src="{{ asset('js/item.js?def_fn=0') }}"></script>
            <?php } ?>

        </div>
        <div class="modal-footer">
            <div class="mm" style="float: left; padding-top: 10px;"></div>
            <button type="button" class="btn btn-primary btnAddItem">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>        
    </div>
      
    </div>
</div>