

<!-- Modal info dialog-->
<div class="modal fade" id="ModalMasterPopup" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
    <div class="modal-content">
        <form method="post" action="/cluster/store" id="f">                
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Add New Cluster</h4>
            </div>
            <div class="modal-body">
          
                <div class="form-group">            
                    <label for="code">Code</label>
                    <input type="text" class="form-control" id="code" name="code" readonly="readonly" value="{{ $code }}" autocomplete="off" >
                    <input type="hidden" name="is_edit" id="is_edit" value="0">
                </div>
                <div class="form-group">
                    <label for="description">Description:</label>
                    <input type="text" class="form-control" id="description" name="description" required autocomplete="off">
                </div>

                <?php if ( !$master_page == 1 ){ ?>    
                <script src="{{ asset('js/app.js') }}"></script>        
                <script src="{{ asset('js/cluster.js?def_fn=0') }}"></script>
                <?php } ?>

            </div>
            <div class="modal-footer">
                <div class="mm" style="float: left; padding-top: 10px"></div>
                <button type="submit" class="btn btn-primary btns">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
      
    </div>
</div>