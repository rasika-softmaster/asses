@extends('main')

@section('title',' | Branch')
@section('page_content')
<p class="page_heading_large">Account Category</p>

<table border="0" width="100%">
    <tr>
        <td><span class="page_heading_medium">Create New Category</span><br><br></td>
        <td style="padding-left: 30px"><span class="page_heading_medium">Exist Account Category</span><br><br></td>
    </tr>
    <tr>
        <td style="width: 300px" valign="top">
            <form method="post" action="/<?=$pagename;?>/store" id="f" novalidate>                
                <div class="form-group">
                    <label for="code">Category Code</label>
                    <input type="text" class="form-control" id="code" name="code" readonly="readonly" value="{{ $code }}" autocomplete="off" >
                    <input type="hidden" name="is_edit" id="is_edit" value="0">
                </div>

                <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" class="form-control" id="description" name="description" required autocomplete="off">
                </div>

                <div class="form-group">
                    <label for="description">Parent</label><br><div class="parent_dd"><?php echo $parent; ?></div>
                </div>

                <div class="form-group">
                    <label for="description">Type</label><br>
    
                        <select class="selectpicker" data-live-search="true" name="type" id="type">

                            <option value=""></option>
                            <option value="BALANCE_SHEET">Balance Sheet</option>
                            <option value="PROFIT_AND_LOSS">Profit and Loss</option>

                        </select>

                </div>
                </div>
                
                <button type="submit" class="btn btn-primary btnx">Save</button>

            </form>
        </td>
        <td align="right" valign="top" id='list_view' style="padding-left: 30px"></td>
    </tr>
</table>

<!-- Scripts -->

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/bootstrap-dialog.min.js') }}"></script>
<script src="{{ asset('js/UI_DOM.js') }}"></script>
<script src="{{ asset('js/'.$pagename.'.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.js') }}"></script>

@endsection