@extends('main')

@section('title',' | Branch')
@section('page_content')
<p class="page_heading_large">Company</p>

<table border="0" width="100%">
    <tr>
        <td><span class="page_heading_medium">Create New Company</span><br><br></td>
        <td style="padding-left: 30px"><span class="page_heading_medium">Exist Company</span><br><br></td>
    </tr>
    <tr>
        <td style="width: 300px" valign="top">
            <form method="post" action="/<?=$pagename;?>/store" id="f">                
                <div class="form-group">
                    <label for="code">Company Code</label>
                    <input type="text" class="form-control" id="code" name="code" readonly="readonly" value="{{ $code }}" autocomplete="off" >
                    <input type="hidden" name="is_edit" id="is_edit" value="0">
                </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" required autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="address01">address01</label>
                    <input type="text" class="form-control" id="address01" name="address01" required autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="phone01">Phone 1</label>
                    <input type="text" class="form-control" id="phone01" name="phone01" maxlength="10" required autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="phone02">Phone 2</label>
                    <input type="text" class="form-control" id="phone02" name="phone02" maxlength="10" required autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email"  autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="name">Fax</label>
                    <input type="text" class="form-control" id="fax" name="fax"  autocomplete="off">
                </div>
                <button type="submit" class="btn btn-primary btnx">Save</button>
            </form>
        </td>
        <td align="right" valign="top" id='list_view' style="padding-left: 30px"></td>
    </tr>
</table>

<!-- Scripts -->

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/bootstrap-dialog.min.js') }}"></script>
<script src="{{ asset('js/UI_DOM.js') }}"></script>
<script src="{{ asset('js/'.$pagename.'.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.js') }}"></script>

@endsection