<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>


@if (count($errors) > 0)

@foreach($errors->all() as $error)

<p class="alert alert-danger">{{ $error }}</p>

@endforeach


@endif

<form action="{{ route('custom.register') }}" method="post">
	
	{{ csrf_field() }}

	Name
	<input type="text" name="name" value="{{ old('name') }}"><br>

	Username
	<input type="text" name="username" value="{{ old('username') }}"><br>

	Password
	<input type="password" name="password" value="{{ old('password') }}"><br>

	branch
	<input type="text" name="branch" value="{{ old('branch') }}"><br>

	<button type="submit">Register</button>

</form>

</body>
</html>