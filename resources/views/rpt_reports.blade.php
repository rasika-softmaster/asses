@extends('main')



@section('title',' | Reports')

@section('page_content')



<link href="{{ asset('css/jquery-ui.css') }}" rel="stylesheet">

<p class="page_heading_large">Reports</p>

<form method="post" action="/rpt_reports/generate" id="f" target="_blank">

    <table border="0" width="100%">

        <tr>

            <td>

                <table border="0">

                    <tr>

                        <td width="100">

                            <div class="form-group">

                                <label for="fd">From Date</label><br>

                                <input type="text" class="form-control" id="fd" name="fd" autocomplete="off"
                                    value="<?=date('Y-m-d');?>" readonly="readonly">

                            </div>

                        </td>

                        <td width="120" style="padding-left: 20px">

                            <div class="form-group">

                                <label for="td">To Date</label><br>

                                <input type="text" class="form-control" id="td" name="td" autocomplete="off"
                                    value="<?=date('Y-m-d');?>" readonly="readonly">

                            </div>

                        </td>

                        <td style="padding-left: 10px"></td>

                        <td width="120" style="padding-left: 10px; border-left: 1px solid #CCC">

                            <div class="form-group">

                                <label for="td">Item Code</label><br>

                                <?=$s_item?>

                            </div>

                        </td>



                        <td style="padding-left: 10px"></td>

                        <td width="120" style="padding-left: 10px; border-left: 1px solid #CCC">

                            <div class="form-group">

                                <label for="td">Location</label><br>

                                <?=$s_location?>

                            </div>

                        </td>





                    </tr>

                </table>

            </td>

        </tr>



        <tr>

            <td style="border-top: 1px solid #ccc; padding-top: 10px; padding-bottom: 10px">

                <label><input type="radio" name="rpt_selected" value="rpt_item_list"> &nbsp;Item List</label>

            </td>

        </tr>



        <tr>

            <td style="border-top: 1px solid #ccc; padding-top: 10px; padding-bottom: 10px">

                <label><input type="radio" name="rpt_selected" value="rpt_item_serial"> &nbsp;Item Serial</label>

                &nbsp; &nbsp; &nbsp;<label><input type="checkbox" id="rpt_selected_all" name="rpt_selected_all"
                        value="rpt_item_serial_all"> &nbsp;All</label>

            </td>



        </tr>



        <tr>

            <td style="border-top: 1px solid #ccc; padding-top: 10px; padding-bottom: 10px">

                <label><input type="radio" name="rpt_selected" value="rpt_item_transfer"> &nbsp;Item Transfer</label>

            </td>

        </tr>
        <tr>
            <td style="border-top: 1px solid #ccc; padding-top: 10px; padding-bottom: 10px">

                <label><input type="radio" name="rpt_selected" value="rpt_stock_in_hand"> &nbsp;Stock in hand</label>

            </td>
        </tr>


        <tr>

            <td style="border-top: 1px solid #ccc; padding-top: 10px; padding-bottom: 10px">

                <label><input type="radio" name="rpt_selected" value="r_pur_summary"> &nbsp;Purchase Summary</label>

            </td>

        </tr>
        <tr>

            <td style="border-top: 1px solid #ccc; padding-top: 10px; padding-bottom: 10px">

                <label><input type="radio" name="rpt_selected" value="r_pur_details"> &nbsp;Purchase Details</label>

            </td>

        </tr>

      

        <tr>

            <td style="width: 300px; border-top:1px solid #ccc; padding-top: 10px" valign="top">

                <button type="button" class="btn btn-primary" id="btnGenPDF">Generate PDF</button>

            </td>

        </tr>

    </table>



    <input type="hidden" name="_token" value="{{ csrf_token() }}">



</form>



<!-- Scripts -->



<script src="{{ asset('js/app.js') }}"></script>

<script src="{{ asset('js/bootstrap-dialog.min.js') }}"></script>

<script src="{{ asset('js/UI_DOM.js') }}"></script>

<script src="{{ asset('js/'.$pagename.'.js') }}"></script>

<script src="{{ asset('js/bootstrap-select.js') }}"></script>

<script src="{{ asset('js/jquery-ui.js') }}"></script>





<script type="text/javascript" src="{{ asset('http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js') }}">
</script>





<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.datepick.css') }}">

<script type="text/javascript" src="{{ asset('js/jquery.plugin.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/jquery.datepick.js') }}"></script>



<script>
    $(document).ready(function () {

        // $( "#fd,#td" ).datepicker({

        //     dateFormat: 'yy-mm-dd' 

        // });



        // $.datepick.setDefaults({pickerClass: 'my-picker'});

        //$(selector).datepick();

        $("#fd").datepick({
            dateFormat: 'yyyy-mm-dd'
        });

        $("#td").datepick({
            dateFormat: 'yyyy-mm-dd'
        });

    });
</script>



@endsection