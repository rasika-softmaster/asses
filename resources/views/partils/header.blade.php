<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">



    <!-- CSRF Token -->

    <meta name="csrf-token" content="{{ csrf_token() }}">



    <title>{{ config('app.name', 'Home') }}</title>



    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet">

</head>

<body>

    <div id="app_page">

        

        @guest



        @else

        <nav class="navbar navbar-default navbar-static-top">

            <div class="container">

                <div class="navbar-header">



                    <!-- Collapsed Hamburger -->

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">

                        <span class="sr-only">Toggle Navigation</span>

                        <span class="icon-bar"></span>

                        <span class="icon-bar"></span>

                        <span class="icon-bar"></span>

                    </button>



                    <!-- Branding Image -->

                    <a class="navbar-brand" href="{{ url('/') }}">

                        {{ config('app.name', 'Home') }}

                    </a>

                </div>



                <div class="collapse navbar-collapse" id="app-navbar-collapse">

                    <!-- Left Side Of Navbar -->

                    <ul class="nav navbar-nav">

                        

                        <li class="dropdown">

                            

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> Master <span class="caret"></span> </a>

                            

                            <ul class="dropdown-menu" role="menu">

                                <!-- <li><a href="/cluster">Cluster</a></li>

                                <li><a href="/branch">Branches</a></li> -->

                                <li><a href="/stores">Stores</a></li>

                                <li><a href="/brand">Brand</a></li>

                                <li><a href="/category">Item Category</a></li>

                                <li><a href="/subcate">Item Sub Category</a></li>

                                <li><a href="/company">Company</a></li>

                                <li><a href="/color">Color</a></li>

                                <li><a href="/unit">Unit</a></li>

                                <li><a href="/size">Size</a></li>

                                <li><a href="/suppliercategory">Supplier Category</a></li>

                                <li><a href="/supplier">Supplier</a></li>

                                <li><a href="/department">FA Department</a></li>

                                <li><a href="/item">Item</a></li>

                                <li role="separator " class="divider "></li>

                                <li><a href="/location_department">Location Department</a></li>

                                <li><a href="/location_division">Location Division</a></li>

                                <li><a href="/location">Location</a></li>

                            </ul>



                        </li>



                        <li class="dropdown">

                            

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> Accounts <span class="caret"></span> </a>

                            

                            <ul class="dropdown-menu" role="menu">

                                <li><a href="account_category">Account Category</a></li>

                                <li><a href="create_modify_account">Create/Modify Account</a></li>

                                <li><a href="set_default_account">Set Default Account</a></li>

                            </ul>                            



                        </li>



                        <li class="dropdown">

                            

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> Transactions <span class="caret"></span> </a>

                            

                            <ul class="dropdown-menu" role="menu">

                               

                                <li><a href="purchase_requisition">Purchase Requisition</a></li>
                                 <li><a href="purchase">Purchase </a></li>

                                <li><a href="fa_registry">FA Registry</a></li>

                                <li><a href="fa_transfer">FA Transfer</a></li>
                                <li><a href="stock_transfer_non_serial">Stock transfer - Non Serial Items</a></li>
                                <li><a href="item_damage">Item Damage</a></li>

                            </ul>



                        </li>





                        <li class="dropdown">

                            

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> Reports <span class="caret"></span> </a>

                            

                            <ul class="dropdown-menu" role="menu">

                                <li><a href="rpt_reports">Reports</a></li>                                

                            </ul>



                        </li>



                        

                         

                    </ul>



                    <!-- Right Side Of Navbar -->

                    <ul class="nav navbar-nav navbar-right">

                        <!-- Authentication Links -->

                        @guest

                            <li><a href="{{ route('login') }}">Login</a></li>                            

                        @else

                            <li class="dropdown">

                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">

                                    {{ Auth::user()->name }} <span class="caret"></span>

                                </a>



                                <ul class="dropdown-menu">

                                    <li>

                                        <a href="{{ route('logout') }}"

                                            onclick="event.preventDefault();

                                                     document.getElementById('logout-form').submit();">

                                            Logout

                                        </a>



                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

                                            {{ csrf_field() }}

                                        </form>

                                    </li>

                                </ul>

                            </li>

                        @endguest

                    </ul>

                </div>

            </div>

        </nav>

    



          <!-- Modal info dialog-->

          <div class="modal fade" id="infoModal" role="dialog">

            <div class="modal-dialog">

            

              <!-- Modal content-->

              <div class="modal-content">

                <div class="modal-header">

                  <button type="button" class="close" data-dismiss="modal">&times;</button>

                  <h4 class="modal-title">Modal Header</h4>

                </div>

                <div class="modal-body">

                  <p>Some text in the modal.</p>

                </div>

                <div class="modal-footer">

                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>

              </div>

              

            </div>

          </div>



          <!-- Modal processing/loading/please wait dialog-->

          <div class="modal fade" id="ModalProgress" role="dialog">

            <div class="modal-dialog">

            

              <!-- Modal content-->

              <div class="modal-content">

                <div class="modal-body">

                  Please wait

                </div>                

              </div>

              

            </div>

          </div>

          

            



    </div>



    @endguest